﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace MainMenu
{
    public class StageVerticalScroll : MonoBehaviour, IDragHandler, IPointerDownHandler, IEndDragHandler, IBeginDragHandler
    {
        public RectTransform rectTransform;
        public Transform ScrollView;

        private Vector3 targetPos;
        private float maxSize;
        private float elasticMaxSize;
        private float elasticMinSize;
        private bool isDrag;

        void Awake()
        {
            targetPos = ScrollView.localPosition;
        }

        IEnumerator Start()
        {
            yield return new WaitForFixedUpdate();
            maxSize = (rectTransform.rect.height - 850f) * -1f;
            elasticMaxSize = (rectTransform.rect.height - 425f) * -1f;
            elasticMinSize = 425f;
        }

        public void OnPointerDown(PointerEventData e)
        {
            isDrag = true;
        }

        public void OnBeginDrag(PointerEventData e)
        {
            isDrag = true;
        }

        public void OnDrag(PointerEventData e)
        {
            var targetPos = -e.delta;
            var pos = ScrollView.localPosition;
            pos.y += (targetPos.normalized.y * targetPos.magnitude) * 0.5f;
            pos.y = Mathf.Clamp(pos.y, elasticMaxSize, elasticMinSize);
            ScrollView.localPosition = pos;
        }

        public void OnEndDrag(PointerEventData e)
        {
            targetPos = ScrollView.localPosition;
            Vector3 velocity = -e.delta;
            targetPos.y += (velocity.normalized.y * velocity.sqrMagnitude) * 0.5f;
            targetPos.x = ScrollView.localPosition.x;
            isDrag = false;
        }

        void Update()
        {
            if (!isDrag)
            {
                targetPos.y = Mathf.Clamp(targetPos.y, maxSize, 0f);
                ScrollView.localPosition =
                    Vector3.Lerp(ScrollView.localPosition, targetPos, 5f * Time.deltaTime);
            }
        }
    }
}