﻿using UnityEngine;
using System;
using System.Collections;

namespace MainMenu
{
    public class StageScroll : MonoBehaviour
    {
        public event EventHandler ScrollEvent;

        public MainMenuStateMng mainMenuStateMng;
        public Transform Content;

        private Vector3 prevPos;
        private Vector3 currPos;
        private float enableScroll;
        private bool isActivated;
        private float gap;

        public int Level { get; private set; }

        // Use this for initialization
        void Start()
        {
            Level = (int)MainMenuStateMng.CurrentLevel - (int)MainMenuStateMng.MenuLevel.NOVICE;
            gap = 720f;
            enableScroll = 43f;
        }

        public void InitScrollPos()
        {
            Vector3 pos = new Vector3(Level * gap, 0f);
            Content.transform.localPosition = pos;
        }

        public void InvokeStageScrollEvent()
        {
            if (ScrollEvent != null)
            {
                ScrollEvent(this, new EventArgs());
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                prevPos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                if (mainMenuStateMng.isSceneIdle || mainMenuStateMng.isSceneEntering)
                    return;
                currPos = Input.mousePosition;
                Vector3 point = currPos - prevPos;
                prevPos = currPos;
                float magnitude = point.x;
                if (Mathf.Abs(magnitude) >= enableScroll && !isActivated)
                {
                    isActivated = true;
                    if (magnitude > 0)
                    {
                        MainMenuStateMng.CurrentLevel--;
                        Level--;
                    }
                    else
                    {
                        MainMenuStateMng.CurrentLevel++;
                        Level++;
                    }
                    Level = Mathf.Clamp(Level, 0, 3);
                    MainMenuStateMng.CurrentLevel =
                        (MainMenuStateMng.MenuLevel)Mathf.Clamp((int)MainMenuStateMng.CurrentLevel, (int)MainMenuStateMng.MenuLevel.NOVICE, (int)MainMenuStateMng.MenuLevel.MASTER);
                    mainMenuStateMng.LevelText.text = MainMenuStateMng.CurrentLevel.ToString();
                    InvokeStageScrollEvent();
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                isActivated = false;
            }

            Vector3 pos = new Vector3(Level * gap, 0f);
            Content.transform.localPosition = Vector3.Lerp(Content.transform.localPosition, pos, mainMenuStateMng.StageScrollSpeed * Time.deltaTime);
        }
    }
}