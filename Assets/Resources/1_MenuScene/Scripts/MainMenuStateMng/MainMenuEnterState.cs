﻿using GameStateLibrary.State;
using System;
using System.Collections;
using UnityEngine;

namespace MainMenu
{
    public class MainMenuEnterState : State<MainMenuStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return MainMenuStateMng.MenuState.enter;
            }
        }

        // 연출 순서
        // 1) 페이드 아웃
        // 2) 카메라 다운
        // 3) -> IdleScene
        public override void Enter(params object[] o_Params)
        {
            instance.isSceneEntering = true;
            StartCoroutine(manageSNS());
            StartCoroutine(FadeOut());
        }

        private IEnumerator manageSNS()
        {
            yield return new WaitForFixedUpdate();
            GoogleMng.Instance.InvokeLoginEvent();
        }
        private IEnumerator FadeOut()
        {
            yield return new WaitForFixedUpdate();
            instance.UICamera.transform.localPosition = new Vector3(0f, -1440f);
            instance.StageCameraParent.localPosition = new Vector3(-2880f, -1440f); // <- 스테이지 선택창이 화면을 가린다...
            instance.CoverImage.transform.localPosition = instance.UICamera.transform.localPosition;
            yield return new WaitForSeconds(0.2f);
            instance.Option.OptionParent.gameObject.SetActive(false);
            instance.Option.OptionCanvasGroup.alpha = 0f;
            instance.StageSelectCanvas.alpha = 0f;
            instance.StageCanvas.alpha = 0f;
            GC.Collect();
            yield return new WaitForSeconds(0.2f);

            Color color = new Color(0f, 0f, 0f, 1f);

            float t = 0f;
            while (t <= 0.997f)
            {
                t += Mathf.Lerp(1f, 0f, t) * 4f * Time.deltaTime;
                color.a = Mathf.Lerp(1f, 0f, t);
                instance.CoverImage.color = color;
                yield return null;
            }
            instance.CoverImage.gameObject.SetActive(false);
            if (MainMenuStateMng.IsGameActivated)
                instance.ChangeState(MainMenuStateMng.MenuState.idle);
            else {
                SoundMng.Instance.PlayBGM();
                StartCoroutine(directMenu());
            }
        }

        // 메뉴 연출
        private IEnumerator directMenu()
        {
            yield return new WaitForSeconds(0.2f);

            Vector3 v = Vector3.zero;
            float t = 0;
            float smoothTime = 72.25f;
            float revision = smoothTime * 0.185f;
            while (t <= smoothTime)
            {
                if (Input.GetMouseButtonDown(0))
                    break;
                t += revision * Time.deltaTime;
                instance.MainCamera.transform.localPosition =
                    Vector3.SmoothDamp(instance.MainCamera.transform.localPosition, Vector3.zero, ref v, smoothTime * Time.deltaTime);
                yield return null;
            }
            if (!Mng.Instance.IsGameDarkest)
            {
                StartCoroutine(changeBGalpha());
                StartCoroutine(raiseSun());
            }
            instance.ChangeState(MainMenuStateMng.MenuState.idle);
        }

        private IEnumerator changeBGalpha()
        {
            float t = 1f;

            while (t >= 0f)
            {
                t -= 1.75f * Time.deltaTime;
                instance.BG_night.color =
                    new Color(1f, 1f, 1f, t);
                yield return null;
            }
            instance.BG_night.gameObject.SetActive(false);
        }

        // 태양을 들어올려라(...) riseSun 보다는 자연스러워서..
        private IEnumerator raiseSun()
        {
            float t = 0f;
            Vector3 start = instance.Sun.localPosition;
            Vector3 end = new Vector3(0f, -178f);
            while (t <= 0.997f)
            {
                t += 2f * Mathf.Lerp(1f, 0f, t) * Time.deltaTime;
                instance.Sun.localPosition =
                    Vector3.Lerp(start, end, t);
                yield return null;
            }
            instance.Sun.localPosition = end;
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            MainMenuStateMng.IsGameActivated = true;
            instance.isSceneEntering = false;
            instance.MainCamera.transform.localPosition = Vector3.zero;
            instance.IdleSceneAnim.TitleAnim.Play();
            instance.IdleSceneAnim.StartBtnAnim.Play();
            instance.IdleSceneAnim.OptionBtnAnim.Play();
            instance.IdleSceneAnim.EditBtnAnim.Play();
            instance.VerticalEnable.ForEach(script => script.enabled = true);
            GC.Collect();
        }
    }
}