﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

namespace MainMenu
{
    public class MainMenuSettingState : State<MainMenuStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return MainMenuStateMng.MenuState.setting;
            }
        }

        private float speed = 12.5f;

        public override void Enter(params object[] o_Params)
        {
            instance.Escape = null;
            instance.Option.OptionParent.SetActive(true);
            StartCoroutine(popup());
        }

        private IEnumerator popup()
        {
            float t = 0f;
            Vector3 start = instance.Option.OptionTransform.localPosition;
            yield return new WaitForFixedUpdate();
            while (t <= 0.997f)
            {
                t += speed * Mathf.Lerp(1f, 0f, t) * Time.deltaTime;
                instance.Option.OptionCanvasGroup.alpha = t;
                instance.Option.OptionTransform.localPosition =
                    Vector3.Lerp(start, Vector3.zero, t);
                yield return null;
            }

            instance.Option.OptionCanvasGroup.alpha = 1f;
            instance.Option.OptionTransform.localPosition = Vector3.zero;
            instance.Escape = () =>
            {
                instance.RemoveState(MainMenuStateMng.MenuState.setting);
            };
        }

        private IEnumerator popdown()
        {
            float t = 0f;
            Vector3 start = instance.Option.OptionTransform.localPosition;
            Vector3 end = new Vector3(0f, -300f);

            while (t <= 0.997f)
            {
                t += speed * Mathf.Lerp(1f, 0f, t) * Time.deltaTime;
                instance.Option.OptionCanvasGroup.alpha = 1f - t;
                instance.Option.OptionTransform.localPosition =
                    Vector3.Lerp(start, end, t);
                yield return null;
            }
            instance.Option.OptionCanvasGroup.alpha = 0f;
            instance.Option.OptionTransform.localPosition = end;
            instance.Option.OptionParent.SetActive(false);
            instance.Escape = instance.Quit;
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            StartCoroutine(popdown());
        }
    }
}