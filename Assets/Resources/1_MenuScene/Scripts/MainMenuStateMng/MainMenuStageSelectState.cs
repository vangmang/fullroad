﻿using GameStateLibrary.State;
using System;
using UnityEngine;
using System.Collections;

namespace MainMenu
{
    public class MainMenuStageSelectState : State<MainMenuStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return MainMenuStateMng.MenuState.stageSelect;
            }
        }

        private bool isSceneStage;

        public override void Enter(params object[] o_Params)
        {
            isSceneStage = true;
            instance.UICamera.transform.localPosition = Vector3.zero;
            instance.StageCameraParent.localPosition = new Vector3(0f, -430f);
            instance.CoverImage.transform.localPosition = instance.UICamera.transform.localPosition;

            StartCoroutine(AlphaFadeOut());
            instance.stageScroll.InvokeStageScrollEvent();
            instance.stageScroll.InitScrollPos();
            instance.Escape = () =>
            {
                instance.ChangeState(MainMenuStateMng.MenuState.idle);
            };
        }

        public override void Execute()
        {
            Vector3 levelPos = new Vector3((int)MainMenuStateMng.CurrentLevel * 13.3f - 20f, -40f);
            instance.MainCamera.transform.localPosition = 
                Vector3.Lerp(instance.MainCamera.transform.localPosition, levelPos, instance.StageScrollSpeed * Time.deltaTime);
            instance.MainCamera.orthographicSize = 
                Mathf.Lerp(instance.MainCamera.orthographicSize, instance.CameraStageSize, instance.CameraDeltaSpeed * Time.deltaTime);
        }

        public override void Exit()
        {
            isSceneStage = false;
            StartCoroutine(AlphaFadeIn());
        }

        private IEnumerator AlphaFadeOut()
        {
            float t = instance.StageSelectCanvas.alpha;
            while (t <= 1f)
            {
                if (!isSceneStage)
                    yield break;
                t += instance.StageScrollSpeed * Time.deltaTime;
                instance.StageSelectCanvas.alpha = t;
                instance.StageCanvas.alpha = t;
                yield return null;
            }
        }

        private IEnumerator AlphaFadeIn()
        {
            float t = instance.StageSelectCanvas.alpha;
            while (t >= 0f)
            {
                if (isSceneStage)
                    yield break;
                t -= instance.StageScrollSpeed * Time.deltaTime;
                instance.StageSelectCanvas.alpha = t;
                instance.StageCanvas.alpha = t;
                yield return null;
            }
            instance.UICamera.transform.localPosition = new Vector3(0f, -1440f);
            instance.StageCameraParent.localPosition = new Vector3(-2880f, -1440f);
            instance.CoverImage.transform.localPosition = instance.UICamera.transform.localPosition;
        }
    }
}