﻿using GameStateLibrary.StateMng;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

namespace MainMenu
{
    public class MainMenuStateMng : SingletonStateMng<MainMenuStateMng>
    {
        public enum MenuState
        {
            enter,
            idle,
            setting,
            stageSelect,
            exit
        }

        public enum MenuLevel
        {
            NOVICE,
            NORMAL,
            EXPERT,
            MASTER
        }

        public static bool IsGameActivated;

        [Serializable]
        public class _Level_List_ : IEnumerable, IEnumerator // 레벨이 어떻게 추가 될지 몰라서 반복가능 클래스로 만들었고, 
                                                             // 또 따로 리스트로 레벨을 만들지 않은 이유는 다른 트랜스폼이 리스트에 잘못 들어갈 수 있기 때문
        {
            private List<RectTransform> LevelList;
            private int i;
            private int length;
            public RectTransform Novice;
            public RectTransform Normal;
            public RectTransform Expert;
            public RectTransform Master;

            public void init()
            {
                i = -1;
                LevelList = new List<RectTransform>();
                LevelList.Add(Novice);
                LevelList.Add(Normal);
                LevelList.Add(Expert);
                LevelList.Add(Master);
                length = LevelList.Count;
            }

            public object Current
            {
                get
                {
                    return LevelList[i];
                }
            }

            public bool MoveNext()
            {
                return ++i < length;
            }

            public void Reset()
            {
                i = -1;
            }

            public IEnumerator GetEnumerator()
            {
                return this;
            }
        }
        [SerializeField]
        private _Level_List_ Level_List;
        [SerializeField]
        private Transform idleScene;
        [SerializeField]
        private Toggle developerModeToggle;

        public Text LevelText;

        public Transform IdleScene { get { return idleScene; } }

        public Image CoverImage;
        public InsigniaMng Insignia;

        [Header("Cameras")]
        public Camera MainCamera;
        public Camera UICamera;
        public Transform StageCameraParent;
        [Space(10)]

        public StageScroll stageScroll;
        public bool isSceneIdle;
        public bool isSceneEntering;
        [Serializable]
        public struct _IdleSceneAnim_
        {
            public Animation TitleAnim;
            public Animation StartBtnAnim;
            public Animation OptionBtnAnim;
            public Animation EditBtnAnim;
        }
        [Serializable]
        public struct _Option_
        {
            public GameObject OptionParent;
            public Transform OptionTransform;
            public CanvasGroup OptionCanvasGroup;
        }
        [Serializable]
        public class _Tip_
        {
            public readonly List<string> tipTextList =
                new List<string>
                {
                    "Tip. 복잡한 맵은 파악을\n먼저하고 진행하세요.",
                    "Tip. 설정에서 휘장을\n선택할 수 있습니다.",
                    "Tip. 게임은 초기화\n할 수 없습니다.",
                    "Tip. 붉은 색 휘장은\n얻기 힘듭니다.",
                    "Tip. 7, 7, 7, == 87",
                    "Tip. 4, 0, 0, <= 50",
                    "Tip. 힌트는 없어요."
                };
            public Transform TipTransform;
            public Text TipText;
            public void InitTipText()
            {
                if (Mng.Instance.EachLevelProcess[MenuLevel.NOVICE] == 0 &&
                    Mng.Instance.EachLevelProcess[MenuLevel.NORMAL] == 0 &&
                    Mng.Instance.EachLevelProcess[MenuLevel.EXPERT] == 0 &&
                    Mng.Instance.EachLevelProcess[MenuLevel.MASTER] == 0)
                    TipText.text = "Tip. 튜토리얼은 NOVICE\n1~4 스테이지 입니다.";
                else
                {
                    int rand = UnityEngine.Random.Range(0, tipTextList.Count);
                    TipText.text = tipTextList[rand];
                }
            }
        }
        public List<StageVerticalScroll> VerticalEnable;

        public Image BG_night;
        public RawImage BG_cloud;
        public CanvasGroup Stars1;
        public CanvasGroup Stars2;

        public _IdleSceneAnim_ IdleSceneAnim;
        public _Option_ Option;
        public _Tip_ Tip;
        public Transform Sun;

        public readonly float StageScrollSpeed = 8.5f;
        public readonly float CameraIdleSize = 640f;
        public readonly float CameraStageSize = 580f;
        public readonly float CameraDeltaSpeed = 5.5f;

        public CanvasGroup StageSelectCanvas;
        public CanvasGroup IdleCanvas;
        public CanvasGroup StageCanvas;
        public GameObject NewInsigniaNotice;

        public delegate void EscapeBtn();
        public EscapeBtn Escape;

        public static MenuLevel CurrentLevel = MenuLevel.NOVICE;

        private bool isStarGlittered;
        private float i;

        public void Quit()
        {
            if (Application.platform == RuntimePlatform.Android)
                Application.Quit();
        }

        void Awake()
        {
#if !UNITY_EDITOR
            developerModeToggle.gameObject.SetActive(false);            
#endif
            Mng.Instance.SetCurrentAchievement();
            // 게임이 이미 시작되었다면 연출하지 않는다.
            // 다키스트활성화라면 연출하지 않는다.
            if (IsGameActivated)
            {
                MainCamera.transform.localPosition = Vector3.zero;
                if (!Mng.Instance.IsGameDarkest)
                {
                    Sun.localPosition = new Vector3(0f, -178f, 0f);
                    BG_night.color = new Color(1f, 1f, 1f, 0f);
                    BG_night.gameObject.SetActive(false);
                }
            }
            InitializeState();
        }

        // Use this for initialization
        IEnumerator Start()
        {
            Mng.Instance.SetDeveloperMode(developerModeToggle);
            LevelText.text = CurrentLevel.ToString();
            StateEnter(MenuState.enter);
            yield return new WaitForFixedUpdate();
            NewInsigniaNotice.SetActive(Mng.Instance.NewInsigniaNotice);
            InitLevelist();
            StartCoroutine(glitter());
        }

        private IEnumerator glitter()
        {
            float t = 0f;
            while (t <= 1f)
            {
                t += 0.5f * Time.deltaTime;
                Stars1.alpha = !isStarGlittered ? t : 1 - t;
                Stars2.alpha = !isStarGlittered ? 1 - t : t;
                yield return null;
            }
            isStarGlittered = !isStarGlittered;
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(glitter());
        }

        private void Cloud()
        {
            i += 0.05625f * Time.deltaTime;
            if (i >= 1f)
                i = 0f;
            Rect rect = new Rect(new Vector2(i, 0), Vector2.one);
            BG_cloud.uvRect = rect;
        }

        private void InitLevelist()
        {
            Level_List.init();
            IEnumerator Levels = Level_List.GetEnumerator();
            MenuLevel level = MenuLevel.NOVICE;
            while (Levels.MoveNext())
            {
                RectTransform obj = (RectTransform)Levels.Current;
                int objCnt = obj.childCount;
                int contentSize = 0;

                for (int i = 0; i < objCnt; i++)
                {
                    if (i % 4 == 0)
                        contentSize++;
                }
                float size = contentSize * 150f;
                float initHeight = size / 2f;
                int j = 0;
                int n = 0;

                for (int i = 0; i < objCnt; i++)
                {
                    if (i % 4 == 0)
                    {
                        n++;
                        j = 0;
                    }
                    obj.GetChild(i).localPosition = new Vector3(-225f + j * 150f, initHeight - n * 150f + 67f);
                    StageBtn btn = obj.GetChild(i).GetComponent<StageBtn>();
                    btn.numberText.text = (i + 1).ToString();
                    int process = Mng.Instance.EachLevelProcess[level];
                    btn.clearImage.SetActive(process > i);
                    btn.lockImage.SetActive(process < i);
                    btn.numberText.gameObject.SetActive(process >= i);
                    btn.bgImage.raycastTarget = process >= i;
                    j++;
                }
                obj.localPosition = new Vector3(0f, -initHeight);
                obj.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size);
                Mng.Instance.MaxLevel[level] = objCnt;
                level++;
            }
        }

        // Update is called once per frame
        void Update()
        {
            Execute();
            Cloud();
            if (Application.isMobilePlatform)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (Escape != null)
                        Escape();
                }
            }
        }

        void OnDestroy()
        {
            DestroyInstance();
        }
    }
}