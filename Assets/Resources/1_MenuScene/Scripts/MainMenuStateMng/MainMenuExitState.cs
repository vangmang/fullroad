﻿using GameStateLibrary.State;
using System;
using UnityEngine;
using System.Collections;

namespace MainMenu
{
    public class MainMenuExitState : State<MainMenuStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return MainMenuStateMng.MenuState.exit;
            }
        }

        int SceneNum;

        public override void Enter(params object[] o_Params)
        {
            if (o_Params.Length > 0)
                SceneNum = (int)o_Params.GetValue(0);
            StartCoroutine(FadeIn());
        }

        private IEnumerator FadeIn()
        {
            instance.CoverImage.gameObject.SetActive(true);
            Color color = new Color(0f, 0f, 0f, 0f);

            float t = 0f;
            while (t <= 0.997f)
            {
                t += Mathf.Lerp(1f, 0f, t) * 4.5f * Time.deltaTime;
                color.a = Mathf.Lerp(0f, 1f, t);
                instance.CoverImage.color = color;
                yield return null;
            }
            GoogleMng.Instance.DestroyLoginEvent();
            yield return new WaitForFixedUpdate();
            GC.Collect();
            Mng.Instance.LoadLevel(SceneNum);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }
    }
}