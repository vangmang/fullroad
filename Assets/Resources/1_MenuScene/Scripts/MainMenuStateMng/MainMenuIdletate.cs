﻿using GameStateLibrary.State;
using System;
using UnityEngine;
using System.Collections;

namespace MainMenu
{
    public class MainMenuIdletate : State<MainMenuStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return MainMenuStateMng.MenuState.idle;
            }
        }


        public override void Enter(params object[] o_Params)
        {
            instance.Tip.InitTipText();
            instance.Escape = instance.Quit;
            instance.isSceneIdle = true;
            instance.IdleScene.gameObject.SetActive(true);
            instance.Tip.TipTransform.gameObject.SetActive(true);
            StartCoroutine(AlphaFadeOut());
        }


        public override void Execute()
        {
            instance.MainCamera.transform.localPosition =
                Vector3.Lerp(instance.MainCamera.transform.localPosition, Vector3.zero, instance.StageScrollSpeed * Time.deltaTime);            
            instance.MainCamera.orthographicSize =
                Mathf.Lerp(instance.MainCamera.orthographicSize, instance.CameraIdleSize, instance.CameraDeltaSpeed * Time.deltaTime);
        }

        public override void Exit()
        {
            instance.isSceneIdle = false;

            StartCoroutine(AlphaFadeIn());
        }

        private IEnumerator AlphaFadeOut()
        {
            float t = instance.IdleCanvas.alpha;
            while (t <= 1f)
            {
                if (!instance.isSceneIdle)
                    yield break;
                t += instance.StageScrollSpeed * Time.deltaTime;
                instance.IdleCanvas.alpha = t;
                yield return null;
            }
        }

        private IEnumerator AlphaFadeIn()
        {
            float t = instance.IdleCanvas.alpha;
            while(t >= 0f)
            {
                if (instance.isSceneIdle)
                    yield break;
                t -= instance.StageScrollSpeed * Time.deltaTime;
                instance.IdleCanvas.alpha = t;
                yield return null;
            }
            instance.Tip.TipTransform.gameObject.SetActive(false);
            instance.IdleScene.gameObject.SetActive(false);
        }
    }
}