﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class InsigniaBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public MainMenuStateMng mainMenuStateMng;
        public Image insigniaImage;

        void OnEnable()
        {
            if (Mng.Instance.CurrentInsignia == name)
            {
                mainMenuStateMng.Insignia.InsigniaImage.sprite = insigniaImage.sprite;
                mainMenuStateMng.Insignia.InsigniaText.text = name;
            }
            bool process = Mng.Instance.AchievementProcessTable[GoogleMng.Instance.InsigniaIdTable[name]];
            Color color = new Color(1f, 1f, 1f,
                process ? 1f : 0.3f);
            insigniaImage.raycastTarget = process;
            insigniaImage.color = color;
        }

        public void OnPointerClick(PointerEventData e)
        {
            mainMenuStateMng.Insignia.InsigniaImage.sprite = insigniaImage.sprite;
            mainMenuStateMng.Insignia.InsigniaText.text = name;
            Mng.Instance.SaveCurrentInsignia(name);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}