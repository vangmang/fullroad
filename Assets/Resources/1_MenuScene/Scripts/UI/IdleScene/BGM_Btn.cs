﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class BGM_Btn : MonoBehaviour, IPointerClickHandler
    {
        public Image OriginImage;
        public Sprite OffImage;
        public Sprite OnImage;

        // Use this for initialization
        void Start()
        {
            OriginImage.sprite = SoundMng.BGM_Enable ? OnImage : OffImage;
        }

        public void OnPointerClick(PointerEventData e)
        {
            SoundMng.Instance.SetBGM_Enable();
            OriginImage.sprite = SoundMng.BGM_Enable ? OnImage : OffImage;
        }

        void OnDestroy()
        {

        }
    }
}