﻿using UnityEngine;
using UnityEngine.UI;

namespace MainMenu
{
    public class InsigniaMng : MonoBehaviour
    {
        public static InsigniaMng Instance { get; private set; }

        public Image InsigniaImage;
        public Text InsigniaText;
        public GameObject InsigniaList;

        [System.Serializable]
        public struct _InsigniaImages_
        {
            // NOIVCE
            public Sprite BlockThrower;
            public Sprite BlockIdentifier;
            public Sprite TheRoader;
            // NORMAL
            public Sprite FullRoader;
            public Sprite RoadRanger;
            public Sprite RoadFinder;
            // EXPERT
            public Sprite BlockExpert;
            public Sprite BlockConqueror;
            public Sprite BlockPenetrater;
            // MASTER
            public Sprite BlockMaster;
            public Sprite RoadMaster;
            // HIDDEN
            public Sprite TheDarkestRoader;
            public Sprite TheHardcoreRoader;
            public Sprite TheRoadAfflicter;
            public Sprite TheDeepDarkRoader;
            public Sprite TheRoadCreator;
        }
        public _InsigniaImages_ InsigniaImages;

        void Awake()
        {
            Instance = this;
        }

        void Start()
        {
            InsigniaList.SetActive(false);
        }

        void OnDestroy()
        {
            Instance = null;
        }
    }
}