﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class OptionBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public MainMenuStateMng mainMenuStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            mainMenuStateMng.SubState(MainMenuStateMng.MenuState.setting);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }

        void OnDestroy()
        {

        }
    }
}