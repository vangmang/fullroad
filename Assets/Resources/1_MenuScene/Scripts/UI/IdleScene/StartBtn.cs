﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class StartBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public MainMenuStateMng mainMenuStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            mainMenuStateMng.ChangeState(MainMenuStateMng.MenuState.stageSelect);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}