﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class OptionCloseBtn : MonoBehaviour, IPointerClickHandler
    {
        public MainMenuStateMng mainMenuStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            if(mainMenuStateMng.Escape != null)
                mainMenuStateMng.Escape();
        }

        void OnDestroy()
        {

        }
    }
}