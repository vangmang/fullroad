﻿using UnityEngine.EventSystems;

namespace MainMenu
{
    public class NoAdBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {

        // Use this for initialization
        void Start()
        {
            gameObject.SetActive(IAPMng.IsAdEnable);
        }

        public void OnPointerClick(PointerEventData e)
        {
            IAPMng.Instance.BuyProduct_NoAds(() => gameObject.SetActive(IAPMng.IsAdEnable));
        }

        public void OnPointerUp(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerDown(PointerEventData e)
        {
            Release();
        }
    }
}