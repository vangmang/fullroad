﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

namespace MainMenu
{
    public class SFX_Btn : MonoBehaviour, IPointerClickHandler
    {
        public Image OriginImage;
        public Sprite OffImage;
        public Sprite OnImage;

        // Use this for initialization
        void Start()
        {
            OriginImage.sprite = SoundMng.SFX_Enable ? OnImage : OffImage;
        }

        public void OnPointerClick(PointerEventData e)
        {
            SoundMng.Instance.SetSFX_Enable();
            OriginImage.sprite = SoundMng.SFX_Enable ? OnImage : OffImage;
        }

        void OnDestroy()
        {

        }
    }
}