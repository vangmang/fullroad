﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class YoutubeBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        private string url;
        // Use this for initialization
        void Start()
        {
            // TODO: 유튜브로 고칠것
            url = "https://www.facebook.com/SHIPgames/";
        }

        public void OnPointerClick(PointerEventData e)
        {
            Application.OpenURL(url);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}