﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

namespace MainMenu
{
    public class GoogleAchievementBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public Image btnImage;
        public Image Icon;

        void Start()
        {
            GoogleMng.Instance.LoginEvent += (sender, e) =>
            {
                btnImage.raycastTarget = GoogleMng.Instance.IsLogin;
                Icon.raycastTarget = GoogleMng.Instance.IsLogin;
                btnImage.color = new Color(1f, 1f, 1f, GoogleMng.Instance.IsLogin ? 1f : 0.5f);
            };
        }

        public void OnPointerClick(PointerEventData e)
        {
            GoogleMng.Instance.ShowAchievement();
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}