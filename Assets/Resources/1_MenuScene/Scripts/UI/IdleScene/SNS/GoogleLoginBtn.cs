﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

namespace MainMenu
{
    public class GoogleLoginBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public Image btnImage;

        void Start()
        {
            GoogleMng.Instance.LoginEvent += (sender, e) =>
            {
                btnImage.color = new Color(1f, 1f, 1f, GoogleMng.Instance.IsLogin ? 0.5f : 1f);
            };
        }

        public void OnPointerClick(PointerEventData e)
        {
            if(!GoogleMng.Instance.IsLogin)
                GoogleMng.Instance.SignIn();
            else
                GoogleMng.Instance.SignOut();
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}