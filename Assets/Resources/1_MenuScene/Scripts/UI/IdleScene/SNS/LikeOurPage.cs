﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace MainMenu
{
    public class LikeOurPage : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        private string url;
        // Use this for initialization
        void Start()
        {
            url = "https://www.facebook.com/SHIPgames/";
        }

        public void OnPointerClick(PointerEventData e)
        {
            Application.OpenURL(url);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}