﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class InsigniaListOpenBtn : MonoBehaviour, IPointerClickHandler
    {
        public MainMenuStateMng mainMenuStateMng;
        public GameObject InsigniaList;
        public void OnPointerClick(PointerEventData e)
        {
            InsigniaList.SetActive(!InsigniaList.activeSelf);
            Mng.Instance.SetInsigniaNotice(false);
            mainMenuStateMng.NewInsigniaNotice.SetActive(false);
        }
    }
}