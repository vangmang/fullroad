﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class EditBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public MainMenuStateMng mainMenuStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            mainMenuStateMng.ChangeState(MainMenuStateMng.MenuState.exit, 3);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}