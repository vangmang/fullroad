﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

namespace MainMenu
{
    public class StageBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public Text numberText;
        public Image bgImage;
        public GameObject clearImage;
        public GameObject lockImage;

        public MainMenuStateMng.MenuLevel menuLevel;
        public void OnPointerClick(PointerEventData e)
        {
            Mng.Instance.StageInfo.CurrentMenuLevel = menuLevel;
            Mng.Instance.StageInfo.CurrentStageLevelNum = int.Parse(numberText.text);
            Mng.Instance.StageInfo.CurrentInsignia = InsigniaMng.Instance.InsigniaImage;
            MainMenuStateMng.Instance.ChangeState(MainMenuStateMng.MenuState.exit, 4);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}