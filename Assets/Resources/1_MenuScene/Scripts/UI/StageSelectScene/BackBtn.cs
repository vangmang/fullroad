﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainMenu
{
    public class BackBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public MainMenuStateMng mainMenuStateMng;

        void Start()
        {
#if !UNITY_EDITOR
            gameObject.SetActive(false);            
#endif
        }

        public void OnPointerClick(PointerEventData e)
        {
            mainMenuStateMng.ChangeState(MainMenuStateMng.MenuState.idle);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}