﻿using UnityEngine;
using UnityEngine.UI;

namespace MainMenu
{
    public class EachStageCount : MonoBehaviour
    {
        public StageScroll stageScroll;
        public Text currentStageCount;
        public Text maxStageCount;

        // Use this for initialization
        void Start()
        {
            stageScroll.ScrollEvent += (sender, e) =>
            {
                currentStageCount.text = Mng.Instance.EachLevelProcess[MainMenuStateMng.CurrentLevel].ToString();
                maxStageCount.text = Mng.Instance.MaxLevel[MainMenuStateMng.CurrentLevel].ToString();
            };
        }

    }
}