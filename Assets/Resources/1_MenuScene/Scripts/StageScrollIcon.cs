﻿using UnityEngine.UI;
using UnityEngine;

namespace MainMenu
{
    public class StageScrollIcon : MonoBehaviour
    {
        public StageScroll stageScroll;
        public int index;
        public Image OriginImage;
        public Image WhiteImage;
        public Image RedImage;

        // Use this for initialization
        void Start()
        {
            stageScroll.ScrollEvent += (sender, e) =>
            {
                OriginImage.sprite = index == stageScroll.Level ? RedImage.sprite : WhiteImage.sprite;
            };
        }
        
        void OnDestroy()
        {

        }
    }
}