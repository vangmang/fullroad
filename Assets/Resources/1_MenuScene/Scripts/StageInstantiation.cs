﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Collections;
using System.Linq;

namespace MainMenu
{
    public class StageInstantiation : MonoBehaviour
    {
        public MainMenuStateMng.MenuLevel Level;
        private IEnumerator files;

        void Awake()
        {
            StringBuilder path = new StringBuilder();
            path.Append("1_MenuScene/Prefabs/level/").Append(Level.ToString());
            var list = Resources.LoadAll(path.ToString()).ToList();
            list.Sort((file1, file2) =>
            {
                int fileName1 = int.Parse(file1.name.Split('_')[0]);
                int fileName2 = int.Parse(file2.name.Split('_')[0]);
                return fileName1.CompareTo(fileName2);
            });

            files = list.GetEnumerator();            
        }

        // Use this for initialization
        void Start()
        {
            while (files.MoveNext())
            {
                TextAsset xmlText = (TextAsset)files.Current;
                string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(xmlText.name);
                StageBtn stageBtn = Mng.Instance.CreatePrefab(transform, "Stage", E_RESOURCES.E_1_MENUSCENE, Vector3.zero, Quaternion.identity, fileNameWithoutExtension).GetComponent<StageBtn>();
                stageBtn.menuLevel = Level;
                stageBtn.gameObject.layer = LayerMask.NameToLayer(Level.ToString());
            }
        }
    }
}