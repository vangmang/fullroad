﻿using System;
using GameStateLibrary.State;
using UnityEngine;

namespace EditableBlock
{
    public class CharBlockEnterState : State<CharBlockStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return CharBlockStateMng.CharBlockState.enter;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            instance.BlockHistroy.Clear();
            instance.BlockInfo.BlockTransform.localPosition = new Vector3(800f, 0f);
            instance.BlockInfo.BlockImage.color = new Color(1f, 1f, 1f, 0f);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }
    }
}