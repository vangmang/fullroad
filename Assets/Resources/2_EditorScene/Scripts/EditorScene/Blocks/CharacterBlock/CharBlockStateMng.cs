﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace EditableBlock
{
    /// <summary>
    /// 캐릭터 블록
    /// </summary>
    public class CharBlockStateMng : BlockBase
    {
        private static CharBlockStateMng instance = null;
        public static CharBlockStateMng Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType(typeof(CharBlockStateMng)) as CharBlockStateMng;
                return instance;
            }
        }

        private Vector2 prevPos;
        private Vector2 currPos;
        private readonly float movableDelta = 12f;
        private bool hasMoved;
        private bool isAbleToEdit;
        public bool isInitialized;
        public bool isMoving;
        public bool isSelecting;

        [Serializable]
        public struct _CharBlockInfo_
        {
            [SerializeField]
            private float charBlockSpeed;
            public float GetCharBlockSpeed { get { return charBlockSpeed; } }
        }

        public struct _CharSpecialBlockInvoker_
        {
            public bool isAfterBlock;
            public bool isWarpBlock;
            public bool isLiveBlock;
        }
        public event EventHandler CharMovedEvent;
        public _CharSpecialBlockInvoker_ CharSpecialBlockInvoker;
        public _CharBlockInfo_ CharBlockInfo;
        public Stack<BlockIdentifier> BlockHistroy = new Stack<BlockIdentifier>();
        public BlockIdentifier FirstBlock; // 블록이 처음 초기화 되고 다시 원점으로 돌아간 후 초기화 될 수 있다.
                                           // 그럴 때 블록에 텍스트가 남기 때문에 그 텍스트를 지워주기 위한 필드
        private int movedCount;

        public enum CharBlockState
        {
            enter,
            idle,
            move,
            selectblock,
        }

        public void InvokeMovedEvent()
        {
            if (CharMovedEvent != null)
                CharMovedEvent(this, null);
        }

        void Awake()
        {
            InitializeState();
        }

        // Use this for initialization
        void Start()
        {
            isAbleToEdit = false;
            editorSceneStateMng.EnableEditingEvent += (sender, e) =>
            {
                isAbleToEdit = true;
            };
            editorSceneStateMng.DisableEditingEvent += (sender, e) =>
            {
                isAbleToEdit = false;
            };
            StateEnter(CharBlockState.enter);
        }

        // Update is called once per frame
        void Update()
        {
            Execute();
            if (isAbleToEdit)
                checkSlide();
        }

        void checkSlide()
        {
            if (Input.GetMouseButtonDown(0))
                prevPos = Input.mousePosition;
            else if (Input.GetMouseButton(0))
            {
                currPos = Input.mousePosition;
                Vector3 deltaPos = currPos - prevPos;
                Vector3 deltaDirection = deltaPos.normalized;
                float deltaMagnitude = deltaPos.magnitude;
                if (deltaMagnitude > movableDelta && !hasMoved && !isMoving && !isSelecting)
                {
                    hasMoved = true;
                    float x = Mathf.Abs(deltaPos.x);
                    float y = Mathf.Abs(deltaPos.y);
                    Direction direction;
                    if (x > y)
                        direction = deltaPos.x > 0 ? Direction.right : Direction.left;
                    else
                        direction = deltaPos.y > 0 ? Direction.down : Direction.up;
                    if(isInitialized)
                        ChangeState(CharBlockState.move, direction);
                }
                prevPos = currPos;
            }
            else if (Input.GetMouseButtonUp(0))
                hasMoved = false;
        }

        public void ExitStage()
        {
            instance = null;
            isAbleToEdit = false;
            isInitialized = false;
        }

        void OnDestroy()
        {
        }
    }
}