﻿using System;
using GameStateLibrary.State;
using PuzzleGame2DLibrary.Misc;
using System.Collections;
using UnityEngine;

namespace EditableBlock
{
    public class CharBlockMoveState : State<CharBlockStateMng>
    {

        public override Enum GetState
        {
            get
            {
                return CharBlockStateMng.CharBlockState.move;
            }
        }

        private Index2 TargetIndex;
        private BlockIdentifier TargetBlock;

        public override void Enter(params object[] o_Params)
        {
            if (!instance.BlockId)
            {
                instance.ChangeState(CharBlockStateMng.CharBlockState.idle);
                return;
            }
            if (instance.CharSpecialBlockInvoker.isWarpBlock)
            {
                instance.editorSceneStateMng.MessageText.text = "Must point another Block!";
                return;
            }
            Direction direction = (Direction)o_Params.GetValue(0);
            TargetIndex = instance.BlockId.BlockIndex + instance.editorSceneStateMng.DirectionTable[direction];
            if(TargetIndex.x < 0 || TargetIndex.x >= (int)instance.editorSceneStateMng.Difficulty ||
               TargetIndex.y < 0 || TargetIndex.y >= (int)instance.editorSceneStateMng.Difficulty ||
               instance.editorSceneStateMng.BlockIdArr[TargetIndex.x, TargetIndex.y].IsPassed)
            {
                //에프터 블록 설정이 완료되지 않았는데 블록이동이 막히면 상태를 전이하지 않는다.
                if (!instance.CharSpecialBlockInvoker.isAfterBlock)
                    instance.ChangeState(CharBlockStateMng.CharBlockState.idle);
                else
                    instance.editorSceneStateMng.MessageText.text = "Unable to point direction!";
                return;
            }

            instance.editorSceneStateMng.MessageText.text = "Character Move";
            TargetBlock = instance.editorSceneStateMng.BlockIdArr[TargetIndex.x, TargetIndex.y];
            instance.isMoving = true;
            instance.BlockId.Passed(instance);
            //에프터 블록 설정
            if (instance.CharSpecialBlockInvoker.isAfterBlock)
            {
                instance.CharSpecialBlockInvoker.isAfterBlock = false;
                instance.BlockId.AfterBlock.AfterDirection = direction;
            }
            instance.InvokeMovedEvent();
            StartCoroutine(moveToTarget());
        }

        private IEnumerator moveToTarget()
        {
            float t = 0f;
            Vector3 start = instance.BlockInfo.BlockTransform.localPosition;
            while (t <= 1f)
            {
                t += instance.CharBlockInfo.GetCharBlockSpeed * Time.deltaTime;
                instance.BlockInfo.BlockTransform.localPosition =
                    Vector3.Lerp(start, TargetBlock.BlockIdTransform.position, t);
                yield return null;
            }
            instance.BlockId = TargetBlock;
            instance.ChangeState(CharBlockStateMng.CharBlockState.selectblock);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            instance.isMoving = false;
        }
    }
}