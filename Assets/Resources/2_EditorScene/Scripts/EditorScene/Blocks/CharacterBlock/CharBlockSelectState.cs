﻿using System;
using GameStateLibrary.State;
using UnityEngine;

namespace EditableBlock
{
    /// <summary>
    /// 캐릭터 블록이 이동 후 지정 자리에 블록을 선택하는 상태
    /// </summary>
    public class CharBlockSelectState : State<CharBlockStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return CharBlockStateMng.CharBlockState.selectblock;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            instance.isSelecting = true;
            instance.editorSceneStateMng.MessageText.text = "Select the Block";
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            instance.isSelecting = false;
        }
    }
}