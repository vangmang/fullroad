﻿using System;
using GameStateLibrary.State;
using UnityEngine;

namespace EditableBlock
{
    public class CharBlockIdleState : State<CharBlockStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return CharBlockStateMng.CharBlockState.idle;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            instance.editorSceneStateMng.MessageText.text = "Character Idle";
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {

        }
    }
}