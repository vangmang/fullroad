﻿using System;
using GameStateLibrary.StateMng;
using PuzzleGame2DLibrary.Misc;
using UnityEngine.UI;
using UnityEngine;

namespace EditableBlock
{
    public enum Direction
    {
        left,
        right,
        up,
        down,
    }

    /// <summary>
    /// 에디터 블록 베이스
    /// </summary>
    public abstract class BlockBase : StateMng, IEditableBlock
    {
        /// <summary>
        /// 블록정보
        /// </summary>
        [Serializable]
        public struct _BlockInfo_
        {
            public Transform BlockTransform;
            public Image BlockImage;
        }
        public _BlockInfo_ BlockInfo;

        [SerializeField]
        protected EditableBlockType blockType;

        public EditorSceneStateMng editorSceneStateMng;
        public BlockIdentifier BlockId;
        public EditableBlockType GetBlockType { get { return blockType; } }
        public Index2 BlockIndex { get { return BlockId.BlockIndex; } }
    }
}