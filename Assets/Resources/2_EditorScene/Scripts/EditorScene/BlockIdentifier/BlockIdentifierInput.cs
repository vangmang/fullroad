﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public class BlockIdentifierInput : MonoBehaviour,
        IPointerClickHandler,
        IPointerDownHandler,
        IPointerUpHandler
    {

        [SerializeField]
        private BlockIdentifier blockIdentifier;

        public void OnPointerClick(PointerEventData eventData)
        {
            // 워프 블록 선택
            if (CharBlockStateMng.Instance.CharSpecialBlockInvoker.isWarpBlock)
            {
                // 이미 지나온 블록은 선택 불가
                if (blockIdentifier.IsPassed)
                {
                    blockIdentifier.editorSceneStateMng.MessageText.text = "This block is already passed!";
                    return;
                }

                blockIdentifier.editorSceneStateMng.BlockUsage.WarpUsageCount++;
                blockIdentifier.editorSceneStateMng.BlockUsage.WarpUsage.text = blockIdentifier.editorSceneStateMng.BlockUsage.WarpUsageCount.ToString();

                blockIdentifier.editorSceneStateMng.BlockUsage.TotalUsageCount++;
                int percent = (int)(((float)blockIdentifier.editorSceneStateMng.BlockUsage.TotalUsageCount / blockIdentifier.editorSceneStateMng.StageMaxBlocks) * 100);
                blockIdentifier.editorSceneStateMng.BlockUsage.TotalUsagePercent.text = percent.ToString();

                CharBlockStateMng.Instance.BlockId = blockIdentifier;
                CharBlockStateMng.Instance.BlockInfo.BlockTransform.localPosition = blockIdentifier.BlockIdTransform.position;
                CharBlockStateMng.Instance.ChangeState(CharBlockStateMng.CharBlockState.idle);
                CharBlockStateMng.Instance.CharSpecialBlockInvoker.isWarpBlock = false;

                blockIdentifier.WarpBlock.matchedBlockId = BlockIdentifier._WarpBlock_.MatchedBlockId;
                blockIdentifier.WarpBlock.matchedBlockId.Passed(CharBlockStateMng.Instance);
                
                BlockIdentifier._WarpBlock_.MatchedBlockId.WarpBlock.matchedBlockId = blockIdentifier;
                blockIdentifier.WarpBlock.WarpIndex = BlockIdentifier._WarpBlock_.MatchedBlockId.WarpBlock.WarpIndex;
                blockIdentifier.SetBlockImage(EditableBlockType.WarpBlock);
                blockIdentifier.BlockType = EditableBlockType.WarpBlock;
                BlockIdentifier._WarpBlock_.MatchedBlockId = null;
            }
            // 라이브 블록 선택
            else if(CharBlockStateMng.Instance.CharSpecialBlockInvoker.isLiveBlock)
            {
                // 이미 선택된 라이브 블록이면 선택 불가
                if (blockIdentifier.IsLiveBlock)
                {
                    // 선택된 라이브 블록 취소
                    if (blockIdentifier.LiveIndex == BlockIdentifier._LiveBlock_.CountLiveIndex)
                    {
                        BlockIdentifier._LiveBlock_.ChildCount--;
                        blockIdentifier.IsLiveBlock = false;
                        blockIdentifier.LiveIndexText.text = string.Empty;
                        BlockIdentifier._LiveBlock_.MotherLiveBlockId.LiveBlock.BlockIdList.Remove(blockIdentifier);
                    }
                    else 
                        blockIdentifier.editorSceneStateMng.MessageText.text = "This block is already selected!";
                    return;
                }
                // 이미 지나온 블록은 선택 불가
                else if(blockIdentifier.IsPassed)
                {
                    blockIdentifier.editorSceneStateMng.MessageText.text = "This block is already passed!";
                    return;
                }
                // 캐릭터가 위치한 블록은 선택불가
                else if(blockIdentifier.BlockIndex == CharBlockStateMng.Instance.BlockIndex)
                {
                    blockIdentifier.editorSceneStateMng.MessageText.text = "Can't make mother block to child block!";
                    return;
                }
                BlockIdentifier._LiveBlock_.ChildCount++;
                blockIdentifier.IsLiveBlock = true;
                blockIdentifier.LiveIndex = BlockIdentifier._LiveBlock_.CountLiveIndex;
                blockIdentifier.LiveIndexText.text = blockIdentifier.LiveIndex.ToString();
                BlockIdentifier._LiveBlock_.MotherLiveBlockId.LiveBlock.BlockIdList.Add(blockIdentifier);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Invoke("designateStartingPoint", 0.75f);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            CancelInvoke("designateStartingPoint");
        }

        private void designateStartingPoint()
        {
            blockIdentifier.editorSceneStateMng.DesignateStartingPoint(blockIdentifier);
        }
    }
}
