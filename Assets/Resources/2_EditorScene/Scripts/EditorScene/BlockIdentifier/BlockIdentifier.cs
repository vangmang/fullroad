﻿using PuzzleGame2DLibrary.Misc;
using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace EditableBlock
{
    /// <summary>
    /// 블록 식별자.
    /// </summary>
    public sealed class BlockIdentifier : MonoBehaviour
    {
        #region SpecialBlock
        [System.Serializable]
        public struct _AfterBlock_
        {
            [SerializeField]
            private Direction afterDirection;
            public Direction AfterDirection
            {
                get { return afterDirection; }
                set { afterDirection = value; }
            }
        }
        [System.Serializable]
        public class _WarpBlock_
        {
            public static BlockIdentifier MatchedBlockId = null;
            public static int CountWarpIndex = 0;
            public int WarpIndex;
            public BlockIdentifier matchedBlockId;
        }
        [System.Serializable]
        public class _LiveBlock_
        {
            public static BlockIdentifier MotherLiveBlockId = null;
            public static int CountLiveIndex = 0;
            public static int ChildCount = 0;
            public int MotherLiveIndex;
            public Text MotherLiveIndexText;
            public List<BlockIdentifier> BlockIdList = new List<BlockIdentifier>();
        }
        // 라이브 블록에 의해 자식 라이브 블록이 되는 경우
        public int LiveIndex;
        public Text LiveIndexText;

        public _AfterBlock_ AfterBlock;
        public _WarpBlock_ WarpBlock;
        public _LiveBlock_ LiveBlock;
        #endregion

        private bool isPassed;
        public EditableBlockType BlockType = EditableBlockType.None;
        public Text NumText;        // 블록이 지나온 순차를 표시하는 텍스트
        public bool IsLiveBlock;    // 라이브 블록에 의해 설정되는지 여부
        public bool IsPassed { get { return isPassed; } }
        [SerializeField]
        private Index2 blockIndex;
        [SerializeField]
        private Image blockImage;

        [System.Serializable]
        public struct _BlockImage_
        {
            public Sprite NormalBlockImage;
            public Sprite AfterBlockImage;
            public Sprite WarpBlockImage;
            public Sprite LiveBlockImage;
        }
        public _BlockImage_ BlockImage;

        public Image GetBlockImage { get { return blockImage; } }
        [SerializeField]
        private Transform blockIdTransform;
        [System.NonSerialized]
        public EditorSceneStateMng editorSceneStateMng;
        public Transform BlockIdTransform { get { return blockIdTransform; } }

        public Index2 BlockIndex { get { return blockIndex; } }
        public Index2 SetBlockIndex { set { blockIndex = value; } }

        IEnumerator Start()
        {
            transform.localPosition =
                new Vector2(EditorSceneStateMng.BlockGap * blockIndex.x - editorSceneStateMng.RevValue,
                            EditorSceneStateMng.BlockGap * -blockIndex.y + editorSceneStateMng.RevValue);
            float t = 0f;
            Vector3 start = new Vector3(0.65f, 0.65f);
            while (t <= 1f)
            {
                t += 2f * Time.deltaTime;
                transform.localScale =
                    Vector3.Lerp(start, Vector3.one, t);
                yield return null;
            }
        }

        public void SetBlock(CharBlockStateMng charBlockStateMng, EditableBlockType blockType, Image blockImage)
        {
            BlockType = blockType;
            this.blockImage.sprite = blockImage.sprite;
            switch (blockType)
            {
                case EditableBlockType.AfterBlock:
                    editorSceneStateMng.BlockUsage.AfterUsageCount++;
                    editorSceneStateMng.BlockUsage.AfterUsage.text = editorSceneStateMng.BlockUsage.AfterUsageCount.ToString();
                    setAfterBlock(charBlockStateMng);
                    break;
                case EditableBlockType.WarpBlock:
                    editorSceneStateMng.BlockUsage.WarpUsageCount++;
                    editorSceneStateMng.BlockUsage.WarpUsage.text = editorSceneStateMng.BlockUsage.WarpUsageCount.ToString();
                    setWarpBlock(charBlockStateMng, ++_WarpBlock_.CountWarpIndex);
                    break;
                case EditableBlockType.LiveBlock:
                    editorSceneStateMng.BlockUsage.LiveUsageCount++;
                    editorSceneStateMng.BlockUsage.LiveUsage.text = editorSceneStateMng.BlockUsage.LiveUsageCount.ToString();
                    setLiveBlock(charBlockStateMng);
                    break;
                default:
                    charBlockStateMng.ChangeState(CharBlockStateMng.CharBlockState.idle);
                    return;
            }
            editorSceneStateMng.BlockUsage.TotalUsageCount++;
            int percent = (int)(((float)editorSceneStateMng.BlockUsage.TotalUsageCount / editorSceneStateMng.StageMaxBlocks) * 100);
            editorSceneStateMng.BlockUsage.TotalUsagePercent.text = percent.ToString();
        }

        public void SetBlockImage(EditableBlockType blockType)
        {
            Sprite image = BlockImage.NormalBlockImage;
            switch(blockType)
            {
                case EditableBlockType.NormalBlock: image = BlockImage.NormalBlockImage; break;
                case EditableBlockType.AfterBlock: image = BlockImage.AfterBlockImage; break;
                case EditableBlockType.WarpBlock: image = BlockImage.WarpBlockImage; break;
                case EditableBlockType.LiveBlock: image = BlockImage.LiveBlockImage; break;
                default: image = BlockImage.NormalBlockImage; break;
            }

            blockImage.sprite = image;
        }

        private void setAfterBlock(CharBlockStateMng charBlockStateMng)
        {
            charBlockStateMng.isSelecting = false;
            charBlockStateMng.editorSceneStateMng.MessageText.text = "Point the Direction";
            charBlockStateMng.CharSpecialBlockInvoker.isAfterBlock = true;
        }

        private void setWarpBlock(CharBlockStateMng charBlockStateMng, int warpIndex)
        {
            charBlockStateMng.isSelecting = false;
            WarpBlock.WarpIndex = warpIndex;
            _WarpBlock_.MatchedBlockId = this;
            NumText.text = (editorSceneStateMng.Sequence + 1).ToString();
            charBlockStateMng.editorSceneStateMng.MessageText.text = "Point another Block to warp";
            charBlockStateMng.CharSpecialBlockInvoker.isWarpBlock = true;
        }

        private void setLiveBlock(CharBlockStateMng charBlockStateMng)
        {
            LiveBlock.MotherLiveIndex = ++_LiveBlock_.CountLiveIndex;
            _LiveBlock_.MotherLiveBlockId = this;
            LiveBlock.MotherLiveIndexText.text = LiveBlock.MotherLiveIndex.ToString();
            charBlockStateMng.editorSceneStateMng.MessageText.text = "Select blocks to make them alive";
            charBlockStateMng.CharSpecialBlockInvoker.isLiveBlock = true;
        }

        public void Passed()
        {
            isPassed = true;
        }
        /// <summary>
        /// 블록을 지나치면 호출되는 메소드
        /// </summary>
        public void Passed(CharBlockStateMng charBlockStateMng)
        {
            isPassed = true;
            editorSceneStateMng.Sequence++;
            NumText.text = editorSceneStateMng.Sequence.ToString();
            charBlockStateMng.BlockHistroy.Push(this);
        }

        public void MakeBlockEmpty()
        {
            MakeBlockEmpty(CharBlockStateMng.Instance);
        }

        /// <summary>
        /// 블록에 채워진 자리를 지워주는 메소드
        /// </summary>
        public void MakeBlockEmpty(CharBlockStateMng charBlockStateMng)
        {
            if (charBlockStateMng.BlockHistroy.Count == 0)
                return;
            bool wasNormal = false;
            switch (BlockType)
            {
                case EditableBlockType.AfterBlock:
                    editorSceneStateMng.BlockUsage.AfterUsageCount--;
                    editorSceneStateMng.BlockUsage.AfterUsage.text = editorSceneStateMng.BlockUsage.AfterUsageCount.ToString();
                    charBlockStateMng.ChangeState(CharBlockStateMng.CharBlockState.idle);
                    break;
                case EditableBlockType.WarpBlock:
                    editorSceneStateMng.BlockUsage.WarpUsageCount--;
                    editorSceneStateMng.BlockUsage.WarpUsage.text = editorSceneStateMng.BlockUsage.WarpUsageCount.ToString();
                    BlockIdentifier matchedBlock = WarpBlock.matchedBlockId;
                    if (matchedBlock != null)
                    {
                        matchedBlock.WarpBlock.matchedBlockId = null;
                        WarpBlock.matchedBlockId = null;
                        _WarpBlock_.CountWarpIndex--;
                        charBlockStateMng.BlockId = this;
                        charBlockStateMng.BlockInfo.BlockTransform.localPosition = BlockIdTransform.position;
                    }
                    else
                        charBlockStateMng.CharSpecialBlockInvoker.isWarpBlock = false;                    
                    break;
                case EditableBlockType.LiveBlock:
                    editorSceneStateMng.BlockUsage.LiveUsageCount--;
                    editorSceneStateMng.BlockUsage.LiveUsage.text = editorSceneStateMng.BlockUsage.LiveUsageCount.ToString();
                    LiveBlock.BlockIdList.ForEach((liveBlock) =>
                    {
                        liveBlock.IsLiveBlock = false;
                        liveBlock.LiveIndex = 0;
                        liveBlock.LiveIndexText.text = string.Empty;
                    });
                    LiveBlock.MotherLiveIndex = 0;
                    LiveBlock.MotherLiveIndexText.text = string.Empty;
                    _LiveBlock_.CountLiveIndex--;
                    break;
                default: wasNormal = true; break;
            }
            if (BlockType != EditableBlockType.NormalBlock)
            {
                editorSceneStateMng.BlockUsage.TotalUsageCount--;
                int percent = (int)(((float)editorSceneStateMng.BlockUsage.TotalUsageCount / editorSceneStateMng.StageMaxBlocks) * 100);
                editorSceneStateMng.BlockUsage.TotalUsagePercent.text = percent.ToString();
            }
            editorSceneStateMng.Sequence--;
            NumText.text = string.Empty;

            SetBlockImage(EditableBlockType.NormalBlock);
            BlockIdentifier blockId = charBlockStateMng.BlockHistroy.Pop();
            blockId.isPassed = false;

            charBlockStateMng.BlockInfo.BlockTransform.localPosition = blockId.blockIdTransform.position;
            charBlockStateMng.BlockId = blockId;
            BlockType = EditableBlockType.None;
            switch (blockId.BlockType)
            {
                case EditableBlockType.AfterBlock:
                    blockId.setAfterBlock(charBlockStateMng);
                    break;
                case EditableBlockType.WarpBlock:
                    if (!wasNormal)
                    {
                        if (blockId.WarpBlock.matchedBlockId == null)
                            blockId.setWarpBlock(charBlockStateMng, ++_WarpBlock_.CountWarpIndex);
                        else
                            charBlockStateMng.ChangeState(CharBlockStateMng.CharBlockState.idle);
                        WarpBlock.WarpIndex = 0;
                    }
                    break;
                default:
                    charBlockStateMng.ChangeState(CharBlockStateMng.CharBlockState.idle);
                    break;
            }
        }

        void OnDestroy()
        {

        }
    }
}