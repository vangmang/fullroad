﻿using UnityEngine;
using PuzzleGame2DLibrary.Misc;

namespace EditableBlock
{
    public enum EditableBlockType
    {
        None,
        Character,
        NormalBlock,
        AfterBlock,
        WarpBlock,
        LiveBlock,
    }

    public interface IEditableBlock
    {
        EditableBlockType GetBlockType { get; }
        Index2 BlockIndex { get; }
    }

    public interface IEditableBlock<T> : IEditableBlock
        where T : class
    {
        T BlockInstance { get; }
        void BlockAction(params object[] o_Params);
    }
}