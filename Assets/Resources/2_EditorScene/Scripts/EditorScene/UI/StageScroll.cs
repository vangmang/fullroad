﻿using UnityEngine;

namespace EditableBlock
{
    public class StageScroll : MonoBehaviour
    {
        public EditorSceneStateMng editorSceneStateMng;
        public Transform Content;

        private Vector3 prevPos;
        private Vector3 currPos;
        private float enableScroll;
        private bool isActivated;


        void Start()
        {
            enableScroll = 23f;
            editorSceneStateMng.LevelText.text = editorSceneStateMng.Difficulty.ToString();
        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                prevPos = Input.mousePosition;
            }
            else if(Input.GetMouseButton(0))
            {
                currPos = Input.mousePosition;
                Vector3 point = currPos - prevPos;
                prevPos = currPos;
                float magnitude = point.x;
                if(Mathf.Abs(magnitude) >= enableScroll && !isActivated && editorSceneStateMng.IsAbleToEdit)
                {
                    isActivated = true;
                    if (magnitude > 0)
                        editorSceneStateMng.Difficulty--;
                    else
                        editorSceneStateMng.Difficulty++;
                    bool isAbleToScroll = true;
                    if (editorSceneStateMng.Difficulty < EditorSceneStateMng.EditorStage.NOVICE || editorSceneStateMng.Difficulty > EditorSceneStateMng.EditorStage.MASTER)
                        isAbleToScroll = false;

                    editorSceneStateMng.Difficulty = 
                        (EditorSceneStateMng.EditorStage)Mathf.Clamp((int)editorSceneStateMng.Difficulty, (int)EditorSceneStateMng.EditorStage.NOVICE, (int)EditorSceneStateMng.EditorStage.MASTER);
                    editorSceneStateMng.LevelText.text = editorSceneStateMng.Difficulty.ToString();
                    if(isAbleToScroll)
                        editorSceneStateMng.GenerateStageToSelect();
                }
            }
            else
            {
                isActivated = false;
            }
        }
    }
}