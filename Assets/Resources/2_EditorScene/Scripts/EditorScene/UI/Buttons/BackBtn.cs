﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public class BackBtn : EditorButtonBase, IPointerClickHandler
    {

        public void OnPointerClick(PointerEventData e)
        {
            if (IsClickable && editorSceneStateMng.EscapeBtn != null &&
                !CharBlockStateMng.Instance.isSelecting && editorSceneStateMng.IsAbleToEdit)
                editorSceneStateMng.EscapeBtn();
        }
    }
}