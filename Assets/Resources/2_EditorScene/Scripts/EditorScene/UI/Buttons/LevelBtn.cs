﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;
using UnityEngine;
using System;

namespace EditableBlock
{
    public class LevelBtn : MonoBehaviour,
        IPointerClickHandler
    {
        public Text LevelNameText;
        public LoadLevelList levelList;
        public EditorSceneStateMng editorSceneStateMng;

        public Image BtnImage;
        public Text BtnText;
        public Transform DeleteBtn;

        public string LevelXml { get; private set; }
        private bool isReadyToLoad;
        public static EventHandler btnClickedEvent = null; // 리스트 내 모든 요소들이 삭제될 때 null로 배정됨.

        void Start()
        {
            StringBuilder sb = new StringBuilder(LevelNameText.text);
            sb.Append(".xml");
            LevelXml = sb.ToString();
            isReadyToLoad = false;

            btnClickedEvent += SendMessage;
        }

        private void SendMessage(object sender, EventArgs e)
        {
            if (sender.Equals(this))
                return;
            else
            {
                if (isReadyToLoad)
                {
                    isReadyToLoad = false;
                    var pos = BtnText.transform.localPosition;
                    pos.x = 0f;
                    BtnText.transform.localPosition = pos;
                    BtnImage.color = new Color(1f, 1f, 1f, 0.5f);
                    DeleteBtn.gameObject.SetActive(false);
                }
            }
        }

        // 버튼을 클릭하면 로드
        public void OnPointerClick(PointerEventData e)
        {
            if(btnClickedEvent != null)
                btnClickedEvent(this, new EventArgs());
            if (isReadyToLoad)
                editorSceneStateMng.ChangeState(EditorSceneStateMng.EditorSceneState.generate, LevelXml);
            isReadyToLoad = !isReadyToLoad;
            var pos = BtnText.transform.localPosition;
            pos.x = -46f;
            BtnText.transform.localPosition = pos;
            BtnImage.color = new Color(1f, 1f, 1f, 0.9f);
            DeleteBtn.gameObject.SetActive(true);
        }

        void OnDestroy()
        {
            btnClickedEvent -= SendMessage;
        }
    }
}