﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using UnityEngine;

namespace EditableBlock
{
    public class BlockBackBtn : EditorButtonBase, IPointerClickHandler
    {
        public CharBlockStateMng charBlockStateMng;
        public Image btnImage;

        void Start()
        {
            charBlockStateMng.CharMovedEvent += (sender, e) =>
            {
                btnImage.raycastTarget = false;
                StartCoroutine(ResetRaycast());
            };
        }

        private IEnumerator ResetRaycast()
        {
            yield return new WaitForFixedUpdate();
            yield return new WaitUntil(() => !charBlockStateMng.isMoving);
            btnImage.raycastTarget = true;
        }

        public void OnPointerClick(PointerEventData e)
        {
            try
            {
                if (IsClickable && !charBlockStateMng.isSelecting)
                {
                    charBlockStateMng.BlockId.MakeBlockEmpty(charBlockStateMng);
                }
            }
            catch (System.NullReferenceException)
            {
                EditorSceneStateMng.Instance.MessageText.text = "Designate starting point first";
            }
        }
    }
}