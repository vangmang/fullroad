﻿using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace EditableBlock
{
    public class BlockSelectionBtn : EditorButtonBase, IPointerClickHandler
    {
        public CharBlockStateMng charBlockStateMng;
        public EditableBlockType blockType;
        public Image blockImage;
        bool isClicked;

        public void OnPointerClick(PointerEventData e)
        {
            if (charBlockStateMng.BlockHistroy.Count == editorSceneStateMng.StageMaxBlocks + 1)
                return;
            // 만약 남은 타일의 개수가 (max - 1)과 같고 스페셜 블록이라면 아무일 도 하지 않는다.
            else if (charBlockStateMng.BlockHistroy.Count == editorSceneStateMng.StageMaxBlocks &&
                     blockType != EditableBlockType.NormalBlock)
            {
                editorSceneStateMng.MessageText.text = "Now you can place normal block only.";
                return;
            }
            if (IsClickable &&
                !charBlockStateMng.CharSpecialBlockInvoker.isAfterBlock &&
                !charBlockStateMng.CharSpecialBlockInvoker.isWarpBlock &&
                charBlockStateMng.isSelecting)
            {
                isClicked = !isClicked;
                if (blockType == EditableBlockType.LiveBlock)
                {
                    if (!isClicked)
                    {
                        if (BlockIdentifier._LiveBlock_.ChildCount == 0)
                        {
                            if (BlockIdentifier._LiveBlock_.ChildCount == 0)
                                CharBlockStateMng.Instance.BlockId.MakeBlockEmpty();
                            editorSceneStateMng.MessageText.text = "Must have at least one block. Try again.";
                            charBlockStateMng.isSelecting = false;
                            BlockIdentifier._LiveBlock_.MotherLiveBlockId = null;
                            charBlockStateMng.CharSpecialBlockInvoker.isLiveBlock = false;
                            return;
                        }
                        BlockIdentifier._LiveBlock_.ChildCount = 0;
                        charBlockStateMng.isSelecting = false;
                        charBlockStateMng.CharSpecialBlockInvoker.isLiveBlock = false;
                        charBlockStateMng.ChangeState(CharBlockStateMng.CharBlockState.idle);
                        BlockIdentifier._LiveBlock_.MotherLiveBlockId = null;
                        return;
                    }
                }
                else if (charBlockStateMng.CharSpecialBlockInvoker.isLiveBlock)
                    return;
                charBlockStateMng.BlockId.SetBlock(charBlockStateMng, blockType, blockImage);
            }
        }
    }
}