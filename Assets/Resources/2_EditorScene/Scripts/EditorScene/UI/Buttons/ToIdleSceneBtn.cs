﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public class ToIdleSceneBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {

        public EditorSceneStateMng editorSceneStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            CharBlockStateMng.Instance.ChangeState(CharBlockStateMng.CharBlockState.enter);
            editorSceneStateMng.Popup.PopupTransform.gameObject.SetActive(false);
            editorSceneStateMng.ChangeState(EditorSceneStateMng.EditorSceneState.idle);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}
