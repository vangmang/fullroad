﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public class SaveCancelBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public EditorSceneStateMng editorSceneStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            editorSceneStateMng.SavePopup.PopupTransform.gameObject.SetActive(false);
            editorSceneStateMng.EscapeBtn = editorSceneStateMng.ToIdleScene;
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}