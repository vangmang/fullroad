﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public class CancelBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public EditorSceneStateMng editorSceneStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            editorSceneStateMng.Popup.PopupTransform.gameObject.SetActive(false);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}