﻿using UnityEngine.EventSystems;
using UnityEngine;
using System.IO;
using System.Text;

namespace EditableBlock
{
    public class DeleteLevelBtn : MonoBehaviour, IPointerClickHandler
    {
        public LevelBtn levelBtn;

        public void OnPointerClick(PointerEventData e)
        {
            LoadLevelList.deletedLevel = levelBtn;
            if (Mng.IsDeveloperMode)
            {
                StringBuilder sb = new StringBuilder(Application.dataPath);
                sb.Append("/Resources/1_MenuScene/Prefabs/level/").Append(EditorSceneStateMng.Instance.Difficulty.ToString()).Append("/").Append(levelBtn.LevelXml);
                FileInfo fi = new FileInfo(sb.ToString());
                try
                {
                    fi.Delete();
#if UNITY_EDITOR
                    UnityEditor.AssetDatabase.Refresh();
#endif
                }
                catch (IOException _e)
                {
                    Debug.Log(_e.Message);
                }
            }
            else
                AndroidFileMng.Instance.DeleteLevelInFile(EditorSceneStateMng.Instance.Difficulty.ToString(), levelBtn.LevelXml);
            levelBtn.levelList.RepositioningAfterDeleteLevel();
        }
    }
}