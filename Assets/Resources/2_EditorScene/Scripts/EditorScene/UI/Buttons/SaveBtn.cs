﻿using UnityEngine.EventSystems;
using System.IO;
using System.Xml;
using UnityEngine;
using System.Text;

namespace EditableBlock
{
    /// <summary>
    /// 에디터에서 수정한 스테이지를 저장하는 버튼
    /// </summary>
    public sealed class SaveBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public EditorSceneStateMng editorSceneStateMng;
        public Animation warningTextAnim;

        public void OnPointerClick(PointerEventData e)
        {
            if (!Mng.IsDeveloperMode)
                if (editorSceneStateMng.SavePopup.PopupText.text.Length > 15)
                {
                    warningTextAnim.Play();
                    AndroidFullRoadPlugin.Instance.Vibrate(100);
                    return;
                }

            save();
            editorSceneStateMng.EscapeBtn = editorSceneStateMng.ToIdleScene;
        }

        private void save()
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));
            XmlElement stageInfo = doc.CreateElement("StageInfo");
            doc.AppendChild(stageInfo);

            stageInfo.SetAttribute("level", editorSceneStateMng.Difficulty.ToString());

            try
            {
                stageInfo.SetAttribute("curr_x", CharBlockStateMng.Instance.BlockIndex.x.ToString());
                stageInfo.SetAttribute("curr_y", CharBlockStateMng.Instance.BlockIndex.y.ToString());
            }
            catch (System.NullReferenceException)
            {
                stageInfo.SetAttribute("curr_x", "0");
                stageInfo.SetAttribute("curr_y", "0");

            }
            stageInfo.SetAttribute("isCharInitialized", CharBlockStateMng.Instance.isInitialized.ToString());

            int length = (int)editorSceneStateMng.Difficulty;
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    BlockIdentifier blockId = editorSceneStateMng.BlockIdArr[j, i];
                    XmlElement block = doc.CreateElement("block");
                    block.SetAttribute("sequence", blockId.NumText.text);
                    block.SetAttribute("type", blockId.BlockType.ToString());
                    block.SetAttribute("x", blockId.BlockIndex.x.ToString());
                    block.SetAttribute("y", blockId.BlockIndex.y.ToString());
                    switch (blockId.BlockType)
                    {
                        case EditableBlockType.AfterBlock: block.SetAttribute("direction", blockId.AfterBlock.AfterDirection.ToString()); break;
                        case EditableBlockType.WarpBlock: block.SetAttribute("warpIndex", blockId.WarpBlock.WarpIndex.ToString()); break;
                        case EditableBlockType.LiveBlock: block.SetAttribute("motherliveIndex", blockId.LiveBlock.MotherLiveIndex.ToString()); break;
                    }
                    if (blockId.IsLiveBlock)
                        block.SetAttribute("liveIndex", blockId.LiveIndex.ToString());
                    stageInfo.AppendChild(block);
                }
            }

            StringBuilder sb = new StringBuilder(editorSceneStateMng.SavePopup.PopupText.text);
            editorSceneStateMng.LevelNameText.text = sb.ToString();
            sb.Append(".xml");

            if (Mng.IsDeveloperMode) // PC
            {
                StringBuilder path = new StringBuilder(Application.dataPath);
                path.Append("/Resources/1_MenuScene/Prefabs/level/").Append(editorSceneStateMng.Difficulty.ToString()).Append("/").Append(sb.ToString());
                File.WriteAllText(path.ToString(), doc.OuterXml, Encoding.UTF8);
            }
            else // Android
                AndroidFileMng.Instance.CreateLevelToFile(doc, editorSceneStateMng.Difficulty.ToString(), sb.ToString());
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
            editorSceneStateMng.SavePopup.PopupTransform.gameObject.SetActive(false);
        }


        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}