﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public abstract class EditorButtonBase : BtnMng, IPointerDownHandler, IPointerUpHandler
    {
        public EditorSceneStateMng editorSceneStateMng;
        public bool IsClickable { get; protected set; }

        void Awake()
        {
            IsClickable = true;
            editorSceneStateMng.EnableEditingEvent += (sender, e) =>
            {
                IsClickable = true;
            };
            editorSceneStateMng.DisableEditingEvent += (sender, e) =>
            {
                IsClickable = false;
            };
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}