﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    // 이놈과 팝업 버튼들은 예외적으로 버튼베이스를 상속하지 않는다.
    public class TutorialBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public EditorSceneStateMng editorSceneStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            if(!editorSceneStateMng.isTutorial && !CharBlockStateMng.Instance.isSelecting)
                editorSceneStateMng.SubState(EditorSceneStateMng.EditorSceneState.tutorial);            
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }

        void OnDestroy()
        {

        }
    }
}