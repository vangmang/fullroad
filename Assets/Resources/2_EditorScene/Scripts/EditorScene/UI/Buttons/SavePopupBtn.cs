﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public class SavePopupBtn : EditorButtonBase, IPointerClickHandler
    {

        public KeyboardEnable keyboardEnable;
        public void OnPointerClick(PointerEventData e)
        {
            if (IsClickable && !CharBlockStateMng.Instance.isSelecting &&
                !CharBlockStateMng.Instance.CharSpecialBlockInvoker.isAfterBlock &&
                !CharBlockStateMng.Instance.CharSpecialBlockInvoker.isLiveBlock &&
                !CharBlockStateMng.Instance.CharSpecialBlockInvoker.isWarpBlock)
            {
                editorSceneStateMng.SavePopup.PopupTransform.gameObject.SetActive(true);
                keyboardEnable.ActivateKeyboard();
                editorSceneStateMng.EscapeBtn = () =>
                {
                    editorSceneStateMng.SavePopup.PopupTransform.gameObject.SetActive(false);
                    editorSceneStateMng.EscapeBtn = editorSceneStateMng.ToIdleScene;
                };
            }
        }
    }
}
