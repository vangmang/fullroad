﻿using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine;
using System.Text;

namespace EditableBlock
{
    public class KeyboardEnable : MonoBehaviour, IPointerClickHandler
    {

        public EditorSceneStateMng editorSceneStateMng;
        private TouchScreenKeyboard keyboard;
        public TouchScreenKeyboard Keyboard { get { return keyboard; } }

        void Awake()
        {
            keyboard = new TouchScreenKeyboard("", TouchScreenKeyboardType.Default, true, false, false, false, "");
        }

        public void OnPointerClick(PointerEventData e)
        {
            ActivateKeyboard();
        }

        public void ActivateKeyboard()
        {
            if (Mng.IsDeveloperMode)
            {
                StartCoroutine(invokePCKeyboard());
            }
            else {
                if (!keyboard.active)
                    TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
                StartCoroutine(invokeTouchScreenKeyboard());
            }
        }
        
        private IEnumerator invokePCKeyboard()
        {
            StringBuilder sb = new StringBuilder();
            while(!Input.GetKeyDown(KeyCode.Return))
            {
                if (Input.anyKeyDown)
                {
                    if (Input.GetKeyDown(KeyCode.Backspace))
                    {
                        if (sb.Length > 0)
                            sb.Remove(sb.Length - 1, 1);
                    }
                    else
                        sb.Append(Input.inputString);
                    string stageName = sb.ToString();
                    if(stageName != string.Empty)
                        editorSceneStateMng.SavePopup.PopupText.text = stageName;
                }
                yield return null;
            }
        }

        private IEnumerator invokeTouchScreenKeyboard()
        {
            yield return new WaitUntil(() => keyboard.active);
            yield return new WaitUntil(() => keyboard.done);
            if (keyboard.text != string.Empty)
            {
                editorSceneStateMng.SavePopup.PopupText.text = keyboard.text;
            }
        }
    }
}