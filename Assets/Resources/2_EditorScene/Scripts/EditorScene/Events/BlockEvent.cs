﻿using System;

namespace EditableBlock
{
    public sealed class BlockEvent : EventArgs
    {
        public IEditableBlock Target { get; private set; }

        public BlockEvent(IEditableBlock target)
        {
            Target = target;
        }
    }
}