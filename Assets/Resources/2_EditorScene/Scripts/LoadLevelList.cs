﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EditableBlock
{
    /// <summary>
    /// IdleScene에서 해당 레벨의 Load버튼을 클릭하면 해당 레벨의 스테이지 리스트를 관리하는 스크립트
    /// </summary>
    public class LoadLevelList : MonoBehaviour
    {
        public EditorSceneStateMng editorSceneStateMng;
        private IEnumerator levelNameIterator;
        private List<LevelBtn> instantiatedList;
        private readonly float elementGap = 122f;
        public static LevelBtn deletedLevel = null;
        public RectTransform Content;

        void Start()
        {
            instantiatedList = new List<LevelBtn>();
        }

        public void LoadLevelArr()
        {
            StartCoroutine(fixedInstantiate());
        }

        private IEnumerator fixedInstantiate()
        {
            if(Mng.IsDeveloperMode)
            {
                DirectoryInfo di = new DirectoryInfo(Application.dataPath + "/Resources/1_MenuScene/Prefabs/level/" + editorSceneStateMng.Difficulty.ToString());
                var list = di.GetFiles().ToList();
                list.Sort((file1, file2) =>
                {
                    int fileName1 = int.Parse(file1.Name.Split('_')[0]);
                    int fileName2 = int.Parse(file2.Name.Split('_')[0]);
                    return fileName1.CompareTo(fileName2);
                });
                levelNameIterator = list.GetEnumerator();                                
            }
            else
                levelNameIterator = AndroidFileMng.Instance.FileCountInLevel(editorSceneStateMng.Difficulty.ToString()).GetEnumerator();

            if (levelNameIterator == null)
                yield break;
            yield return new WaitForFixedUpdate();
            while (levelNameIterator.MoveNext())
            {
                string name = string.Empty;
                if (Mng.IsDeveloperMode)
                {
                    string name_for_check = ((FileInfo)levelNameIterator.Current).Name;
                    if (name_for_check.Contains("meta"))
                        continue;
                    name = Path.GetFileNameWithoutExtension(name_for_check);
                }
                else
                    name = (string)levelNameIterator.Current;
                LevelBtn element =
                    Mng.Instance.CreatePrefab("Canvas/LoadScene/List/Viewport/Content", 
                                              "LevelElement", 
                                              E_RESOURCES.E_2_EDITORSCENE, 
                                              Vector3.zero, 
                                              Quaternion.identity, name).GetComponent<LevelBtn>();
                string fixedName = Path.GetFileNameWithoutExtension(name);
                element.LevelNameText.text = fixedName;
                element.levelList = this;
                element.editorSceneStateMng = editorSceneStateMng;
                instantiatedList.Add(element);
            }
            SetContentSize();
        }

        public void RepositioningAfterDeleteLevel()
        {
            StartCoroutine(invokeRepositioningAfterDeleteLevel());
        }

        private IEnumerator invokeRepositioningAfterDeleteLevel()
        {
            yield return new WaitForFixedUpdate();
            if (deletedLevel != null)
            {
                instantiatedList.Remove(deletedLevel);
                Destroy(deletedLevel.gameObject);
            }
            SetContentSize();
        }

        // 콘텐츠 사이즈와 레벨 요소들의 포지션을 초기화 시켜준다
        public void SetContentSize()
        {
            float maxSize = instantiatedList.Count * elementGap;
            float revSize = maxSize / 2f - 45f;
            int i = 0;
            foreach (LevelBtn obj in instantiatedList)
            {
                obj.transform.localPosition = new Vector3(0f, revSize + (i * -elementGap));
                i++;
            }
            Content.transform.localPosition = new Vector3(0f, revSize);
            Content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, maxSize + 45f);
        }

        public void DeleteLoadedLevelArr()
        {
            foreach (LevelBtn obj in instantiatedList)
                Destroy(obj.gameObject);
            LevelBtn.btnClickedEvent = null;
            instantiatedList.Clear();
        }

        void OnDestroy()
        {

        }
    }
}
