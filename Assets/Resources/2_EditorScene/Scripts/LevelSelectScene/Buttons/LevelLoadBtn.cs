﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock
{
    public class LevelLoadBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public EditorSceneStateMng editorSceneStateMng;
        public LoadLevelList loadLevelList;

        public void OnPointerClick(PointerEventData e)
        {
            if (editorSceneStateMng.IsAbleToEdit)
            {
                editorSceneStateMng.ChangeState(EditorSceneStateMng.EditorSceneState.load);
                loadLevelList.LoadLevelArr();
            }
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}