﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace EditableBlock {
    public sealed class LevelSelectBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {

        public EditorSceneStateMng editorSceneStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            if(editorSceneStateMng.IsAbleToEdit)
                editorSceneStateMng.ChangeState(EditorSceneStateMng.EditorSceneState.generate);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}
