﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

namespace EditableBlock
{
    public class EditorSceneExitState : State<EditorSceneStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return EditorSceneStateMng.EditorSceneState.exit;  
            }
        }

        public override void Enter(params object[] o_Params)
        {
            StartCoroutine(FadeIn());
        }

        private IEnumerator FadeIn()
        {
            instance.CoverImage.gameObject.SetActive(true);
            Color color = new Color(0f, 0f, 0f, 0f);

            float t = 0f;
            while (t <= 0.997f)
            {
                t += Mathf.Lerp(1f, 0f, t) * 4.5f * Time.deltaTime;
                color.a = Mathf.Lerp(0f, 1f, t);
                instance.CoverImage.color = color;
                yield return null;
            }
            AdMobManager.Instance.HideBanner();
            Mng.Instance.LoadLevel(2);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }

        void OnDestroy()
        {

        }
    }
}
