﻿using GameStateLibrary.State;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace EditableBlock
{
    public class EditorSceneTutorialState : State<EditorSceneStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return EditorSceneStateMng.EditorSceneState.tutorial;
            }
        }

        private int tutorialSequence;

        public override void Enter(params object[] o_Params)
        {
            instance.isTutorial = true;
            tutorialSequence = 0;
            instance.Tutorial.PopupTransform.gameObject.SetActive(true);
            instance.InvokeDisableEditing();
            StartCoroutine(invokeTutorial());
        }

        private IEnumerator invokeTutorial()
        {
            instance.Tutorial.MessageBox.localPosition = instance.MessageBoxPosArr[0];
            foreach (string text in instance.MessageTextList)
            {
                yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
                instance.Tutorial.MessageText.text = text;
                Vector2 size = instance.MessageBoxScaleArr[tutorialSequence];
                instance.Tutorial.MessageBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
                instance.Tutorial.MessageBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
                instance.Tutorial.MessageBox.localPosition = instance.MessageBoxPosArr[tutorialSequence];
                tutorialSequence++;
                yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
            }
            yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
            instance.RemoveState(EditorSceneStateMng.EditorSceneState.tutorial);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {

            instance.Tutorial.PopupTransform.gameObject.SetActive(false);
            instance.Tutorial.MessageText.text = "환영합니다!\n스테이지를 수정하는 방법을\n설명하는 튜토리얼입니다.";
            instance.Tutorial.MessageBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 320f);
            instance.Tutorial.MessageBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 110f);
            instance.Tutorial.MessageBox.localPosition = instance.MessageBoxPosArr[0];
            instance.isTutorial = false;
            instance.InvokeEnableEditing();
        }
    }
}