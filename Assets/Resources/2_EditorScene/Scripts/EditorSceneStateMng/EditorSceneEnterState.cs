﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

namespace EditableBlock
{
    public class EditorSceneEnterState : State<EditorSceneStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return EditorSceneStateMng.EditorSceneState.enter;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            StartCoroutine(fadeOut());
            AdMobManager.Instance.ShowBannerAd();
        }

        private IEnumerator fadeOut()
        {
            yield return new WaitForFixedUpdate();
            instance.EditorScene.gameObject.SetActive(false);
            Color color = new Color(0f, 0f, 0f, 1f);

            float t = 0f;
            while (t <= 0.997f)
            {
                t += Mathf.Lerp(1f, 0f, t) * 6f * Time.deltaTime;
                color.a = Mathf.Lerp(1f, 0f, t);
                instance.CoverImage.color = color;
                yield return null;
            }
            instance.CoverImage.gameObject.SetActive(false);
            instance.EditorSceneAnimation.LevelTextAnim.Play();
            instance.EditorSceneAnimation.BackBtnAnim.Play();
            instance.ChangeState(EditorSceneStateMng.EditorSceneState.idle);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }

        void OnDestroy()
        {

        }
    }
}