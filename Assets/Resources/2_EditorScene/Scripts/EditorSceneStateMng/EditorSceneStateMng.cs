﻿using GameStateLibrary.StateMng;
using UnityEngine;
using System.Collections.Generic;
using PuzzleGame2DLibrary.Misc;
using System.Collections;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System;
using System.Linq;
using System.Text;

namespace EditableBlock
{
    public class EditorSceneStateMng : SingletonStateMng<EditorSceneStateMng>
    {
        public static readonly float BlockGap = 64f;

        public event EventHandler EnableEditingEvent;
        public event EventHandler DisableEditingEvent;

        public float RevValue // 난이도간 블록 위치 보정값
        {
            get
            {
                return ((int)Difficulty - 1) * BlockGap / 2;
            }
        }

        public enum EditorStage
        {
            NOVICE = 5,
            NORMAL = 6,
            EXPERT = 7,
            MASTER = 8,
        }

        public enum EditorSceneState
        {
            enter,
            idle,
            load,
            generate,
            tutorial,
            exit,
        }

        [Serializable]
        public struct _SelectLvScene_
        {
            public Transform SelectScene;
        }
        [Serializable]
        public struct _LoadLvScene_
        {
            public Transform LoadScene;
        }
        [Serializable]
        public struct _Popup_
        {
            public Transform PopupTransform;
            public Text PopupText;
        }
        [Serializable]
        public struct _SavePopup_
        {
            public Transform PopupTransform;
            public Text PopupText;
        }
        [Serializable]
        public struct _Tutorial_
        {
            public Transform PopupTransform;
            public RectTransform MessageBox;
            public Text MessageText;
        }

        [Serializable]
        public struct _BlockUsage_
        {
            public Text AfterUsage;
            public Text WarpUsage;
            public Text LiveUsage;
            public Text TotalUsagePercent;

            public int AfterUsageCount;
            public int WarpUsageCount;
            public int LiveUsageCount;
            public int TotalUsageCount;
        }

        public _SelectLvScene_ SelectLvScene;
        public _LoadLvScene_ LoadLvScene;
        public _Popup_ Popup;
        public _SavePopup_ SavePopup;
        public _Tutorial_ Tutorial;
        public _BlockUsage_ BlockUsage;

        private BlockIdentifier[,] blockIdArr;
        [Space]
        [SerializeField]
        private CharBlockStateMng charBlockStateMng;
        [NonSerialized]
        public int Sequence;
        public delegate void _EscapeBtn();  // 뒤로가기 버튼
        public _EscapeBtn EscapeBtn;

        public int StageMaxBlocks;
        public LoadLevelList loadLevelList;
        public EditorStage Difficulty;
        public bool isSceneIdle;
        public bool isTutorial;
        public bool isStageInitialized;
        public Transform EditorScene;
        public Text MessageText;        // 제작 중 이벤트에 대한 설명을 해주는 텍스트
        public Text LevelText;          // 스테이지 위에 표시되는 레벨 텍스트
        public Text LevelNameText;      // 수정된 레벨 이름 텍스트
        public Image CoverImage;        // 페이드 효과 커버이미지
        public GameObject DeveloperMode;// 개발자 모드 체크

        [Serializable]
        public struct _EditorAnimation_
        {
            public Animation LevelTextAnim;     // 처음 에디터 씬에 진입했을 때 한번만 연출되는 애니메이션
            public Animation BackBtnAnim;

            public Animation NormalBlockAnim;   // 백그라운드 애니메이션 후 실행되는 블록 애니메이션
            public Animation AfterBlockAnim;
            public Animation WarpBlockAnim;
            public Animation LiveBlockAnim;

        }
        public _EditorAnimation_ EditorSceneAnimation;

        public Dictionary<Direction, Index2> DirectionTable = new Dictionary<Direction, Index2>();
        private List<BlockIdentifier> blockListToEditStage = new List<BlockIdentifier>();

        public BlockIdentifier[,] BlockIdArr { get { return blockIdArr; } }
        public bool IsAbleToEdit { get; private set; }
        public bool IsStageSet { get; private set; }

        public List<string> MessageTextList = new List<string>();
        public Vector2[] MessageBoxScaleArr;
        public Vector2[] MessageBoxPosArr;

        public CanvasGroup Stars1;
        public CanvasGroup Stars2;
        public RawImage BG_cloud;

        private bool isStarGlittered;
        float i;

        void Awake()
        {
            #region TutorialTexts
            //1 맨 처음 메시지 박스 소개
            MessageTextList.Add("당신이 행동을 할 때 이\n메시지에서 표시됩니다.");
            //2 튜토리얼의 절차 소개, 첫 번째로 캐릭터의 시작 지점 지정
            MessageTextList.Add("처음은 캐릭터 블록의 시작\n지점을 지정해야 합니다.");
            //3 빈 블록에 캐릭터를 지정
            MessageTextList.Add("원하는 블록을 꾹 누르면 지정됩니다.");
            //4 캐릭터가 세팅되면 스테이지 수정 가능
            MessageTextList.Add("시작지점이 지정되면\n스테이지를 수정할 수 있습니다.");
            //5 슬라이드로 캐릭터를 이동시킬 수 있음
            MessageTextList.Add("슬라이드를 통해 캐릭터를\n움직일 수 있습니다.");
            //6 네 방향으로 슬라이드 가능
            MessageTextList.Add("슬라이드 방향은\n위, 아래, 왼쪽, 오른쪽입니다.");
            //7 캐릭터의 움직임이 끝나면 블록 세팅을 위해 캐릭터가 있는곳에 블록을 선택해야함
            MessageTextList.Add("캐릭터를 이동시킨 후\n캐릭터가 위치한 곳에 블록을\n선택해야 합니다.");
            //8 노멀 블록
            MessageTextList.Add("노멀 블록은 아무 효과도 없습니다.");
            //9 애프터 블록
            MessageTextList.Add("에프터 블록은 방향을 가지고 있습니다.\n에프터 블록을 통해 움직이면\n그 방향이 에프터 블록의 방향이 됩니다.");
            //10 워프 블록
            MessageTextList.Add("워프 블록을 선택하면 다른\n지점으로 워프할 블록을 선택해야 합니다.");
            //11 라이브 블록
            MessageTextList.Add("마지막으로 라이브 블록은\n라이브 블록이 가질\n자식 블록들을 선택해야 합니다.");
            //12 라이브 블록 설명
            MessageTextList.Add("블록들이 선택되면 그 블록\n들은 라이브 블록의 자식이됩니다.\n자식 블록 선택에는\n제한이 없습니다!");
            //13 장애물
            MessageTextList.Add("빈 블록은 장애물이 됩니다.");
            //14
            MessageTextList.Add("뒤로 가고 싶으면\n뒤로 가기 버튼을 누르세요.");
            #endregion
            int length = MessageTextList.Count;
            #region TutorialScales
            MessageBoxScaleArr = new Vector2[length];
            MessageBoxScaleArr[0] = new Vector2(360f, 110f);
            MessageBoxScaleArr[1] = new Vector2(400f, 110f);
            MessageBoxScaleArr[2] = new Vector2(520f, 80f);
            MessageBoxScaleArr[3] = new Vector2(400f, 110f);
            MessageBoxScaleArr[4] = new Vector2(400f, 110f);
            MessageBoxScaleArr[5] = new Vector2(400f, 110f);
            MessageBoxScaleArr[6] = new Vector2(480f, 130f);
            MessageBoxScaleArr[7] = new Vector2(520f, 80f);
            MessageBoxScaleArr[8] = new Vector2(600f, 130f);
            MessageBoxScaleArr[9] = new Vector2(520f, 110f);
            MessageBoxScaleArr[10] = new Vector2(500f, 110f);
            MessageBoxScaleArr[11] = new Vector2(480f, 160f);
            MessageBoxScaleArr[12] = new Vector2(380f, 110f);
            MessageBoxScaleArr[13] = new Vector2(420f, 110f);

            #endregion
            MessageBoxPosArr = new Vector2[length]; // 포지션 요소할당은 메시지 박스의 위치에 따라 변경 되므로 PositionSetting메소드에서 작성해준다.

            IsAbleToEdit = true;
            InitializeState();
            DirectionTable.Add(Direction.left, Index2.left);
            DirectionTable.Add(Direction.right, Index2.right);
            DirectionTable.Add(Direction.up, Index2.up);
            DirectionTable.Add(Direction.down, Index2.down);
        }

        // Use this for initialization
        void Start()
        {
            StateEnter(EditorSceneState.enter);
            StartCoroutine(glitter());
            DeveloperMode.SetActive(Mng.IsDeveloperMode);
        }

        private IEnumerator glitter()
        {
            float t = 0f;
            while (t <= 1f)
            {
                t += 0.5f * Time.deltaTime;
                Stars1.alpha = !isStarGlittered ? t : 1 - t;
                Stars2.alpha = !isStarGlittered ? 1 - t : t;
                yield return null;
            }
            isStarGlittered = !isStarGlittered;
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(glitter());
        }

        private void Cloud()
        {
            i += 0.028125f * Time.deltaTime;
            if (i >= 1f)
                i = 0f;
            Rect rect = new Rect(new Vector2(i, 0), Vector2.one);
            BG_cloud.uvRect = rect;
        }

        // Update is called once per frame
        void Update()
        {
            Execute();
            Cloud();
            if (Application.isMobilePlatform)
            {
                if (Input.GetKeyDown(KeyCode.Escape) && EscapeBtn != null && IsAbleToEdit)
                    EscapeBtn();
            }
        }
        public static void DeactivationSavePopup()
        {
            Instance.SavePopup.PopupTransform.gameObject.SetActive(false);
        }

        public void InitUsage()
        {
            BlockUsage.AfterUsageCount = 0;
            BlockUsage.WarpUsageCount = 0;
            BlockUsage.LiveUsageCount = 0;
            BlockUsage.TotalUsageCount = 0;

            BlockUsage.AfterUsage.text = "0";
            BlockUsage.WarpUsage.text = "0";
            BlockUsage.LiveUsage.text = "0";
            BlockUsage.TotalUsagePercent.text = "0";
        }

        public void InvokeDisableEditing()
        {
            if (DisableEditingEvent != null)
                DisableEditingEvent(this, null);
        }
        public void InvokeEnableEditing()
        {
            if (EnableEditingEvent != null)
                EnableEditingEvent(this, null);
        }

        public void ToIdleScene()
        {
            if (isStageInitialized)
            {
                Popup.PopupText.text = "Stage is modified.\nBut are you sure\nyou want to exit?";
                Popup.PopupTransform.gameObject.SetActive(true);
            }
            else
                ChangeState(EditorSceneState.idle);

        }

        /// <summary>
        /// 시작지점 지정
        /// </summary>
        public void DesignateStartingPoint(BlockIdentifier blockId)
        {
            if (IsStageSet && charBlockStateMng.BlockHistroy.Count == 0) // 캐릭터의 위치가 초기화 되고 한 번이라도 움직이면 초기화 불가
            {
                if (charBlockStateMng.FirstBlock)
                    charBlockStateMng.FirstBlock.NumText.text = "";

                AndroidFullRoadPlugin.Instance.Vibrate(100);

                if (charBlockStateMng.BlockId)
                    charBlockStateMng.BlockId.BlockType = EditableBlockType.None;
                charBlockStateMng.BlockId = blockId;
                charBlockStateMng.FirstBlock = blockId;
                charBlockStateMng.BlockInfo.BlockTransform.localPosition = blockId.BlockIdTransform.position;
                charBlockStateMng.BlockId.BlockType = EditableBlockType.Character;
                charBlockStateMng.BlockInfo.BlockImage.color = new Color(1f, 1f, 1f, 1f);
                charBlockStateMng.ChangeState(CharBlockStateMng.CharBlockState.idle, false);
                charBlockStateMng.isInitialized = true;
                isStageInitialized = true;
            }
        }

        public void GenerateStageToSelect()
        {
            StartCoroutine(invokeGenerateStageToSelect());
        }

        public void SetStage()
        {
            LevelNameText.text = "Default Level";
            blockListToEditStage.ForEach((block) =>
            {
                Index2 index = block.BlockIndex;
                blockIdArr[index.x, index.y] = block;
            });
            if (EnableEditingEvent != null)
            {
                EnableEditingEvent(this, new EventArgs());
            }
            IsStageSet = true;
        }

        private IEnumerator invokeGenerateStageToSelect()
        {
            IsAbleToEdit = false;
            int max;
            positionSetting(out max);
            DestroyBlocks();
            for (int i = 0; i < max; i++)
            {
                for (int j = 0; j < max; j++)
                {
                    BlockIdentifier blockId =
                        Mng.Instance.CreatePrefab("Canvas/Stage", "BlockIdentifier", E_RESOURCES.E_2_EDITORSCENE, Vector3.zero, Quaternion.identity, "Block[" + j + "][" + i + "]").GetComponent<BlockIdentifier>();
                    blockId.SetBlockIndex = new Index2(j, i);
                    blockId.editorSceneStateMng = this;
                    blockListToEditStage.Add(blockId);
                    yield return new WaitForSeconds(0.0125f); // 원래는 프레임마다 대기하려 했는데 디바이스 성능차이때문에 그냥 초단위로 기다린다.
                }
            }
            IsAbleToEdit = true;
            IsStageSet = false;
        }

        public void DestroyBlocks()
        {
            blockListToEditStage.ForEach((block) =>
            {
                Destroy(block.gameObject);
            });
            blockListToEditStage.Clear();
        }

        public void GenerateStage(string levelName)
        {
            IEnumerator generate = invokeGenerateStage(levelName);
            StartCoroutine(generate);
        }

        private IEnumerator invokeGenerateStage(string levelName)
        {
            IsAbleToEdit = false;
            int max;
            positionSetting(out max);
            XmlDocument xmlDoc = null;

            if (Mng.IsDeveloperMode)
            {
                StringBuilder path = new StringBuilder(Application.dataPath);
                path.Append("/Resources/1_MenuScene/Prefabs/level/").Append(Difficulty.ToString()).Append("/").Append(levelName);
                FileInfo file = new FileInfo(path.ToString());

                StreamReader str = file.OpenText();
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(str.ReadToEnd());
            }
            else
                xmlDoc = AndroidFileMng.Instance.LoadFile(Difficulty.ToString(), levelName);

            XmlElement elements = xmlDoc["StageInfo"];
            List<BlockIdentifier> blockIdList = new List<BlockIdentifier>();        // 블록의 히스토리, 설정 등에 사용될 리스트
            List<BlockIdentifier> wholeBlockList = new List<BlockIdentifier>();     // 단순 ForEach를 위해 사용되는 리스트

            foreach (XmlElement element in elements.ChildNodes)
            {
                int x = int.Parse(element.GetAttribute("x"));
                int y = int.Parse(element.GetAttribute("y"));
                string sequence = element.GetAttribute("sequence");
                EditableBlockType type = (EditableBlockType)Enum.Parse(typeof(EditableBlockType), element.GetAttribute("type"));
                BlockIdentifier blockId =
                        Mng.Instance.CreatePrefab("Canvas/Stage", "BlockIdentifier", E_RESOURCES.E_2_EDITORSCENE, Vector3.zero, Quaternion.identity, "Block[" + x + "][" + y + "]").GetComponent<BlockIdentifier>();
                blockId.SetBlockIndex = new Index2(x, y);
                blockId.editorSceneStateMng = this;
                if(sequence == "1")
                    charBlockStateMng.FirstBlock = blockId;

                if (sequence != string.Empty)
                    blockId.Passed();

                blockId.BlockType = type;
                blockId.SetBlockImage(type);
                blockId.NumText.text = sequence;

                try
                {
                    string liveIndex = element.GetAttribute("liveIndex");
                    blockId.LiveIndex = int.Parse(liveIndex);
                    blockId.LiveIndexText.text = liveIndex;
                    blockId.IsLiveBlock = true;
                }
                catch (FormatException)
                {
                }

                switch (type)
                {
                    case EditableBlockType.AfterBlock:
                        BlockUsage.AfterUsageCount++;
                        BlockUsage.TotalUsageCount++;
                        Direction afterDir = (Direction)Enum.Parse(typeof(Direction), element.GetAttribute("direction"));
                        blockId.AfterBlock.AfterDirection = afterDir;
                        break;
                    case EditableBlockType.WarpBlock:
                        BlockUsage.WarpUsageCount++;
                        BlockUsage.TotalUsageCount++;
                        int warpIndex = int.Parse(element.GetAttribute("warpIndex"));
                        blockId.WarpBlock.WarpIndex = warpIndex;
                        break;
                    case EditableBlockType.LiveBlock:
                        BlockUsage.LiveUsageCount++;
                        BlockUsage.TotalUsageCount++;
                        string liveIndex = element.GetAttribute("motherliveIndex");
                        blockId.LiveBlock.MotherLiveIndex = int.Parse(liveIndex);
                        blockId.LiveBlock.MotherLiveIndexText.text = liveIndex;
                        break;
                    default: break;
                }

                blockIdArr[x, y] = blockId;
                if (type != EditableBlockType.None && sequence != string.Empty)
                {
                    Sequence++;
                    blockIdList.Add(blockId);
                }
                wholeBlockList.Add(blockId);
                yield return null;
            }

            foreach (BlockIdentifier blockId in wholeBlockList)
            {
                switch (blockId.BlockType)
                {
                    case EditableBlockType.WarpBlock:
                        // 이미 짝을 찾은 워프 블록이면 건너뛴다.
                        if (!blockId.WarpBlock.matchedBlockId)
                        {
                            ++BlockIdentifier._WarpBlock_.CountWarpIndex;
                            blockId.WarpBlock.matchedBlockId = blockIdList.First((warpBlock) => warpBlock.WarpBlock.WarpIndex == blockId.WarpBlock.WarpIndex);
                            blockId.WarpBlock.matchedBlockId.WarpBlock.matchedBlockId = blockId;
                        }
                        break;
                    case EditableBlockType.LiveBlock:
                        ++BlockIdentifier._LiveBlock_.CountLiveIndex;
                        wholeBlockList.ForEach((liveBlock) =>
                        {
                            if (liveBlock.LiveIndex == blockId.LiveBlock.MotherLiveIndex)
                                blockId.LiveBlock.BlockIdList.Add(liveBlock);
                        });
                        break;
                }
            }

            blockIdList.Sort((a, b) =>
            {
                return int.Parse(a.NumText.text).CompareTo(int.Parse(b.NumText.text));
            });
            blockIdList.ForEach((block) =>
            {
                charBlockStateMng.BlockHistroy.Push(block);
            });

            int curr_x = int.Parse(elements.GetAttribute("curr_x"));
            int curr_y = int.Parse(elements.GetAttribute("curr_y"));
            bool isCharInitialized = bool.Parse(elements.GetAttribute("isCharInitialized"));

            if (isCharInitialized)
            {
                charBlockStateMng.BlockId = blockIdArr[curr_x, curr_y];
                charBlockStateMng.BlockInfo.BlockTransform.localPosition = blockIdArr[curr_x, curr_y].BlockIdTransform.position;
                charBlockStateMng.BlockInfo.BlockImage.color = new Color(1f, 1f, 1f, 1f);
                charBlockStateMng.ChangeState(CharBlockStateMng.CharBlockState.idle, false);
                charBlockStateMng.isInitialized = true;
                isStageInitialized = true;
            }

            BlockUsage.AfterUsage.text = BlockUsage.AfterUsageCount.ToString();
            BlockUsage.WarpUsage.text = BlockUsage.WarpUsageCount.ToString();
            BlockUsage.LiveUsage.text = BlockUsage.LiveUsageCount.ToString();
            int percent = (int)(((float)BlockUsage.TotalUsageCount / StageMaxBlocks) * 100);
            BlockUsage.TotalUsagePercent.text = percent.ToString();
            yield return new WaitForFixedUpdate();
            IsAbleToEdit = true;
            if (EnableEditingEvent != null)
                EnableEditingEvent(this, new EventArgs());
            IsStageSet = true;
        }

        private void positionSetting(out int max)
        {
            max = (int)Difficulty;
            blockIdArr = new BlockIdentifier[max, max];
            Vector3 messagePos = MessageText.transform.localPosition;
            messagePos.y = 256f + ((int)Difficulty - 5) * 32f; // 스테이지에 해당하는 메시지 박스 포지션
            MessageText.transform.localPosition = messagePos;
            #region TutorialPos
            messagePos.y += 95f;
            MessageBoxPosArr[0] = messagePos;
            MessageBoxPosArr[1] = messagePos;
            MessageBoxPosArr[2] = messagePos;
            MessageBoxPosArr[3] = messagePos;
            MessageBoxPosArr[4] = messagePos;
            MessageBoxPosArr[5] = messagePos;
            MessageBoxPosArr[6] = new Vector2(0f, -122f);
            MessageBoxPosArr[7] = new Vector2(0f, -122f);
            MessageBoxPosArr[8] = new Vector2(0f, -122f);
            MessageBoxPosArr[9] = new Vector2(0f, -122f);
            MessageBoxPosArr[10] = new Vector2(0f, -122f);
            MessageBoxPosArr[11] = new Vector2(0f, -122f);
            MessageBoxPosArr[12] = new Vector2(0f, -122f);
            MessageBoxPosArr[13] = new Vector2(0f, -122f);
            #endregion
        }

        void OnDestroy()
        {
            DestroyInstance();
        }
    }
}