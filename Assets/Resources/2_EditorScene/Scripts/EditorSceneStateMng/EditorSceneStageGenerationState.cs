﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

namespace EditableBlock
{
    /// <summary>
    /// 스테이지를 생성하는 상태
    /// </summary>
    public class EditorSceneStageGenerationState : State<EditorSceneStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return EditorSceneStateMng.EditorSceneState.generate;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            Invoke("PlayBlockAnim", 0.15f);
            BlockIdentifier._LiveBlock_.CountLiveIndex = 0;
            BlockIdentifier._WarpBlock_.CountWarpIndex = 0;

            instance.StageMaxBlocks = (int)Mathf.Pow((int)instance.Difficulty, 2) - 1;
            instance.InitUsage();

            instance.MessageText.text = "Designate starting point";
            instance.isStageInitialized = false;
            instance.EditorScene.gameObject.SetActive(true);
            if (o_Params.Length > 0)
            {
                string levelName = (string)o_Params.GetValue(0);
                instance.GenerateStage(levelName);
                levelName = levelName.Replace(".xml", "");
                instance.SavePopup.PopupText.text = levelName;
                instance.LevelNameText.text = levelName;
            }
            else
                instance.SetStage();

            instance.EscapeBtn = instance.ToIdleScene;
        }

        // Invoke
        private void PlayBlockAnim()
        {
            instance.EditorSceneAnimation.NormalBlockAnim.Play();
            instance.EditorSceneAnimation.AfterBlockAnim.Play();
            instance.EditorSceneAnimation.WarpBlockAnim.Play();
            instance.EditorSceneAnimation.LiveBlockAnim.Play();
        }

        private void ResetBlockScale()
        {
            instance.EditorSceneAnimation.NormalBlockAnim.transform.localScale = Vector2.zero;
            instance.EditorSceneAnimation.AfterBlockAnim.transform.localScale = Vector2.zero;
            instance.EditorSceneAnimation.WarpBlockAnim.transform.localScale = Vector2.zero;
            instance.EditorSceneAnimation.LiveBlockAnim.transform.localScale = Vector2.zero;
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            CancelInvoke("PlayBlockAnim");
            ResetBlockScale();

            CharBlockStateMng.Instance.ExitStage();
            foreach (BlockIdentifier blockId in instance.BlockIdArr)
            {
                Destroy(blockId.gameObject);
            }
            instance.EditorScene.gameObject.SetActive(false);
            instance.EscapeBtn = null;
            instance.Sequence = 0;
        }
    }
}