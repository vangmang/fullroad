﻿using System;
using GameStateLibrary.State;
using UnityEngine;

namespace EditableBlock
{
    public class EditorSceneLoadState : State<EditorSceneStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return EditorSceneStateMng.EditorSceneState.load;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            instance.LoadLvScene.LoadScene.gameObject.SetActive(true);
            instance.DestroyBlocks();
            instance.EscapeBtn = () =>
            {
                instance.loadLevelList.DeleteLoadedLevelArr();
                instance.ChangeState(EditorSceneStateMng.EditorSceneState.idle);
            };

        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            instance.loadLevelList.DeleteLoadedLevelArr();
            instance.LoadLvScene.LoadScene.gameObject.SetActive(false);
            instance.EscapeBtn = null;
        }
    }
}
