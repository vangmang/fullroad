﻿using GameStateLibrary.State;
using System;
using UnityEngine;
using System.Collections;

namespace EditableBlock
{
    public class EditorSceneIdleState : State<EditorSceneStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return EditorSceneStateMng.EditorSceneState.idle;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            instance.EscapeBtn = () => { instance.ChangeState(EditorSceneStateMng.EditorSceneState.exit); };
            instance.SelectLvScene.SelectScene.gameObject.SetActive(true);
            instance.isSceneIdle = true;
            instance.GenerateStageToSelect();
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            instance.isSceneIdle = false;
            instance.SelectLvScene.SelectScene.gameObject.SetActive(false);
        }

        void OnDestroy()
        {

        }
    }
}