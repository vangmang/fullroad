﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Xml;
using System.IO;

public class LoadLogoScene : MonoBehaviour
{
    public Text ProgressText;
    public Text LoadingMessage;
    private readonly string processPath =
#if UNITY_EDITOR
        "./Assets/Resources/currentProcess.xml";
#else
        Application.persistentDataPath + "/currentProcess.xml";
#endif

    private string[] NormalMessageArr =
    {
        "스테이지를 불러오는 중입니다.",
        "경쾌한 OST를 준비중입니다.",
        "깔끔한 메뉴를 구성중입니다.",
    };
    private string[] DarkestMessageArr =
    {
        "당신같은 스타일을 잘 알죠.",
        "하드코어 게이머.",
        "여기, 고통이 준비되었습니다.",
    };

    // Use this for initialization
    IEnumerator Start()
    {
        bool isDarkest;
        try {
            XmlDocument xmlDoc = new XmlDocument();
            using (StreamReader sr = new StreamReader(processPath))
            {
                string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
                xmlDoc.LoadXml(decryption);
            }
            XmlElement elements = xmlDoc["Level"];
            isDarkest = bool.Parse(elements.GetAttribute("DarkestEnable"));
        }
        catch (System.IO.IsolatedStorage.IsolatedStorageException)
        {
            isDarkest = false;
        }
        catch (FileNotFoundException)
        {
            isDarkest = false;
        }
        AsyncOperation async = SceneManager.LoadSceneAsync(1);
        var messages = isDarkest ? DarkestMessageArr : NormalMessageArr;
        LoadingMessage.text = messages[0];
        while (!async.isDone)
        {
            ProgressText.text = (int)(async.progress * 100f) + "%";
            if (async.progress >= 0.8f)
                LoadingMessage.text = messages[2];
            else if(async.progress >= 0.2f)
                LoadingMessage.text = messages[1];
            yield return null;
        }
    }
}
