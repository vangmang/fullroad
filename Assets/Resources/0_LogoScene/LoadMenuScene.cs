﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoadMenuScene : MonoBehaviour {

    public Image LogoImage;

	// Use this for initialization
	IEnumerator Start () {
        Color color = new Color(1f, 1f, 1f, 0f);
        float t = 0f;
        yield return new WaitForFixedUpdate();

        while(t <= 0.997f)
        {
            t += Mathf.Lerp(1f, 0f, t) * 3f * Time.deltaTime;
            color.a = Mathf.Lerp(0f, 1f, t);
            LogoImage.color = color;
            yield return null;
        }
        yield return new WaitForSeconds(0.125f);
        t = 0f;
        while (t <= 0.997f)
        {
            t += Mathf.Lerp(1f, 0f, t) * 6f * Time.deltaTime;
            color.a = Mathf.Lerp(1f, 0f, t);
            LogoImage.color = color;
            yield return null;
        }
        yield return new WaitForFixedUpdate();
        Mng.Instance.LoadLevel(2);
    }

    void OnDestroy()
    {

    }
}
