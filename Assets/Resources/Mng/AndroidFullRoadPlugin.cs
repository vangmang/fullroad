﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidFullRoadPlugin : MonoBehaviour {
    private AndroidFullRoadPlugin() { }

    private static AndroidFullRoadPlugin instance = null;
    public static AndroidFullRoadPlugin Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(AndroidFullRoadPlugin)) as AndroidFullRoadPlugin;
            return instance;
        }
    }

    AndroidJavaClass Jc;
    AndroidJavaObject Jo;
    AndroidJavaObject vibrator;

	// Use this for initialization
	void Start () {
#if !UNITY_EDITOR
        Jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        Jo = Jc.GetStatic<AndroidJavaObject>("currentActivity");
        vibrator = Jo.Call<AndroidJavaObject>("getSystemService", "vibrator");
#endif
    }

    public void Vibrate(long length)
    {
        if(vibrator != null)
            vibrator.Call("vibrate", length);
    }
}
