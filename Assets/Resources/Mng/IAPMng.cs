﻿using UnityEngine;
using UnityEngine.Purchasing;
using System;

public class IAPMng : MonoBehaviour, IStoreListener
{

    private static IAPMng instance = null;
    public static IAPMng Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(IAPMng)) as IAPMng;
            return instance;
        }
    }

    private static IStoreController storeController;
    private static IExtensionProvider extensionProvider;

    private const string ID_no_ads = "no_ads";
    private Action afterPurchasing;

    public static bool IsAdEnable = true;
    
    void Start()
    {
        if (storeController == null)
            InitializePurchasing();
    }

    void checkNoAd()
    {
        try
        {
            IsAdEnable = !storeController.products.WithID(ID_no_ads).hasReceipt;
            Debug.Log("Is Advertisement enable: " + IsAdEnable);
        }
        catch (NullReferenceException e)
        {
            Debug.Log(e.Message);
        }
    }


    public void InitializePurchasing()
    {
        if (isInitialized())
            return;
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(ID_no_ads, ProductType.NonConsumable, new IDs() {
            { ID_no_ads, GooglePlay.Name }
        });
        UnityPurchasing.Initialize(this, builder);
    }

    // 광고제거 상품
    public void BuyProduct_NoAds(Action invoke)
    {
        afterPurchasing = invoke;
        InvokeBuyProductID(ID_no_ads);
    }

    private void InvokeBuyProductID(string id)
    {
        if (isInitialized())
        {
            var product = storeController.products.WithID(id);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                storeController.InitiatePurchase(product);
            }
            else
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
        }
        else
        {
            InitializePurchasing();
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    private bool isInitialized()
    {
        return storeController != null && extensionProvider != null;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        storeController = controller;
        extensionProvider = extensions;
        checkNoAd();
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        switch (e.purchasedProduct.definition.id)
        {
            case ID_no_ads:
                IsAdEnable = false;
                afterPurchasing();
                break;
        }

        return PurchaseProcessingResult.Complete;
    }
}
