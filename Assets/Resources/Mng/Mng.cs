﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using System.IO;
using IgaworksUnityAOS;

public enum E_RESOURCES
{
    E_0_LOADINGSCENE = 0,
    E_1_MENUSCENE = 1,
    E_2_EDITORSCENE = 2,
    E_3_GAMESCENE = 3,
    E_MAX = 4,
}

/// <summary>
/// 게임에 사용할 모든 매니저 스크립트.
/// </summary>
public class Mng : MonoBehaviour
{
    private Mng() { }
    private string[] s_SceneTitle =
    {
        "0_LoadingScene",
        "1_MenuScene",
        "2_EditorScene",
        "3_GameScene"
    };

    public static bool IsDeveloperMode { get; private set; }


    private static Mng m_Instance = null;
    public static Mng Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType(typeof(Mng)) as Mng;

                if (m_Instance == null)
                {
                    return null;
                }
            }

            return m_Instance;
        }
    }

    public static bool IsGameStarted;

    // 게임씬에서 불러올 스테이지 정보
    public struct _StageInfo_
    {
        public int CurrentStageLevelNum;
        public MainMenu.MainMenuStateMng.MenuLevel CurrentMenuLevel;
        public Image CurrentInsignia;
    }

    public struct _EachLevelProcess_
    {
        public int this[MainMenu.MainMenuStateMng.MenuLevel level]
        {
            get
            {
                switch (level)
                {
                    case MainMenu.MainMenuStateMng.MenuLevel.NOVICE: return NOVICE;
                    case MainMenu.MainMenuStateMng.MenuLevel.NORMAL: return NORMAL;
                    case MainMenu.MainMenuStateMng.MenuLevel.EXPERT: return EXPERT;
                    case MainMenu.MainMenuStateMng.MenuLevel.MASTER: return MASTER;
                    default: return -1;
                }
            }
        }
        public int NOVICE;
        public int NORMAL;
        public int EXPERT;
        public int MASTER;
    }

    public struct _MaxLevel_
    {
        public int this[MainMenu.MainMenuStateMng.MenuLevel level]
        {
            get
            {
                switch (level)
                {
                    case MainMenu.MainMenuStateMng.MenuLevel.NOVICE: return MaxNoviceLevels;
                    case MainMenu.MainMenuStateMng.MenuLevel.NORMAL: return MaxNormalLevels;
                    case MainMenu.MainMenuStateMng.MenuLevel.EXPERT: return MaxExpertLevels;
                    case MainMenu.MainMenuStateMng.MenuLevel.MASTER: return MaxMasterLevels;
                    default: return -1;
                }
            }
            set
            {
                switch (level)
                {
                    case MainMenu.MainMenuStateMng.MenuLevel.NOVICE: MaxNoviceLevels = value; break;
                    case MainMenu.MainMenuStateMng.MenuLevel.NORMAL: MaxNormalLevels = value; break;
                    case MainMenu.MainMenuStateMng.MenuLevel.EXPERT: MaxExpertLevels = value; break;
                    case MainMenu.MainMenuStateMng.MenuLevel.MASTER: MaxMasterLevels = value; break;
                }
            }
        }

        public int MaxNoviceLevels;
        public int MaxNormalLevels;
        public int MaxExpertLevels;
        public int MaxMasterLevels;
    }

    public _StageInfo_ StageInfo;
    public _EachLevelProcess_ EachLevelProcess;
    public _MaxLevel_ MaxLevel;
    private readonly string processPath =
#if UNITY_EDITOR
        "./Assets/Resources/currentProcess.xml";
#else
        Application.persistentDataPath + "/currentProcess.xml";
#endif

    private readonly string insigniaProcessPath =
#if UNITY_EDITOR
        "./Assets/Resources/insigniaProcess.xml";
#else
        Application.persistentDataPath + "/insigniaProcess.xml";
#endif

    /// <summary>
    /// 로딩할때 코루틴에 넘겨줄 enum. 기본적으로 게임씬으로 저장되어있음
    /// </summary>
    public E_RESOURCES Scene = E_RESOURCES.E_3_GAMESCENE;
    public Dictionary<string, bool> AchievementProcessTable
    {
        get; private set;
    }
    public string CurrentInsignia
    {
        get; private set;
    }
    public bool NewInsigniaNotice
    {
        get; private set;
    }
    public bool IsGameDarkest
    {
        get; private set;
    }
    public bool IsAbleToDarkestOn
    {
        get; private set;
    }

    /// <summary>
    /// 현재 로딩 상황을 알려줄 필드
    /// </summary>
    public float fProgress
    {
        get; private set;
    }

    void Awake()
    {
        Application.targetFrameRate = 60;
        IgaworksUnityPluginAOS.InitPlugin();
        AchievementProcessTable = new Dictionary<string, bool>();
        GoogleMng.Instance.InsgniaIdList.ForEach(name =>
        {
            AchievementProcessTable.Add(name, false);
        });

        DontDestroyOnLoad(this);
#if UNITY_EDITOR
        IsDeveloperMode = true;
#endif
        if (File.Exists(processPath) && File.Exists(insigniaProcessPath))
        {
            SetCurrentProcess();
            SetCurrentDarkest();
            SetCurrentAchievement();
            SetCurrentInsignia();
        }
        else
        {
            EachLevelProcess.NOVICE = 0;
            EachLevelProcess.NORMAL = 0;
            EachLevelProcess.EXPERT = 0;
            EachLevelProcess.MASTER = 0;
            // process
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null));
            XmlElement currentProcess = xmlDoc.CreateElement("Level");
            currentProcess.SetAttribute("NOVICE", "0");
            currentProcess.SetAttribute("NORMAL", "0");
            currentProcess.SetAttribute("EXPERT", "0");
            currentProcess.SetAttribute("MASTER", "0");
            currentProcess.SetAttribute("DarkestEnable", "False");
            currentProcess.SetAttribute("IsAbleToDarkestOn", "True");
            IsAbleToDarkestOn = true;

            xmlDoc.AppendChild(currentProcess);
            string encryption = DataSecurity.Encrypt(xmlDoc.OuterXml);
            File.WriteAllText(processPath, encryption);

            // achievement
            XmlDocument achievementXmlDoc = new XmlDocument();
            achievementXmlDoc.AppendChild(achievementXmlDoc.CreateXmlDeclaration("1.0", "utf-8", null));
            XmlElement currentAchievementProcess = achievementXmlDoc.CreateElement("Achievement");
            GoogleMng.Instance.InsgniaIdList.ForEach(name => currentAchievementProcess.SetAttribute(name, "False"));
            currentAchievementProcess.SetAttribute("CurrentInsignia", "None");
            currentAchievementProcess.SetAttribute("NewInsignia", "False");

            achievementXmlDoc.AppendChild(currentAchievementProcess);
            encryption = DataSecurity.Encrypt(achievementXmlDoc.OuterXml);
            File.WriteAllText(insigniaProcessPath, encryption);
        }
    }

    void Start()
    {
        string id = string.Empty;
        Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnable, string error) => {
            id = DataSecurity.Encrypt(advertisingId);
            trackingEnable = true;
        });
        IgaworksUnityPluginAOS.Common.setUserId(id);
        IgaworksUnityPluginAOS.LiveOps.initialize();
        IgaworksUnityPluginAOS.Common.startSession();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Debug.Log("go to Background");
            IgaworksUnityPluginAOS.Common.endSession();
        }
        else {
            Debug.Log("go to Foreground");
            IgaworksUnityPluginAOS.Common.startSession();
            IgaworksUnityPluginAOS.LiveOps.resume();
        }
    }

    // 테스트용 삭제 메소드
    public void TEST()
    {
        File.Delete(insigniaProcessPath);
    }

    public void SetCurrentDarkest()
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(processPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Level"];
        IsGameDarkest = bool.Parse(elements.GetAttribute("DarkestEnable"));
        IsAbleToDarkestOn = bool.Parse(elements.GetAttribute("IsAbleToDarkestOn"));
    }

    // 다키스트업적 활성화 설정 메소드
    public void SetDarkestEnable(bool toggle)
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(processPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Level"];
        IsGameDarkest = toggle;
        elements.SetAttribute("DarkestEnable", toggle.ToString());
        string encryption = DataSecurity.Encrypt(xmlDoc.OuterXml);
        File.WriteAllText(processPath, encryption);
    }
    // 다키스트업적을 활성화 할 수 있는지에 대한 여부 설정 메소드
    public void SetIsDarkestActive(bool toggle)
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(processPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Level"];
        IsAbleToDarkestOn = toggle;
        elements.SetAttribute("IsAbleToDarkestOn", toggle.ToString());
        string encryption = DataSecurity.Encrypt(xmlDoc.OuterXml);
        File.WriteAllText(processPath, encryption);
    }

    // 다키스트가 비활성되는 조건 
    // 1) 다키스트를 활성화하지 않고 스테이지 클리어
    // 2) 다키스트가 이미 활성화 된 상태이고 마스터 50레벨 이하일 때 다른 스테이지를 클리어했을 때
    public void CheckDarkestRelease(MainMenu.MainMenuStateMng.MenuLevel level, int stageNum)
    {
        if (!IsGameDarkest)
        {
            if (level != MainMenu.MainMenuStateMng.MenuLevel.MASTER && stageNum >= 1)
            {
                if (level == MainMenu.MainMenuStateMng.MenuLevel.NOVICE && stageNum <= 4)
                    return;
                DisableDarkest();
            }
        }
        else
        {
            if (level != MainMenu.MainMenuStateMng.MenuLevel.MASTER && stageNum >= 1 &&
                EachLevelProcess.MASTER <= 50)
            {
                if (level == MainMenu.MainMenuStateMng.MenuLevel.NOVICE && stageNum <= 4)
                    return;
                SoundMng.Instance.ResetDarkestBGM();
                DisableDarkest();
            }
        }
    }

    private void DisableDarkest()
    {
        SetIsDarkestActive(false);
        SetDarkestEnable(false);
    }

    public void SetInsigniaNotice(bool toggle)
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(insigniaProcessPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Achievement"];
        NewInsigniaNotice = toggle;
        elements.SetAttribute("NewInsignia", toggle.ToString());
        string encryption = DataSecurity.Encrypt(xmlDoc.OuterXml);
        File.WriteAllText(insigniaProcessPath, encryption);
    }

    public void SetCurrentInsignia()
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(insigniaProcessPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Achievement"];
        CurrentInsignia = elements.GetAttribute("CurrentInsignia");
        NewInsigniaNotice = bool.Parse(elements.GetAttribute("NewInsignia"));
    }

    public void SaveCurrentInsignia(string id)
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(insigniaProcessPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Achievement"];
        elements.SetAttribute("CurrentInsignia", id);
        CurrentInsignia = id;
        string encryption = DataSecurity.Encrypt(xmlDoc.OuterXml);
        File.WriteAllText(insigniaProcessPath, encryption);
    }

    public void SetCurrentAchievement()
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(insigniaProcessPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Achievement"];
        foreach (XmlAttribute attribute in elements.Attributes)
        {
            if (attribute.Name == "CurrentInsignia" || attribute.Name == "NewInsignia")
                continue;
            bool achieve = bool.Parse(attribute.Value);
            string name = attribute.Name;
            AchievementProcessTable[name] = achieve;
        }
    }

    /// <summary>
    /// 휘장 업적 달성시 휘장을 저장해주는 메소드
    /// </summary>
    /// <param name="id"> GPGSId </param>
    public void SaveCurrentAchievement(string id)
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(insigniaProcessPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Achievement"];
        // 이미 업적이 활성화 되어있으면 아무일도 하지 않는다
        if (bool.Parse(elements.GetAttribute(id)))
            return;
        elements.SetAttribute(id, "True");
        elements.SetAttribute("NewInsignia", "True");
        NewInsigniaNotice = true;

        string encryption = DataSecurity.Encrypt(xmlDoc.OuterXml);
        File.WriteAllText(insigniaProcessPath, encryption);
    }

    /// <summary>
    /// 게임이 시작되었을 때 유저가 어디까지 깼는지 체크 후 스테이지 락을 설정하는 메소드
    /// </summary>
    public void SetCurrentProcess()
    {
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(processPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Level"];
        EachLevelProcess.NOVICE = int.Parse(elements.GetAttribute("NOVICE"));
        EachLevelProcess.NORMAL = int.Parse(elements.GetAttribute("NORMAL"));
        EachLevelProcess.EXPERT = int.Parse(elements.GetAttribute("EXPERT"));
        EachLevelProcess.MASTER = int.Parse(elements.GetAttribute("MASTER"));
    }

    /// <summary>
    /// 현재까지 깬 레벨을 저장하는 메소드
    /// </summary>
    /// <param name="level"> 저장할 난이도 </param>
    /// <param name="stageNum"> 스테이지 번호 </param>
    public void SaveCurrentProcess(MainMenu.MainMenuStateMng.MenuLevel level, int stageNum)
    {
        string name = level.ToString();
        XmlDocument xmlDoc = new XmlDocument();
        using (StreamReader sr = new StreamReader(processPath))
        {
            string decryption = DataSecurity.Decrypt(sr.ReadToEnd());
            xmlDoc.LoadXml(decryption);
        }
        XmlElement elements = xmlDoc["Level"];

        int originNum = int.Parse(elements.GetAttribute(name));
        if (stageNum > originNum)
        {
            elements.SetAttribute(name, stageNum.ToString());
            string encryption = DataSecurity.Encrypt(xmlDoc.OuterXml);
            File.WriteAllText(processPath, encryption);
        }
    }

    public void SetDeveloperMode(Toggle developerModeToggle)
    {
        developerModeToggle.onValueChanged.AddListener((isDeveloperMode) => IsDeveloperMode = isDeveloperMode);
        developerModeToggle.isOn = IsDeveloperMode;
    }

    /// <summary>
    /// 문자열로 찾은 후 프리팹 생성
    /// </summary>
    /// <param name="s_Parent"> 프리팹을 어디에 생성할건지의 여부</param>
    /// <param name="s_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(string s_Parent,
                             string s_Prefab,
                             E_RESOURCES E_L_SCENE,
                             Vector3 Position,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(s_Prefab);

        // 부모 오브젝트를 먼저 찾는다.
        GameObject parent = GameObject.Find(s_Parent);

        // 리소스 테이블에서 생성할 프리팹을 찾는다
        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;

        // 찾은 프리팹을 인스턴시에이트 해준다.
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;

        // 생성한 프리팹을 부모의 자식으로 넣어준다.
        CreatedPrefab.transform.SetParent(parent.transform);
        CreatedPrefab.name = s_PrefabName;

        return CreatedPrefab;
    }

    /// <summary>
    /// 게임오브젝트로 찾은 후 프리팹 생성
    /// </summary>
    /// <param name="g_Parent"> 프리팹을 어디에 생성할건지의 여부 </param>
    /// <param name="g_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(GameObject g_Parent,
                             GameObject g_Prefab,
                             E_RESOURCES E_L_SCENE,
                             Vector3 Position,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(g_Parent.name);

        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;
        CreatedPrefab.transform.SetParent(g_Parent.transform);

        CreatedPrefab.name = s_PrefabName;
        return CreatedPrefab;
    }

    /// <summary>
    /// 트랜스폼으로 찾은 후 프리팹 생성
    /// </summary>
    /// <param name="g_Parent"> 프리팹을 어디에 생성할건지의 여부 </param>
    /// <param name="g_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(Transform g_Parent,
                             string s_Prefab,
                             E_RESOURCES E_L_SCENE,
                             Vector3 Position,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(s_Prefab);

        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;
        CreatedPrefab.transform.SetParent(g_Parent.transform);

        CreatedPrefab.name = s_PrefabName;
        return CreatedPrefab;
    }

    /// <summary>
    /// 월드에 프리팹 생성
    /// </summary>
    /// <param name="s_Prefab"> 어떤 프리팹을 생성할건지의 여부 </param>
    /// <param name="Position"> 생성할 프리팹의 포지션값 </param>
    /// <param name="Rotation"> 생성할 프리팹의 로테이션값 </param>
    /// <param name="s_PrefabName"> 생성할 프리팹의 이름 </param>
    public GameObject CreatePrefab(string s_Prefab,
                             Vector3 Position,
                             E_RESOURCES E_L_SCENE,
                             Quaternion Rotation,
                             string s_PrefabName = "Prefab(Clone)")
    {
        StringBuilder PrefabObj = new StringBuilder(s_SceneTitle[(int)E_L_SCENE]).Append("/Prefabs/").Append(s_Prefab);

        GameObject Obj = Resources.Load(PrefabObj.ToString()) as GameObject;
        GameObject CreatedPrefab = Instantiate(Obj, Position, Rotation) as GameObject;

        CreatedPrefab.name = s_PrefabName;
        return CreatedPrefab;
    }

    /// <summary>
    /// 로딩중 호출되는 코루틴 메소드
    /// </summary>
    /// <returns></returns>
    private IEnumerator SceneLoading(int scene)
    {
        AsyncOperation Async = SceneManager.LoadSceneAsync(scene);

        while (!Async.isDone)
        {
            fProgress = Async.progress * 100.0f;
            yield return true;
        }
    }

    /// <summary>
    /// 씬 전환 메소드
    /// </summary>
    /// <param name="Scene"> 씬 인덱스 </param>
    public void LoadLevel(int scene)
    {
        IEnumerator loading = SceneLoading(scene);
        StartCoroutine(loading);
    }
}
