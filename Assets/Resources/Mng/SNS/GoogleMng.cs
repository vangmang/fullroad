﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Collections.Generic;
using System;
using UnityEngine;
using IgaworksUnityAOS;
using System.Collections;

public sealed class GoogleMng : MonoBehaviour
{
    private GoogleMng() { }
    private static GoogleMng instance = null;
    public static GoogleMng Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(GoogleMng)) as GoogleMng;
            return instance;
        }
    }

    // 휘장 아이디 리스트
    public readonly List<string> InsgniaIdList = new List<string>
    {
        // NOVICE
        GPGSIds.achievement_block_thrower,
        GPGSIds.achievement_block_identifier,
        GPGSIds.achievement_the_roader,
        // NORMAL
        GPGSIds.achievement_full_roader,
        GPGSIds.achievement_road_ranger,
        GPGSIds.achievement_road_finder,
        // EXPERT
        GPGSIds.achievement_block_expert,
        GPGSIds.achievement_block_conqueror,
        GPGSIds.achievement_block_penetrater,
        // MASTER
        GPGSIds.achievement_block_master,
        GPGSIds.achievement_road_master,
        // HIDDEN
        GPGSIds.achievement_the_darkest_roader,
        GPGSIds.achievement_the_hardcore_roader,
        GPGSIds.achievement_the_road_afflicter,
        GPGSIds.achievement_the_deep_dark_roader,
        GPGSIds.achievement_the_road_creator
    };

    public readonly Dictionary<string, string> InsigniaIdTable = new Dictionary<string, string>
    {
        { "Block Thrower", GPGSIds.achievement_block_thrower },
        { "Block Identifier", GPGSIds.achievement_block_identifier},
        { "The Roader", GPGSIds.achievement_the_roader },

        { "Full Roader", GPGSIds.achievement_full_roader },
        { "Road Ranger", GPGSIds.achievement_road_ranger },
        { "Road Finder", GPGSIds.achievement_road_finder },

        { "Block Expert", GPGSIds.achievement_block_expert },
        { "Block Conqueror", GPGSIds.achievement_block_conqueror },
        { "Block Penetrater", GPGSIds.achievement_block_penetrater },

        { "Block Master", GPGSIds.achievement_block_master },
        { "Road Master", GPGSIds.achievement_road_master },

        { "The Darkest Roader", GPGSIds.achievement_the_darkest_roader },
        { "The Hardcore Roader", GPGSIds.achievement_the_hardcore_roader },
        { "The Road Afflicter", GPGSIds.achievement_the_road_afflicter },
        { "The Deep Dark Roader", GPGSIds.achievement_the_deep_dark_roader },
        { "The Road Creator", GPGSIds.achievement_the_road_creator },
    };

    public bool IsLogin { get; private set; }
    public event EventHandler LoginEvent;

    // Use this for initialization
    void Start()
    {
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().
            EnableSavedGames().
            Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = false;
        PlayGamesPlatform.Activate();
        SignIn();
#endif
    }

    public void DestroyLoginEvent()
    {
        LoginEvent = null;
    }

    public void InvokeLoginEvent()
    {
        if (LoginEvent != null)
            LoginEvent(this, null);
    }

    public void SignIn()
    {
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success) =>
            {
                IsLogin = success;
                InvokeLoginEvent();
            });
        }
    }

    public void SignOut()
    {
        IsLogin = false;
        if (Social.localUser.authenticated)
            PlayGamesPlatform.Instance.SignOut();
        InvokeLoginEvent();
    }

    public void ShowAchievement()
    {
        Social.ShowAchievementsUI();
    }

    public void ShowLeaderboard()
    {
        Social.ShowLeaderboardUI();
    }

    #region Achievement
    public void CheckAchievement(MainMenu.MainMenuStateMng.MenuLevel level, int stageNum, Action invokeDarkest)
    {
        Mng.Instance.CheckDarkestRelease(level, stageNum);
        invokeDarkest();
        switch (level)
        {
            case MainMenu.MainMenuStateMng.MenuLevel.NOVICE:
                if (stageNum == 4) Welcome_Road();
                else if (stageNum == 25) Block_Thrower();
                else if (stageNum == 50) Block_Identifier();
                else if (stageNum == 75) The_Roader();
                break;
            case MainMenu.MainMenuStateMng.MenuLevel.NORMAL:
                if (stageNum == 50) Full_Roader();
                else if (stageNum == 100) Road_Ranger();
                else if (stageNum == 150) Road_Finder();
                break;
            case MainMenu.MainMenuStateMng.MenuLevel.EXPERT:
                if (stageNum == 50) Block_Expert();
                else if (stageNum == 100) Block_Conqueror();
                else if (stageNum == 150) Block_Penetrater();
                break;
            case MainMenu.MainMenuStateMng.MenuLevel.MASTER:
                if (stageNum == 1) The_Darkest_Roader();
                else if (stageNum == 25) The_Hardcore_Roader();
                else if (stageNum == 40) Block_Master();
                else if (stageNum == 50) The_Road_Afflicter();
                else if (stageNum == 75) The_Deep_Dark_Roader();
                else if (stageNum == 87)
                {
                    OnTheOldRoadWeWillFindOurRedemption();
                    The_Road_Creator();
                    Road_Master();
                }
                break;
        }
    }

    public void Welcome_Road()
    {
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_welcome_road, 100f, null);
    }
    #region NOVICE
    // NOVICE //
    public void Block_Thrower()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_block_thrower);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_block_thrower, 100f, null);
    }
    public void Block_Identifier()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_block_identifier);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_block_identifier, 100f, null);
    }
    public void The_Roader()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_the_roader);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_the_roader, 100f, null);
    }
    #endregion 
    #region NORMAL
    // NORMAL //
    public void Full_Roader()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_full_roader);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_full_roader, 100f, null);
    }
    public void Road_Ranger()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_road_ranger);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_road_ranger, 100f, null);
    }
    public void Road_Finder()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_road_finder);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_road_finder, 100f, null);
    }
    #endregion
    #region EXPERT
    // EXPERT //
    public void Block_Expert()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_block_expert);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_block_expert, 100f, null);
    }
    public void Block_Conqueror()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_block_conqueror);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_block_conqueror, 100f, null);
    }
    public void Block_Penetrater()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_block_penetrater);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_block_penetrater, 100f, null);
    }
    #endregion
    #region MASTER
    // MASTER //
    public void Block_Master()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_block_master);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_block_master, 100f, null);
    }
    public void Road_Master()
    {
        Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_road_master);
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_road_master, 100f, null);
    }
    #endregion
    #region Misc
    public void Death_Roader()
    {
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_death_roader, 100f, null);
    }
    public void IDIOT()
    {
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_idiot, 100f, null);
    }
    public void You_are_quite_good_at_it()
    {
        if (IsLogin) Social.ReportProgress(GPGSIds.achievement_youre_quite_good_at_it, 100f, null);
    }
    #endregion
    #region DARKEST
    public void The_Darkest_Roader()
    {
        if (Mng.Instance.EachLevelProcess.NOVICE <= 4 &&
            Mng.Instance.EachLevelProcess.NORMAL == 0 &&
            Mng.Instance.EachLevelProcess.EXPERT == 0 &&
            Mng.Instance.IsAbleToDarkestOn && 
            !Mng.Instance.IsGameDarkest)
        {
            SoundMng.Instance.InvokeDarkestBGM();
            Mng.Instance.SetDarkestEnable(true); // 다키스트 활성
            Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_the_darkest_roader);
            if (IsLogin) Social.ReportProgress(GPGSIds.achievement_the_darkest_roader, 100f, null);
        }
    }
    public void The_Hardcore_Roader()
    {
        if (Mng.Instance.IsGameDarkest)
        {
            Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_the_hardcore_roader);
            if (IsLogin) Social.ReportProgress(GPGSIds.achievement_the_hardcore_roader, 100f, null);
        }
    }
    public void The_Road_Afflicter()
    {
        if (Mng.Instance.IsGameDarkest)
        {
            Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_the_road_afflicter);
            if (IsLogin) Social.ReportProgress(GPGSIds.achievement_the_road_afflicter, 100f, null);
        }
    }
    public void The_Deep_Dark_Roader()
    {
        if (Mng.Instance.IsGameDarkest)
        {
            Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_the_deep_dark_roader);
            if (IsLogin) Social.ReportProgress(GPGSIds.achievement_the_deep_dark_roader, 100f, null);
        }
    }
    public void The_Road_Creator()
    {
        if (Mng.Instance.IsGameDarkest)
        {
            Mng.Instance.SaveCurrentAchievement(GPGSIds.achievement_the_road_creator);
            if (IsLogin) Social.ReportProgress(GPGSIds.achievement_the_road_creator, 100f, null);
        }
    }
    public void OnTheOldRoadWeWillFindOurRedemption()
    {
        if (Mng.Instance.EachLevelProcess.NOVICE == 7 &&
            Mng.Instance.EachLevelProcess.NORMAL == 7 &&
            Mng.Instance.EachLevelProcess.EXPERT == 7)
            if (IsLogin) Social.ReportProgress(GPGSIds.achievement_on_the_old_road_we_will_find_our_redemption, 100f, null);
    }
    #endregion
    #endregion
}
