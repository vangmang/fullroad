﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Xml;

public class AndroidFileMng : MonoBehaviour
{
    private static AndroidFileMng instance = null;
    public static AndroidFileMng Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(AndroidFileMng)) as AndroidFileMng;
            return instance;
        }
    }

    public void CreateLevelToFile(XmlDocument xmlDoc, string level, string fileName)
    {
        string path = GetPathString(level);
        DirectoryInfo di = new DirectoryInfo(path);
        if (!di.Exists)
            di.Create();

        StringBuilder sb = new StringBuilder(path);
        sb.Append("/");
        sb.Append(fileName);
        FileStream file = new FileStream(sb.ToString(), FileMode.Create, FileAccess.Write);

        xmlDoc.Save(file);
        file.Close();
    }

    public void DeleteLevelInFile(string level, string fileName)
    {
        string path = GetPathString(level);
        StringBuilder sb = new StringBuilder(path);
        sb.Append("/");
        sb.Append(fileName);
        FileInfo fi = new FileInfo(sb.ToString());
        try
        {
            fi.Delete();
        }
        catch (IOException e)
        {
            Debug.Log(e.Message);
        }
    }

    public List<string> FileCountInLevel(string level)
    {
        string path = GetPathString(level);
        DirectoryInfo di = new DirectoryInfo(path);
        if (!di.Exists)
            return null;
        List<string> nameList = new List<string>();
        FileInfo[] fileArr = di.GetFiles();
        foreach (FileInfo file in fileArr)
            nameList.Add(file.Name);
        return nameList;
    }

    public XmlDocument LoadFile(string level, string fileName)
    {
        string path = GetPathString(level);
        DirectoryInfo di = new DirectoryInfo(path);
        if (!di.Exists)
            return null;

        StringBuilder sb = new StringBuilder(path);
        sb.Append("/");
        sb.Append(fileName);
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(sb.ToString());
        return xmlDoc;
    }

    private string GetPathString(string level)
    {
        string path = Application.persistentDataPath;
        path = path.Substring(0, path.LastIndexOf('/'));
        return Path.Combine(path, level);
    }
    void OnDestroy()
    {
        instance = null;
    }
}
