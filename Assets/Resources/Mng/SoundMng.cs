﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SoundMng : MonoBehaviour
{
    private SoundMng() { }

    [SerializeField]
    private List<AudioSource> BGM_List;
    [SerializeField]
    private AudioSource DarkestBGM;
    private static AudioSource BGM;

    private static SoundMng instance = null;
    public static SoundMng Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(SoundMng)) as SoundMng;
            return instance;
        }
    }

    private bool isPausing;
    private float time;

    public static bool BGM_Enable
    {
        get; private set;
    }
    public static bool SFX_Enable
    {
        get; private set;
    }

    void Awake()
    {
        BGM_Enable = true;
        SFX_Enable = true;
    }

    private IEnumerator ChangeBGM()
    {
        yield return new WaitForFixedUpdate();
        int randomIndex = Random.Range(0, BGM_List.Count);
        BGM = Mng.Instance.IsGameDarkest ? DarkestBGM : BGM_List[randomIndex];
        BGM.time = 0f;
        BGM.volume = 1f;
        yield return new WaitUntil(() => BGM_Enable);
        BGM.Play();
        if (Mng.Instance.IsGameDarkest)
            yield break;
        yield return new WaitWhile(() => BGM.isPlaying || isPausing);
        StartCoroutine(ChangeBGM());
    }

    public void InvokeDarkestBGM()
    {
        StartCoroutine(TurnOffMainBGMandStartDarkest());
        StopCoroutine(ChangeBGM());
    }

    public void ResetDarkestBGM()
    {
        StartCoroutine(TurnOffDarkestBGMandStartMain());
    }

    private IEnumerator TurnOffMainBGMandStartDarkest()
    {
        float t = BGM.volume;
        while (t > 0f)
        {
            t -= 0.67f * Time.deltaTime;
            BGM.volume = t;
            yield return null;
        }
        BGM.volume = 0f;
        BGM = DarkestBGM;
        if(BGM_Enable)
            BGM.Play();
    }

    private IEnumerator TurnOffDarkestBGMandStartMain()
    {
        float t = BGM.volume;
        while (t > 0f)
        {
            t -= 0.67f * Time.deltaTime;
            BGM.volume = t;
            yield return null;
        }
        BGM.volume = 0f;
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ChangeBGM());
    }

    public void PlayBGM()
    {
        StartCoroutine(ChangeBGM());
    }

    public void SetBGM_Enable()
    {
        BGM_Enable = !BGM_Enable;
        if (BGM_Enable)
            StartCoroutine(InterpolateVolumeTo_1());
        else {
            StartCoroutine(InterpolateVolumeTo_0());
        }
    }
    private IEnumerator InterpolateVolumeTo_1()
    {
        isPausing = false;
        BGM.time = time;
        if(!BGM.isPlaying)
            BGM.Play();
        float t = BGM.volume;
        while (t <= 1f)
        {
            t += 5f * Time.deltaTime;
            BGM.volume = t;
            if (!BGM_Enable)
                yield break;
            yield return null;
        }
        BGM.volume = 1f;
    }
    private IEnumerator InterpolateVolumeTo_0()
    {
        float t = BGM.volume;
        while (t > 0f)
        {
            t -= 5f * Time.deltaTime;
            BGM.volume = t;
            if (BGM_Enable)
                yield break;
            yield return null;
        }
        BGM.volume = 0f;
        time = BGM.time;
        isPausing = true;
        BGM.Pause();
    }

    public void SetSFX_Enable()
    {
        SFX_Enable = !SFX_Enable;
    }
}
