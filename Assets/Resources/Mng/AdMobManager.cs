﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds.Api;

public class AdMobManager : MonoBehaviour
{
    private static AdMobManager instance = null;
    public static AdMobManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(AdMobManager)) as AdMobManager;
            return instance;
        }
    }

    public string android_banner_id;
    public string ios_banner_id;

    public string android_interstitial_id;
    public string ios_interstitial_id;

    private BannerView bannerView;
    private InterstitialAd interstitialAd;

    public void Start()
    {
        RequestInterstitialAd();
        RequestBannerAd();
    }

    public void RequestBannerAd()
    {
        string adUnitId = string.Empty;
#if UNITY_ANDROID
        adUnitId = android_banner_id;
#elif UNITY_IOS
        adUnitId = ios_bannerAdUnitId;
#endif
        bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
        bannerView.Hide();
    }

    private void RequestInterstitialAd()
    {
        string adUnitId = string.Empty;
#if UNITY_ANDROID
        adUnitId = android_interstitial_id;
#elif UNITY_IOS
        adUnitId = ios_interstitialAdUnitId;
#endif

        interstitialAd = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();

        interstitialAd.LoadAd(request);
        interstitialAd.OnAdClosed += HandleOnInterstitialAdClosed;
    }

    public void HandleOnInterstitialAdClosed(object sender, EventArgs args)
    {
        print("HandleOnInterstitialAdClosed event received.");

        interstitialAd.Destroy();

        RequestInterstitialAd();
    }

    public void HideBanner()
    {
        if (IAPMng.IsAdEnable)
        {
            bannerView.Hide();
            bannerView.SetPosition(AdPosition.Top);
        }
    }

    public void ShowBannerAd()
    {
        if (IAPMng.IsAdEnable)
            StartCoroutine(DelayBanner());
    }

    private IEnumerator DelayBanner()
    {
        yield return new WaitForFixedUpdate();
        bannerView.SetPosition(AdPosition.Bottom);
        bannerView.Show();
    }

    public void ShowInterstitialAd()
    {
        if (IAPMng.IsAdEnable)
        {
            if (!interstitialAd.IsLoaded())
            {
                RequestInterstitialAd();
                return;
            }
            interstitialAd.Show();
        }
    }
}