﻿using System.Collections;
using UnityEngine;

public abstract class BtnMng : MonoBehaviour {

    public Transform BtnTransform;
    public AudioSource btnSFX;
    public AudioSource btnEndSFX;
    private bool isPressed;
    private float t = 1f;

    public void Pressed()
    {
        isPressed = true;
        if(SoundMng.SFX_Enable && btnSFX != null)
            btnSFX.Play();
        StartCoroutine(InvokePressed());
    }

    private IEnumerator InvokePressed()
    {
        while(t >= 0.97f)
        {
            if (!isPressed)
                yield break;
            t -= 3f * Time.deltaTime;
            BtnTransform.localScale = new Vector2(t, t);
            yield return null;
        }
        BtnTransform.localScale = Vector2.one * 0.97f;
    }

    public void Release()
    {
        isPressed = false;
        if (SoundMng.SFX_Enable && btnEndSFX != null)
            btnEndSFX.Play();
        StartCoroutine(InvokeRelease());
    }

    private IEnumerator InvokeRelease()
    {
        while (t <= 1f)
        {
            if (isPressed)
                yield break;
            t += 3f * Time.deltaTime;
            BtnTransform.localScale = new Vector2(t, t);
            yield return null;
        }
        BtnTransform.localScale = Vector2.one;
    }

}
