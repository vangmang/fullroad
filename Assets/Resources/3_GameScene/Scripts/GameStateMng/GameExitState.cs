﻿using System;
using GameStateLibrary.State;
using UnityEngine;
using System.Collections;

namespace MainGame
{
    public class GameExitState : State<GameStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return GameStateMng.GameState.exit;
            }
        }

        private int sceneIndex;

        public override void Enter(params object[] o_Params)
        {
            sceneIndex = (int)o_Params.GetValue(0);
            instance.CoverImage.gameObject.SetActive(true);
            StartCoroutine(FadeIn());
        }

        private IEnumerator FadeIn()
        {
            instance.CoverImage.gameObject.SetActive(true);
            Color color = new Color(0f, 0f, 0f, 0f);

            float t = 0f;
            while (t <= 0.997f)
            {
                t += Mathf.Lerp(1f, 0f, t) * 4.5f * Time.deltaTime;
                color.a = Mathf.Lerp(0f, 1f, t);
                instance.CoverImage.color = color;
                yield return null;
            }

            if (Mng.Instance.StageInfo.CurrentMenuLevel == MainMenu.MainMenuStateMng.MenuLevel.NOVICE)
                if (Mng.Instance.StageInfo.CurrentStageLevelNum <= 4)
                    goto Do_not_show_AD;
            AdMobManager.Instance.ShowInterstitialAd();
        Do_not_show_AD:
            {
                AdMobManager.Instance.HideBanner();
                yield return new WaitForFixedUpdate();
                GC.Collect();
                Mng.Instance.LoadLevel(sceneIndex);
            }
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }
    }
}