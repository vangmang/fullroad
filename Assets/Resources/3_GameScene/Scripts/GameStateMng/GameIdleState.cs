﻿using System;
using GameStateLibrary.State;
using UnityEngine;

namespace MainGame
{
    public class GameIdleState : State<GameStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return GameStateMng.GameState.idle;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            instance.Escape = () =>
            {
                instance.SubState(GameStateMng.GameState.exitPopup);
            };
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }
    }
}