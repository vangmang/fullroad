﻿using GameStateLibrary.State;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using PuzzleGame2DLibrary.Misc;
using System.Text;
using EditableBlock;
using System.Linq;

namespace MainGame
{
    public class GameEnterState : State<GameStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return GameStateMng.GameState.enter;
            }
        }

        private Dictionary<EditableBlockType, string> blockNameTable = new Dictionary<EditableBlockType, string>()
        {
            { EditableBlockType.NormalBlock, "NormalBlock" },
            { EditableBlockType.Character, "NormalBlock" },
            { EditableBlockType.AfterBlock, "AfterBlock" },
            { EditableBlockType.WarpBlock, "WarpBlock" },
            { EditableBlockType.LiveBlock, "LiveBlock" },
            { EditableBlockType.None, "Obstacle" },
        };

        private Dictionary<Direction, BlockMisc.Direction> blockDirection = new Dictionary<Direction, BlockMisc.Direction>()
        {
            { Direction.left, BlockMisc.Direction.left },
            { Direction.right, BlockMisc.Direction.right},
            { Direction.up, BlockMisc.Direction.up },
            { Direction.down, BlockMisc.Direction.down },
        };


        public override void Enter(params object[] o_Params)
        {
            AdMobManager.Instance.ShowBannerAd();
            instance.SceneDirection.DarkBackgroundImage.gameObject.SetActive(Mng.Instance.IsGameDarkest);
            instance.SceneDirection.MainBackgroundImage.gameObject.SetActive(!Mng.Instance.IsGameDarkest);
            instance.SceneDirection.SunObj.localPosition = new Vector3(0f, Mng.Instance.IsGameDarkest ? -1280f : -181f);

            InstantiateStage();
            StartCoroutine(FadeOut());
        }

        private void InstantiateStage()
        {
            XmlDocument xmlDoc = new XmlDocument();

            StringBuilder path;
            path = new StringBuilder("1_MenuScene/Prefabs/level/");
            path.Append(Mng.Instance.StageInfo.CurrentMenuLevel);
            TextAsset textAsset = 
                Resources.LoadAll(path.ToString()).First((file) =>
            {
                int stageNum = int.Parse(file.name.Split('_')[0]);
                return stageNum == Mng.Instance.StageInfo.CurrentStageLevelNum;
            }) as TextAsset;

            string stageName = textAsset.name;
            instance.StageInfo.StageTitle.text = Mng.Instance.StageInfo.CurrentMenuLevel.ToString();
            instance.StageInfo.StageName.text = stageName;
            xmlDoc.LoadXml(textAsset.text);

            XmlElement elements = xmlDoc["StageInfo"];
            List<BlockBase> blockList = new List<BlockBase>();

            int curr_x = 0;
            int curr_y = 0;
            bool startInit = false;
            EditorSceneStateMng.EditorStage level = (EditorSceneStateMng.EditorStage)Enum.Parse(typeof(EditorSceneStateMng.EditorStage), elements.GetAttribute("level"));
            instance.InitBlockArr(level);

            foreach (XmlElement element in elements.ChildNodes)
            {
                int x = int.Parse(element.GetAttribute("x"));
                int y = int.Parse(element.GetAttribute("y"));
                string sequence = element.GetAttribute("sequence");
                if (!startInit && sequence != string.Empty)
                {
                    int seq = int.Parse(sequence);
                    if (seq == 1)
                    {
                        curr_x = x;
                        curr_y = y;
                        startInit = true;
                    }
                }
                EditableBlockType type = (EditableBlockType)Enum.Parse(typeof(EditableBlockType), element.GetAttribute("type"));
                BlockBase block =
                    Mng.Instance.CreatePrefab("Canvas/Stage", blockNameTable[type], E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity, "Block[" + x + "][" + y + "]").GetComponent<BlockBase>();
                instance.BlockArr[x, y] = block;
                block.BlockIndex = new Index2(x, y);
                block.gameStateMng = instance;

                try
                {
                    string liveIndex = element.GetAttribute("liveIndex");
                    block.LiveIndex = int.Parse(liveIndex);
                    block.IsLiveBlock = true;
                    block.ImageGroup.alpha = 0.3f;
                    block.GetLockImage.gameObject.SetActive(true);
                }
                catch (FormatException) { }
                if (block.BlockType != BlockType.obstacle)
                    instance.StageInfo.BlockCount++;

                switch (block.BlockType)
                {
                    case BlockType.after:
                        Direction afterDir = (Direction)Enum.Parse(typeof(Direction), element.GetAttribute("direction"));
                        AfterBlock afterBlock = (AfterBlock)block;
                        afterBlock.AfterDirection = blockDirection[afterDir];
                        break;
                    case BlockType.warp:
                        int warpIndex = int.Parse(element.GetAttribute("warpIndex"));
                        WarpBlock warpBlock = (WarpBlock)block;
                        warpBlock.WarpIndex = warpIndex;
                        break;
                    case BlockType.live:
                        int liveIndex = int.Parse(element.GetAttribute("motherliveIndex"));
                        LiveBlock liveBlock = (LiveBlock)block;
                        liveBlock.MotherLiveIndex = liveIndex;
                        break;
                }

                blockList.Add(block);
            }
            foreach (BlockBase block in blockList)
            {
                switch (block.BlockType)
                {
                    case BlockType.warp:
                        WarpBlock warpBlock = (WarpBlock)block;
                        if (!warpBlock.matchedWarpBlock)
                        {
                            warpBlock.matchedWarpBlock = blockList.First((matchedBlock) =>
                            {
                                try
                                {
                                    return warpBlock.WarpIndex == ((WarpBlock)matchedBlock).WarpIndex;
                                }
                                catch (InvalidCastException)
                                {
                                    return false;
                                }
                            }) as WarpBlock;
                            warpBlock.matchedWarpBlock.matchedWarpBlock = warpBlock;
                        }
                        break;
                    case BlockType.live:
                        LiveBlock liveBlock = (LiveBlock)block;
                        blockList.ForEach((_block) =>
                        {
                            if (_block.IsLiveBlock)
                            {
                                if (liveBlock.MotherLiveIndex == _block.LiveIndex)
                                    liveBlock.LiveBlockList.Add(_block);
                            }
                        });
                        break;
                }
            }

            CharBlockStateMng charBlock =
                Mng.Instance.CreatePrefab("Canvas/Stage", "CharacterBlock", E_RESOURCES.E_3_GAMESCENE, Vector3.zero, Quaternion.identity, "Character").GetComponentInChildren<CharBlockStateMng>();
            charBlock.gameStateMng = instance;
            charBlock.currentBlock = instance.BlockArr[curr_x, curr_y];
            instance.StartBlock = instance.BlockArr[curr_x, curr_y];
            instance.charBlockStateMng = charBlock;
        }

        private IEnumerator FadeOut()
        {
            GC.Collect();
            yield return new WaitForSeconds(0.5f);
            Color color = new Color(0f, 0f, 0f, 1f);

            float t = 0f;
            while (t <= 0.997f)
            {
                t += Mathf.Lerp(1f, 0f, t) * 6f * Time.deltaTime;
                color.a = Mathf.Lerp(1f, 0f, t);
                instance.CoverImage.color = color;
                yield return null;
            }
            yield return new WaitForFixedUpdate();
            instance.CoverImage.gameObject.SetActive(false);
            instance.ChangeState(GameStateMng.GameState.idle);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }
    }
}