﻿using System;
using GameStateLibrary.State;
using System.Collections;
using UnityEngine;

namespace MainGame
{
    public class GameExitPopupState : State<GameStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return GameStateMng.GameState.exitPopup;
            }
        }

        public override void Enter(params object[] o_Params)
        {
            instance.ExitPopup.gameObject.SetActive(true);
            StartCoroutine(FadeIn());
        }

        private IEnumerator FadeIn()
        {
            float t = instance.ExitPopup.alpha;
            while(t <= 1f)
            {
                t += 5f * Time.deltaTime;
                instance.ExitPopup.alpha = t;
                yield return null;
            }
            instance.Escape = () =>
            {
                instance.RemoveState(GameStateMng.GameState.exitPopup);
            };
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
            StartCoroutine(FadeOut());
        }

        private IEnumerator FadeOut()
        {
            float t = instance.ExitPopup.alpha;
            while (t >= 0f)
            {
                t -= 5f * Time.deltaTime;
                instance.ExitPopup.alpha = t;
                yield return null;
            }
            instance.ExitPopup.gameObject.SetActive(false);
            instance.Escape = () =>
            {
                instance.SubState(GameStateMng.GameState.exitPopup);
            };
        }

    }
}