﻿using GameStateLibrary.StateMng;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace MainGame
{
    public class GameStateMng : SingletonStateMng<GameStateMng>
    {
        public enum GameState
        {
            enter,
            idle,
            exitPopup,
            exit
        }

        public RawImage BG_cloud;
        public CanvasGroup Stars1;
        public CanvasGroup Stars2;
        public Image CoverImage;
        public CanvasGroup ExitPopup;
       
        [System.Serializable]
        public struct _StageInfo_
        {
            public Text StageTitle;
            public Text StageName;
            public int BlockCount
            {
                get; set;
            }
        }
        [System.Serializable]
        public struct _SceneDirection_
        {
            public Image MainBackgroundImage;
            public Image DarkBackgroundImage;
            public Transform SunObj;
        }

        public _StageInfo_ StageInfo;
        public _SceneDirection_ SceneDirection;

        public delegate void _Escape();
        public _Escape Escape;

        public BlockBase[,] BlockArr { get; private set; }
        public BlockBase StartBlock;
        public CharBlockStateMng charBlockStateMng;
        public CanvasGroup stageBtns;
        public CanvasGroup clearBtn;
        public Transform InsigniaTransform;
        public ParticleSystem DarkestEnableParticle;
        public ParticleSystem DarkestDisableParticle;
        public GameObject ReturnToStartPositionText;

        private bool isStarGlittered;

        private float i;
        public readonly float BlockGap = 64f;

        public float revValue
        {
            get
            {
                return ((int)Mng.Instance.StageInfo.CurrentMenuLevel + 4) * BlockGap / 2;
            }
        }

        void Awake()
        {
            BlockMisc.Init();
            InitializeState();
        }

        public void StageClear()
        {
            StartCoroutine(invokeStageClear());
        }

        private IEnumerator invokeStageClear()
        {
            yield return new WaitUntil(() => !charBlockStateMng.isMoving);
            if (Mng.Instance.StageInfo.CurrentMenuLevel != MainMenu.MainMenuStateMng.MenuLevel.NOVICE &&
                charBlockStateMng.BackCount == 0)
                GoogleMng.Instance.You_are_quite_good_at_it();

            GoogleMng.Instance.CheckAchievement(Mng.Instance.StageInfo.CurrentMenuLevel, Mng.Instance.StageInfo.CurrentStageLevelNum,
                () => {
                    StartCoroutine(invokeDarkestDirection());
                });
            StartCoroutine(invokeBlockFinale());
            Mng.Instance.SaveCurrentProcess(Mng.Instance.StageInfo.CurrentMenuLevel, Mng.Instance.StageInfo.CurrentStageLevelNum);
            Mng.Instance.SetCurrentProcess();
            ++Mng.Instance.StageInfo.CurrentStageLevelNum;
            StartCoroutine(disableStageBtns());
            StartCoroutine(enableClearBtn());
        }

        private IEnumerator invokeBlockFinale()
        {
            float delay = 1f / charBlockStateMng.BlockHistory.Count;
            var arr = charBlockStateMng.BlockHistory.ToArray();
            for(int i = arr.Length - 1; i >= 0; i--)
            {
                arr[i].BlockParticles.ClearParticle.Play();
                yield return new WaitForSeconds(delay);
            }
        }

        private IEnumerator invokeDarkestDirection()
        {
            yield return new WaitForSeconds(0.2f);
            if (Mng.Instance.IsGameDarkest)
            {
                SceneDirection.MainBackgroundImage.transform.SetSiblingIndex(0);
                SceneDirection.DarkBackgroundImage.color = new Color(1f, 1f, 1f, 0f);
                SceneDirection.DarkBackgroundImage.gameObject.SetActive(true);
                DarkestEnableParticle.Play();
                Vector3 start = SceneDirection.SunObj.localPosition;
                Vector3 end = new Vector3(0f, -1280f);
                float t = 0f;
                while(t <= 0.997f)
                {
                    t += 2f * Mathf.Lerp(1f, 0f, t) * Time.deltaTime;
                    SceneDirection.DarkBackgroundImage.color = new Color(1f, 1f, 1f, t);
                    SceneDirection.SunObj.localPosition = Vector3.Lerp(start, end, t);
                    yield return null;
                }
                SceneDirection.MainBackgroundImage.gameObject.SetActive(false);
            }
            else
            {
                SceneDirection.DarkBackgroundImage.transform.SetSiblingIndex(0);
                SceneDirection.MainBackgroundImage.color = new Color(1f, 1f, 1f, 0f);
                SceneDirection.MainBackgroundImage.gameObject.SetActive(true);
                DarkestDisableParticle.Play();
                Vector3 start = SceneDirection.SunObj.localPosition;
                Vector3 end = new Vector3(0f, -181f);
                float t = 0f;
                while (t <= 0.997f)
                {
                    t += 2f * Mathf.Lerp(1f, 0f, t) * Time.deltaTime;
                    SceneDirection.MainBackgroundImage.color = new Color(1f, 1f, 1f, t);
                    SceneDirection.SunObj.localPosition = Vector3.Lerp(start, end, t);
                    yield return null;
                }
                SceneDirection.DarkBackgroundImage.gameObject.SetActive(false);
            }
        }

        private IEnumerator disableStageBtns()
        {
            float t = 1f;
            while (t >= 0f)
            {
                t -= 5f * Time.deltaTime;
                stageBtns.alpha = t;
                yield return null;
            }
            stageBtns.alpha = 0f;
            stageBtns.gameObject.SetActive(false);
        }

        private IEnumerator enableClearBtn()
        {
            clearBtn.gameObject.SetActive(true);
            float t = 0f;
            while (t <= 1f)
            {
                t += 5f * Time.deltaTime;
                clearBtn.alpha = t;
                yield return null;
            }
            clearBtn.alpha = 1f;
        }

        public void InitBlockArr(EditableBlock.EditorSceneStateMng.EditorStage level)
        {
            int max = (int)level;
            BlockArr = new BlockBase[max, max];
        }

        // Use this for initialization
        void Start()
        {
            InsigniaTransform.localPosition = new Vector3(0f, revValue + 96f);
            StateEnter(GameState.enter);
            StartCoroutine(glitter());
        }

        private IEnumerator glitter()
        {
            float t = 0f;
            while (t <= 1f)
            {
                t += 0.5f * Time.deltaTime;
                Stars1.alpha = !isStarGlittered ? t : 1 - t;
                Stars2.alpha = !isStarGlittered ? 1 - t : t;
                yield return null;
            }
            isStarGlittered = !isStarGlittered;
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(glitter());
        }

        private void Cloud()
        {
            i += 0.05625f * Time.deltaTime;
            if (i >= 1f)
                i = 0f;
            Rect rect = new Rect(new Vector2(i, 0), Vector2.one);
            BG_cloud.uvRect = rect;
        }

        // Update is called once per frame
        void Update()
        {
            Execute();
            Cloud();
            if (Application.isMobilePlatform)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (Escape != null)
                        Escape();
                }
            }
        }

        void OnDestroy()
        {
            DestroyInstance();
            BlockMisc.Release();
        }
    }
}