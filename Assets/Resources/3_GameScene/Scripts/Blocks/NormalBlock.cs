﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace MainGame
{
    /// <summary>
    /// 노멀 블록
    /// </summary>
    public class NormalBlock : BlockBase
    {
        [SerializeField]
        private Image FilledBlockImage;
        [Serializable]
        private struct _NormalBlockLine_
        {
            public Sprite up_left;
            public Sprite up_none;
            public Sprite up_right;
            public Sprite right_none;
            public Sprite down_right;
            public Sprite down_none;
            public Sprite down_left;
            public Sprite left_none;
            public Sprite vertical;
            public Sprite Horizontal;
        }
        [SerializeField]
        private _NormalBlockLine_ NormalBlockLine;
        private Dictionary<BlockMisc.Direction, Dictionary<BlockMisc.Direction, Sprite>> LineTable = new Dictionary<BlockMisc.Direction, Dictionary<BlockMisc.Direction, Sprite>>();

        public override BlockType BlockType
        {
            get
            {
                return BlockType.normal;
            }
        }

        void Awake()
        {
            LineTable[BlockMisc.Direction.right] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.left] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.up] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.down] = new Dictionary<BlockMisc.Direction, Sprite>();

            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.up] = NormalBlockLine.vertical;
            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.down] = NormalBlockLine.vertical;
            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.right] = NormalBlockLine.Horizontal;
            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.left] = NormalBlockLine.Horizontal;

            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.right] = NormalBlockLine.up_left;
            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.zero] = NormalBlockLine.down_none;
            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.left] = NormalBlockLine.up_right;

            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.right] = NormalBlockLine.down_left;
            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.zero] = NormalBlockLine.up_none;
            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.left] = NormalBlockLine.down_right;

            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.up] = NormalBlockLine.down_right;
            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.zero] = NormalBlockLine.right_none;
            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.down] = NormalBlockLine.up_right;

            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.up] = NormalBlockLine.down_left;
            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.zero] = NormalBlockLine.left_none;
            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.down] = NormalBlockLine.up_left;
        }

        public override void Fill(BlockMisc.Direction currentDir, BlockMisc.Direction recentDir)
        {
            IsPassed = true;
            blockDirection = recentDir;
            FilledBlockImage.gameObject.SetActive(true);
            BlockImage.gameObject.SetActive(false);
            BlockLine.sprite = LineTable[currentDir][recentDir];
            BlockLine.gameObject.SetActive(true);
        }
        public override void Empt(CharBlockStateMng charBlockStateMng)
        {
            IsPassed = false;
            charBlockStateMng.currentDirection = blockDirection;
            FilledBlockImage.gameObject.SetActive(false);
            BlockImage.gameObject.SetActive(true);
            BlockLine.gameObject.SetActive(false);
        }
    }
}