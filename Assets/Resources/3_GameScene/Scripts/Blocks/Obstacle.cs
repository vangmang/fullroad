﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainGame
{
    public class Obstacle : BlockBase
    {        
        public override BlockType BlockType
        {
            get
            {
                return BlockType.obstacle;
            }
        }

        public override void Fill(BlockMisc.Direction currentDir, BlockMisc.Direction recentDir)
        {
            throw new System.NotSupportedException();
        }
        public override void Empt(CharBlockStateMng charBlockStateMng)
        {
            throw new System.NotSupportedException();
        }
    }
}