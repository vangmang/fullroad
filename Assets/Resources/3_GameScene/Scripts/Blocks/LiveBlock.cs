﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace MainGame
{
    public class LiveBlock : BlockBase
    {
        public List<BlockBase> LiveBlockList = new List<BlockBase>();

        [SerializeField]
        private Image FilledBlockImage;
        [SerializeField]
        private Image live2Image;
        [SerializeField]
        private Image filledLive2Image;

        [Serializable]
        private struct _LiveBlockLine_
        {
            public Sprite up_left;
            public Sprite up_none;
            public Sprite up_right;
            public Sprite right_none;
            public Sprite down_right;
            public Sprite down_none;
            public Sprite down_left;
            public Sprite left_none;
            public Sprite vertical;
            public Sprite Horizontal;
        }

        public override BlockType BlockType
        {
            get
            {
                return BlockType.live;
            }
        }

        public int MotherLiveIndex;


        [SerializeField]
        private _LiveBlockLine_ LiveBlockLine;
        private Dictionary<BlockMisc.Direction, Dictionary<BlockMisc.Direction, Sprite>> LineTable = new Dictionary<BlockMisc.Direction, Dictionary<BlockMisc.Direction, Sprite>>();

        void Awake()
        {
            LineTable[BlockMisc.Direction.right] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.left] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.up] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.down] = new Dictionary<BlockMisc.Direction, Sprite>();

            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.up] = LiveBlockLine.vertical;
            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.down] = LiveBlockLine.vertical;
            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.right] = LiveBlockLine.Horizontal;
            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.left] = LiveBlockLine.Horizontal;

            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.right] = LiveBlockLine.up_left;
            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.zero] = LiveBlockLine.down_none;
            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.left] = LiveBlockLine.up_right;

            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.right] = LiveBlockLine.down_left;
            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.zero] = LiveBlockLine.up_none;
            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.left] = LiveBlockLine.down_right;

            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.up] = LiveBlockLine.down_right;
            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.zero] = LiveBlockLine.right_none;
            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.down] = LiveBlockLine.up_right;

            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.up] = LiveBlockLine.down_left;
            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.zero] = LiveBlockLine.left_none;
            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.down] = LiveBlockLine.up_left;
        }

        public void MakeChildrenAlive()
        {
            LiveBlockList.ForEach((block) =>
            {
                block.BlockParticles.LiveParticle.Play();
                block.IsLiveBlockAndActivated = true;
                block.ImageGroup.alpha = 1f;
                block.GetLockImage.gameObject.SetActive(false);
            });
        }

        public void MakeChildrenDead()
        {
            LiveBlockList.ForEach((block) =>
            {
                block.IsLiveBlockAndActivated = false;
                block.ImageGroup.alpha = 0.3f;
                block.GetLockImage.gameObject.SetActive(true);
            });
        }

        public override void Fill(BlockMisc.Direction currentDir, BlockMisc.Direction recentDir)
        {
            IsPassed = true;
            blockDirection = recentDir;
            BlockLine.sprite = LineTable[currentDir][recentDir];
            BlockLine.gameObject.SetActive(true);
            FilledBlockImage.gameObject.SetActive(true);

            live2Image.gameObject.SetActive(false);
            filledLive2Image.gameObject.SetActive(true);
            BlockImage.gameObject.SetActive(false);
        }
        public override void Empt(CharBlockStateMng charBlockStateMng)
        {
            BlockImage.gameObject.SetActive(true);
            IsPassed = false;
            charBlockStateMng.currentDirection = blockDirection;
            BlockLine.gameObject.SetActive(false);
            FilledBlockImage.gameObject.SetActive(false);

            live2Image.gameObject.SetActive(true);
            filledLive2Image.gameObject.SetActive(false);
        }
    }
}