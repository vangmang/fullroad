﻿using System.Collections.Generic;
using PuzzleGame2DLibrary.Misc;

namespace MainGame
{
    public static class BlockMisc 
    {
        public enum Direction
        {
            zero,
            left,
            right,
            up,
            down,
        }

        public static Dictionary<Direction, Index2> DirectionTable;


        public static void Init()
        {
            DirectionTable = new Dictionary<Direction, Index2>();
            DirectionTable.Add(Direction.zero, Index2.zero);
            DirectionTable.Add(Direction.left, Index2.left);
            DirectionTable.Add(Direction.right, Index2.right);
            DirectionTable.Add(Direction.up, Index2.up);
            DirectionTable.Add(Direction.down, Index2.down);
        }

        public static void Release()
        {
            DirectionTable = null;
        }
    }
}