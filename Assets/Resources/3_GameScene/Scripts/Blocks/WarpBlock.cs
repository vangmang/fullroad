﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MainGame
{
    /// <summary>
    /// 워프 블록
    /// </summary>
    public class WarpBlock : BlockBase
    {
        [SerializeField]
        private Image FilledBlockImage;
        [SerializeField]
        private int warpIndex;

        public int WarpIndex { get { return warpIndex; } set { warpIndex = value; } }
        public WarpBlock matchedWarpBlock;

        [System.Serializable]
        private struct _WarpBlockLine_
        {
            public Sprite up_none;
            public Sprite right_none;
            public Sprite down_none;
            public Sprite left_none;
        }
        [SerializeField]
        private _WarpBlockLine_ WarpBlockLine;
        private Dictionary<BlockMisc.Direction, Dictionary<BlockMisc.Direction, Sprite>> LineTable = new Dictionary<BlockMisc.Direction, Dictionary<BlockMisc.Direction, Sprite>>();

        public override BlockType BlockType
        {
            get
            {
                return BlockType.warp;
            }
        }

        void Awake()
        {
            LineTable[BlockMisc.Direction.zero] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.right] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.left] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.up] = new Dictionary<BlockMisc.Direction, Sprite>();
            LineTable[BlockMisc.Direction.down] = new Dictionary<BlockMisc.Direction, Sprite>();

            LineTable[BlockMisc.Direction.zero][BlockMisc.Direction.up] = WarpBlockLine.up_none;
            LineTable[BlockMisc.Direction.zero][BlockMisc.Direction.down] = WarpBlockLine.down_none;
            LineTable[BlockMisc.Direction.zero][BlockMisc.Direction.right] = WarpBlockLine.left_none;
            LineTable[BlockMisc.Direction.zero][BlockMisc.Direction.left] = WarpBlockLine.right_none;

            LineTable[BlockMisc.Direction.up][BlockMisc.Direction.zero] = WarpBlockLine.down_none;
            LineTable[BlockMisc.Direction.down][BlockMisc.Direction.zero] = WarpBlockLine.up_none;
            LineTable[BlockMisc.Direction.right][BlockMisc.Direction.zero] = WarpBlockLine.right_none;
            LineTable[BlockMisc.Direction.left][BlockMisc.Direction.zero] = WarpBlockLine.left_none;
        }

        public override void Fill(BlockMisc.Direction currentDir, BlockMisc.Direction recentDir)
        {
            IsPassed = true;
            blockDirection = recentDir;
            BlockLine.sprite = LineTable[currentDir][recentDir];
            BlockLine.gameObject.SetActive(true);
            FilledBlockImage.gameObject.SetActive(true);
            BlockImage.gameObject.SetActive(false);
        }
        public override void Empt(CharBlockStateMng charBlockStateMng)
        {
            IsPassed = false;
            charBlockStateMng.currentDirection = blockDirection;
            BlockLine.gameObject.SetActive(false);
            BlockImage.gameObject.SetActive(true);
            FilledBlockImage.gameObject.SetActive(false);
        }
    }
}