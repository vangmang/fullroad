﻿using GameStateLibrary.State;
using System;
using PuzzleGame2DLibrary.Misc;
using System.Collections;
using UnityEngine;

namespace MainGame
{
    public class CharBlockActiveState : State<CharBlockStateMng>
    {

        public override Enum GetState
        {
            get
            {
                return CharBlockStateMng.CharBlockState.active;
            }
        }

        private BlockBase targetBlock;
        private BlockMisc.Direction direction;

        public override void Enter(params object[] o_Params)
        {
            direction = (BlockMisc.Direction)o_Params.GetValue(0);
            // 맵 범위 체크
            Index2 targetIndex = instance.Index + BlockMisc.DirectionTable[direction];
            try
            {
                targetBlock = instance.gameStateMng.BlockArr[targetIndex.x, targetIndex.y];
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
            // 첫 번째 체크: 장애물인지 판별, 이미 지나왔는지 체크
            bool firstCheck = targetBlock.BlockType != BlockType.obstacle && !targetBlock.IsPassed;

            if (firstCheck)
            {
                // 두 번째 체크: 라이브 블록이고 라이브활성화 되어있는지 체크
                if (targetBlock.IsLiveBlock)
                {
                    if (!targetBlock.IsLiveBlockAndActivated)
                    {
                        targetBlock.AlertBlock();
                        return;
                    }
                }
                // 가려는 블록이 워프 블록이고 매칭되어있던 워프 블록이 라이브블록의 자식일 때 활성화 되어있는지 체크
                if (!CheckLivedWarpBlock(targetBlock))
                    return;
                
                // 세 번째 체크: 타겟이 애프터 블록일때 
                if (targetBlock.BlockType == BlockType.after)
                {
                    AfterBlock afterBlock = (AfterBlock)targetBlock;
                    if (BlockMisc.DirectionTable[afterBlock.AfterDirection] + BlockMisc.DirectionTable[direction] == Index2.zero)
                        return;
                    Index2 afterIndex = targetBlock.BlockIndex + BlockMisc.DirectionTable[afterBlock.AfterDirection];
                    BlockBase afterTargetBlock = instance.gameStateMng.BlockArr[afterIndex.x, afterIndex.y];

                    while (afterTargetBlock.BlockType == BlockType.after)
                    {
                        if (afterTargetBlock.IsLiveBlock)
                            if (!afterTargetBlock.IsLiveBlockAndActivated)
                            {
                                afterTargetBlock.AlertBlock();
                                return;
                            }
                        if (!CheckLivedWarpBlock(afterTargetBlock))
                            return;
                        afterIndex = afterIndex + BlockMisc.DirectionTable[((AfterBlock)afterTargetBlock).AfterDirection];
                        afterTargetBlock = instance.gameStateMng.BlockArr[afterIndex.x, afterIndex.y];
                    }
                    // 최종적으로 가리키는 방향이 자기 자신을 가리키는지 체크
                    if (afterTargetBlock.BlockIndex == instance.currentBlock.BlockIndex)
                    {
                        afterTargetBlock.AlertBlock();
                        return;
                    }
                    if (afterTargetBlock.IsLiveBlock)
                    {
                        if (!afterTargetBlock.IsLiveBlockAndActivated)
                        {
                            afterTargetBlock.AlertBlock();
                            return;
                        }
                    }
                    if (!CheckLivedWarpBlock(afterTargetBlock))
                        return;
                    else
                    {
                        if (afterTargetBlock.IsPassed)
                        {
                            afterTargetBlock.AlertBlock();
                            return;
                        }
                    }
                }
            }
            else
                return;

            instance.isMoving = true;
            instance.InvokePreActive();
            instance.currentBlock.Fill(direction, instance.currentDirection);
            instance.BlockHistory.Push(instance.currentBlock);
            instance.currentDirection = direction;
            StartCoroutine(moveToTarget());
        }

        private bool CheckLivedWarpBlock(BlockBase block)
        {
            try
            {
                WarpBlock warpBlock = (WarpBlock)block;
                if (warpBlock.matchedWarpBlock.IsLiveBlock)
                {
                    if (warpBlock.matchedWarpBlock.IsLiveBlockAndActivated)
                        return true;
                    else
                    {
                        warpBlock.matchedWarpBlock.AlertBlock();
                        return false;
                    }
                }
            }
            catch (InvalidCastException)
            {
                return true;
            }
            return true;
        }



        private IEnumerator moveToTarget()
        {
            float t = 0f;
            Vector3 start = instance.CharTransform.localPosition;
            while (t <= 1f)
            {
                t += instance.CharSpeed * Time.deltaTime;
                instance.CharTransform.localPosition =
                    Vector3.Lerp(start, targetBlock.BlockTransform.localPosition, t);
                yield return null;
            }
            instance.isMoving = true;
            instance.currentBlock = targetBlock;
            instance.SetActiveSound = instance.ActiveSoundTable[instance.currentBlock.BlockType];
            instance.InvokeActive();

            switch (instance.currentBlock.BlockType)
            {
                case BlockType.after:
                    AfterBlock afterBlock = (AfterBlock)instance.currentBlock;
                    yield return new WaitForSeconds(0.11f);
                    instance.PlayActiveSound();
                    instance.PlayAfterParticle(afterBlock.AfterDirection);
                    instance.ChangeState(CharBlockStateMng.CharBlockState.active, afterBlock.AfterDirection);
                    yield break;
                case BlockType.warp:
                    WarpBlock warpBlock = (WarpBlock)instance.currentBlock;
                    instance.InvokeActive();
                    instance.PlayActiveSound();
                    instance.WarpParticle.Play();
                    yield return new WaitForSeconds(instance.WarpParticle.main.startLifetime.constant * 0.35f);
                    instance.CharBlockImage.color = new Color(1f, 1f, 1f, 0f);
                    yield return new WaitUntil(() => !instance.WarpParticle.isPlaying);
                    yield return new WaitForSeconds(0.11f);
                    instance.WarpReverseParticle.Play();
                    instance.currentBlock.Fill(BlockMisc.Direction.zero, instance.currentDirection);
                    instance.BlockHistory.Push(instance.currentBlock);
                    instance.currentBlock = warpBlock.matchedWarpBlock;
                    instance.CharTransform.localPosition = instance.currentBlock.transform.localPosition;
                    instance.currentDirection = BlockMisc.Direction.zero;
                    yield return new WaitUntil(() => !instance.WarpReverseParticle.isPlaying);
                    instance.CharBlockImage.color = new Color(1f, 1f, 1f, 1f);
                    break;
                case BlockType.live:
                    instance.PlayActiveSound(instance.ActiveSoundTable[BlockType.live]);
                    LiveBlock liveBlock = (LiveBlock)instance.currentBlock;
                    liveBlock.MakeChildrenAlive();
                    break;
                default:
                    instance.PlayActiveSound();
                    break;
            }
            instance.isMoving = false;
            instance.ChangeState(CharBlockStateMng.CharBlockState.idle);
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }
    }
}