﻿using GameStateLibrary.StateMng;
using PuzzleGame2DLibrary.Misc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace MainGame
{
    public class CharBlockStateMng : SingletonStateMng<CharBlockStateMng>
    {
        [SerializeField]
        private Transform charTranform;
        public Index2 Index { get { return currentBlock.BlockIndex; } }
        public Transform CharTransform { get { return charTranform; } }
        public GameStateMng gameStateMng;

        public event EventHandler CharPreMovedEvent;
        public event EventHandler CharMovedEvent;

        public bool isMoving;
        public readonly float CharSpeed = 7f;

        public Stack<BlockBase> BlockHistory = new Stack<BlockBase>();
        public BlockBase currentBlock;
        public BlockMisc.Direction currentDirection;
        public int movedCount;

        private Vector2 prevPos;
        private Vector2 currPos;
        public ParticleSystem AfterParticle; // 에프터 블록 파티클
        public ParticleSystem WarpParticle; // 워프 블록 파티클
        public ParticleSystem WarpReverseParticle; // 워프 후 파티클
        public Image CharBlockImage;

        private readonly float movableDelta = 12f;
        private bool hasMoved;

        public int BackCount { get; private set; }

        [Serializable]
        private struct _ActiveSounds_
        {
            public AudioClip NormalActive;
            public AudioClip AfterActive;
            public AudioClip WarpActive;
            public AudioClip LiveActive;
            public AudioClip LiveDeactive;
        }
        [SerializeField]
        private _ActiveSounds_ ActiveSounds;
        [SerializeField]
        private AudioSource ActiveSound;
        public AudioClip SetActiveSound
        {
            set
            {
                ActiveSound.clip = value;
            }
        }
        public Dictionary<BlockType, AudioClip> ActiveSoundTable { get; private set; }

        public enum CharBlockState
        {
            enter,
            idle,
            active
        }

        public void PlayAfterParticle(BlockMisc.Direction afterDirection)
        {
            Vector3 pos = Vector3.zero;
            Vector3 angle = Vector3.zero;
            Vector3 scale = Vector3.one;
            switch(afterDirection)
            {
                case BlockMisc.Direction.right:
                    pos.x = 30f;
                    angle.z = 0f;
                    scale.x = 2.5f;
                    break;
                case BlockMisc.Direction.left:
                    pos.x = -30f;
                    angle.z = 180f;
                    scale.x = 2.5f;
                    break;
                case BlockMisc.Direction.up:
                    pos.y = -30f;
                    angle.z = -90f;
                    scale.z = 2.5f;
                    break;
                case BlockMisc.Direction.down:
                    pos.y = 30f;
                    angle.z = 90f;
                    scale.z = 2.5f;
                    break;
            }
            pos.z = -1f;
            AfterParticle.transform.localPosition = pos;
            AfterParticle.transform.eulerAngles = angle;
            AfterParticle.transform.localScale = scale;
            AfterParticle.Play();
        }
        public void PlayActiveSound()
        {
            if (SoundMng.SFX_Enable)
                ActiveSound.Play();
        }

        public void PlayActiveSound(AudioClip clip)
        {
            if (SoundMng.SFX_Enable)
            {
                ActiveSound.clip = clip;
                ActiveSound.Play();
            }
        }

        void Awake()
        {
            movedCount = 1;
            ActiveSoundTable = new Dictionary<BlockType, AudioClip>();
            ActiveSoundTable.Add(BlockType.normal, ActiveSounds.NormalActive);
            ActiveSoundTable.Add(BlockType.after, ActiveSounds.AfterActive);
            ActiveSoundTable.Add(BlockType.warp, ActiveSounds.WarpActive);
            ActiveSoundTable.Add(BlockType.live, ActiveSounds.LiveActive);

            currentDirection = BlockMisc.Direction.zero;
            InitializeState();
        }

        // Use this for initialization
        IEnumerator Start()
        {
            CharMovedEvent += (sender, e) =>
            {
                movedCount++;
                if (movedCount == gameStateMng.StageInfo.BlockCount)
                    gameStateMng.StageClear();
            };
            yield return new WaitForFixedUpdate();
            charTranform.localPosition =
                new Vector3(gameStateMng.BlockGap * currentBlock.BlockIndex.x - GameStateMng.Instance.revValue,
                            gameStateMng.BlockGap * -currentBlock.BlockIndex.y + GameStateMng.Instance.revValue);
            StateEnter(CharBlockState.enter);
        }

        // Update is called once per frame
        void Update()
        {
            checkSlide();
            Execute();
        }

        void checkSlide()
        {
            if (Input.GetMouseButtonDown(0))
                prevPos = Input.mousePosition;
            else if (Input.GetMouseButton(0))
            {
                currPos = Input.mousePosition;
                Vector3 deltaPos = currPos - prevPos;
                Vector3 deltaDirection = deltaPos.normalized;
                float deltaMagnitude = deltaPos.magnitude;
                if (deltaMagnitude > movableDelta && !hasMoved && !isMoving)
                {
                    hasMoved = true;
                    float x = Mathf.Abs(deltaPos.x);
                    float y = Mathf.Abs(deltaPos.y);
                    BlockMisc.Direction direction;
                    if (x > y)
                        direction = deltaPos.x > 0 ? BlockMisc.Direction.right : BlockMisc.Direction.left;
                    else
                        direction = deltaPos.y > 0 ? BlockMisc.Direction.down : BlockMisc.Direction.up;
                    ChangeState(CharBlockState.active, direction);
                }
                prevPos = currPos;
            }
            else if (Input.GetMouseButtonUp(0))
                hasMoved = false;
        }

        public void InvokeActive()
        {
            if (CharMovedEvent != null)
                CharMovedEvent(this, null);
        }
        public void InvokePreActive()
        {
            if (CharPreMovedEvent != null)
                CharPreMovedEvent(this, null);
        }

        public void Back()
        {
            if (BlockHistory.Count == 0)
                return;
            else {
                isMoving = true;

                BackCount++;
                if (BackCount == 30)
                    GoogleMng.Instance.Death_Roader();
                else if (BackCount == 100)
                    GoogleMng.Instance.IDIOT();
                StartCoroutine(invokeBack());
            }
        }

        public void BackToStartPosition()
        {
            if (BlockHistory.Count == 0)
                return;
            else {
                if (currentBlock.BlockType == BlockType.live)
                {
                    ((LiveBlock)currentBlock).MakeChildrenDead();
                    PlayActiveSound(ActiveSounds.LiveDeactive);
                }
                BlockBase recentBlock = null;
                while (BlockHistory.Count > 0)
                {
                    recentBlock = BlockHistory.Pop();
                    if (recentBlock.BlockType == BlockType.live)
                        ((LiveBlock)recentBlock).MakeChildrenDead();
                    recentBlock.Empt(this);
                    movedCount--;
                }
                currentBlock = recentBlock;
                charTranform.localPosition = currentBlock.BlockTransform.localPosition;
            }
        }
        private IEnumerator invokeBack()
        {
            bool isRecentBlockNormalOrLive = false;
            if (currentBlock.BlockType == BlockType.live)
            {
                ((LiveBlock)currentBlock).MakeChildrenDead();
                PlayActiveSound(ActiveSounds.LiveDeactive);
            }
            BlockBase recentBlock = null;
            do
            {
                recentBlock = BlockHistory.Pop();
                recentBlock.Empt(this);
                movedCount--;
                isRecentBlockNormalOrLive = recentBlock.BlockType == BlockType.normal || recentBlock.BlockType == BlockType.live;
                charTranform.localPosition = recentBlock.BlockTransform.localPosition;
                yield return new WaitForSeconds(0.025f);
            }
            while (!isRecentBlockNormalOrLive);
            currentBlock = recentBlock;
            charTranform.localPosition = currentBlock.BlockTransform.localPosition;
            yield return new WaitForFixedUpdate();
            isMoving = false;
        }
        void OnDestroy()
        {
            DestroyInstance();
        }
    }
}