﻿using GameStateLibrary.State;
using System;
using UnityEngine;

namespace MainGame
{
    public class CharBlockEnterState : State<CharBlockStateMng>
    {
        public override Enum GetState
        {
            get
            {
                return CharBlockStateMng.CharBlockState.enter;
            }
        }

        public override void Enter(params object[] o_Params)
        {
        }

        public override void Execute()
        {
        }

        public override void Exit()
        {
        }
    }
}