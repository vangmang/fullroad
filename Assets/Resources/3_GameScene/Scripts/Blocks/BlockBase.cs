﻿using UnityEngine;
using PuzzleGame2DLibrary.Misc;
using UnityEngine.UI;
using System.Collections;

namespace MainGame
{
    public enum BlockType
    {
        normal,
        after,
        warp,
        live,
        obstacle,
    }

    public abstract class BlockBase : MonoBehaviour
    {
        [System.Serializable]
        public struct _BlockParticles_
        {
            public ParticleSystem ClearParticle;
            public ParticleSystem LiveParticle;
            public ParticleSystem AlertParticle;
        }
        public _BlockParticles_ BlockParticles;

        [SerializeField]
        protected CanvasGroup Images;
        [SerializeField]
        protected Image LockImage;
        [SerializeField]
        protected Image BlockImage;   // 블록 이미지
                
        [SerializeField]              // 각각의 블록에서 스프라이트로 이미지 할당
        protected Image BlockLine;    // 블록이 채워질 때 생기는 라인 이미지
        [SerializeField]
        protected Transform blockTransform;
        protected Index2 blockIndex;

        // 라이브 
        [SerializeField]
        private int liveIndex;
        public int LiveIndex { get { return liveIndex; } set { liveIndex = value; } }
        public bool IsLiveBlock;
        public bool IsLiveBlockAndActivated;
        public bool IsPassed { get; protected set; }

        public BlockMisc.Direction blockDirection;
        public CanvasGroup ImageGroup { get { return Images; } }
        public Image GetLockImage { get { return LockImage; } }
        public Transform BlockTransform { get { return blockTransform; } }
        public Index2 BlockIndex { get { return blockIndex; } set { blockIndex = value; } }
        public GameStateMng gameStateMng;
        public abstract BlockType BlockType { get; }

        IEnumerator Start()
        {
            yield return new WaitForFixedUpdate();
            blockTransform.localPosition = 
                new Vector3(gameStateMng.BlockGap * blockIndex.x - GameStateMng.Instance.revValue,
                            gameStateMng.BlockGap * -blockIndex.y + GameStateMng.Instance.revValue);
        }

        public void AlertBlock()
        {
            if (BlockParticles.AlertParticle.isPlaying)
                return;
            AndroidFullRoadPlugin.Instance.Vibrate(100);
            BlockParticles.AlertParticle.Play();
            StartCoroutine(invokeAlertBlock());
        }

        private IEnumerator invokeAlertBlock()
        {
            Vector2 originPos = blockTransform.localPosition;
            while (BlockParticles.AlertParticle.isPlaying)
            {
                Vector2 minPos = new Vector2(-3f, -3f);
                Vector2 maxPos = new Vector2(3f, 3f);
                Vector2 randomPos = new Vector2(Random.Range(minPos.x, maxPos.x), Random.Range(minPos.y, maxPos.y));
                blockTransform.localPosition = originPos + randomPos;
                yield return null;
            }
            blockTransform.localPosition = originPos;
        }

        public abstract void Fill(BlockMisc.Direction currentDir, BlockMisc.Direction recentDir);
        public abstract void Empt(CharBlockStateMng charBlockStateMng);
    }
}