﻿using UnityEngine.UI;
using UnityEngine;

namespace MainGame
{
    /// <summary>
    /// 애프터 블록
    /// </summary>
    public class AfterBlock : BlockBase
    {
        [SerializeField]
        private Image FilledBlockImage;
        [SerializeField]
        private Image FilledDirectionImage;

        [System.Serializable]
        private struct _AfterImage_
        {
            public Image DirectionImage;
            public Sprite right;
            public Sprite left;
            public Sprite up;
            public Sprite down;

            public Sprite right2;
            public Sprite left2;
            public Sprite up2;
            public Sprite down2;
        }
        [SerializeField]
        private _AfterImage_ AfterImage;

        [SerializeField]
        private BlockMisc.Direction afterDirection;

        public BlockMisc.Direction AfterDirection {
            get { return afterDirection; }
            set
            {
                afterDirection = value;
                switch (afterDirection)
                {
                    case BlockMisc.Direction.right:
                        AfterImage.DirectionImage.sprite = AfterImage.right;
                        FilledDirectionImage.sprite = AfterImage.right2;
                        break;
                    case BlockMisc.Direction.left:
                        AfterImage.DirectionImage.sprite = AfterImage.left;
                        FilledDirectionImage.sprite = AfterImage.left2;
                        break;
                    case BlockMisc.Direction.up:
                        AfterImage.DirectionImage.sprite = AfterImage.down;
                        FilledDirectionImage.sprite = AfterImage.down2;
                        break;
                    case BlockMisc.Direction.down:
                        AfterImage.DirectionImage.sprite = AfterImage.up;
                        FilledDirectionImage.sprite = AfterImage.up2;
                        break;
                }
            }
        }

        public override BlockType BlockType
        {
            get
            {
                return BlockType.after;
            }
        }

        public override void Fill(BlockMisc.Direction currentDir, BlockMisc.Direction recentDir)
        {
            IsPassed = true;
            FilledBlockImage.gameObject.SetActive(true);
            FilledDirectionImage.gameObject.SetActive(true);
            BlockImage.gameObject.SetActive(false);
        }
        public override void Empt(CharBlockStateMng charBlockStateMng)
        {
            IsPassed = false;
            FilledBlockImage.gameObject.SetActive(false);
            FilledDirectionImage.gameObject.SetActive(false);
            BlockImage.gameObject.SetActive(true);
        }
    }
}