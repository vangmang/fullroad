﻿using UnityEngine.EventSystems;

namespace MainGame
{
    public class ExitBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public GameStateMng gameStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            gameStateMng.ChangeState(GameStateMng.GameState.exit, 2);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}