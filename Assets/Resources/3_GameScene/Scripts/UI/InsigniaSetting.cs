﻿using UnityEngine.UI;
using UnityEngine;

namespace MainGame
{
    public class InsigniaSetting : MonoBehaviour
    {
        public Image insigniaImage;

        // Use this for initialization
        void Start()
        {
            insigniaImage.sprite = Mng.Instance.StageInfo.CurrentInsignia.sprite;
        }
    }
}