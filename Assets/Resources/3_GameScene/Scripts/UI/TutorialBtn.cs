﻿using UnityEngine.EventSystems;
using UnityEngine;
using System.Collections;

namespace MainGame
{
    public class TutorialBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public GameObject TutorialBox;

        bool isClicked;

        public void OnPointerClick(PointerEventData e)
        {
            if (!isClicked)
            {
                isClicked = true;
                TutorialBox.SetActive(true);
                StartCoroutine(ClickAnywhere());
            }
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }


        private IEnumerator ClickAnywhere()
        {
            yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
            TutorialBox.SetActive(false);
            isClicked = false;
        }
    }
}