﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainGame
{
    public class BackBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public GameStateMng gameStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            if (gameStateMng.Escape != null)
                gameStateMng.Escape();
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}