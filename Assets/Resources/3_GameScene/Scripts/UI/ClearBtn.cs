﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainGame
{
    public class ClearBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public GameStateMng gameStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            int sceneIndex = Mng.Instance.MaxLevel[Mng.Instance.StageInfo.CurrentMenuLevel] + 1 > Mng.Instance.StageInfo.CurrentStageLevelNum ? 4 : 2;
            if (Mng.Instance.StageInfo.CurrentMenuLevel == MainMenu.MainMenuStateMng.MenuLevel.NOVICE && Mng.Instance.StageInfo.CurrentStageLevelNum == 5)
                sceneIndex = 2;
            gameStateMng.ChangeState(GameStateMng.GameState.exit, sceneIndex);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}