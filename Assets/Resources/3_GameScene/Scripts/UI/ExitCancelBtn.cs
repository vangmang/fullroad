﻿using UnityEngine.EventSystems;
using UnityEngine;

namespace MainGame
{
    public class ExitCancelBtn : BtnMng, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public GameStateMng gameStateMng;

        public void OnPointerClick(PointerEventData e)
        {
            gameStateMng.RemoveState(GameStateMng.GameState.exitPopup);
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
        }

        public void OnPointerUp(PointerEventData e)
        {
            Release();
        }
    }
}