﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using UnityEngine;

namespace MainGame
{
    public class BlockBackBtn : BtnMng, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
    {
        public GameStateMng gameStateMng;
        public Image image;

        private bool enableInitStage;
        IEnumerator Start()
        {
            yield return new WaitForFixedUpdate();
            gameStateMng.charBlockStateMng.CharPreMovedEvent += (sender, e) =>
            {
                image.raycastTarget = false;
                StartCoroutine(ResetRaycast());
            };
        }
        
        private IEnumerator ResetRaycast()
        {
            yield return new WaitForFixedUpdate();
            yield return new WaitUntil(() => !gameStateMng.charBlockStateMng.isMoving);
            image.raycastTarget = true;
        }

        public void OnPointerClick(PointerEventData e)
        {
            if (!gameStateMng.charBlockStateMng.isMoving)
                gameStateMng.charBlockStateMng.Back();
        }

        public void OnPointerDown(PointerEventData e)
        {
            Pressed();
            Invoke("AskInitStage", 1f);
        }

        private void AskInitStage()
        {
            gameStateMng.ReturnToStartPositionText.SetActive(true);
            enableInitStage = true;
        }

        private void InvokeInitStage()
        {
            enableInitStage = false;
            gameStateMng.charBlockStateMng.BackToStartPosition();
            gameStateMng.ReturnToStartPositionText.SetActive(false);
        }

        public void OnPointerUp(PointerEventData e)
        {
            if (enableInitStage)
                InvokeInitStage();
            CancelInvoke("AskInitStage");
            Release();
        }

        public void OnPointerExit(PointerEventData e)
        {
            gameStateMng.ReturnToStartPositionText.SetActive(false);
            enableInitStage = false;
        }
    }
}