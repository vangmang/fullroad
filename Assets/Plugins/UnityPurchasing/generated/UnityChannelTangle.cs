#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class UnityChannelTangle
    {
        private static byte[] data = System.Convert.FromBase64String("e9nXU7EzVFOKik1Zd8iz87HupgU7OVYFvWQa+iwqxlKm/z9ubsNW0XShC4DnI+30OYu/fn9IgOATB6SD93R6dUX3dH9393R0dft1wCBUGQXCxPiS/FmUfsdlCs8hdwSNVHa9H0X3dFdFeHN8X/M984J4dHR0cHV25P3VinGA5LYP0HfyKUN9j+tOqmf2zuqQsMwHonhwNmI8XJeSE/AQtMwxwwR+IpwjlVl/yzWAtw+v+mS9odBbGUF4uKLF93V4FijDZvFaMXRvFCvgd308JqROx5riRiTPQwNEENX2PylmhNEDHo6ZaoPxBkziTys2zijQXlwPH6iMsOD4/qjEdB4XVbmmiNFc3tpmyiZOkqVaCAqlTNsUUxNEoWKHONzmKnd2dHV0");
        private static int[] order = new int[] { 7,11,10,11,13,7,11,9,9,11,13,13,12,13,14 };
        private static int key = 117;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
