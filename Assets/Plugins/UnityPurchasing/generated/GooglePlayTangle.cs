#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("w8Gu/UWc4gLU0j6qXgfHlpY7riktDsfRnnwp++Z2YZJ7Cf60GrfTzg42EmhINP9agIjOmsSkb2rrCOhMNtAopqT351B0SBgABlA8jObvrUFecCmkJiKeMt62al2i8PJdtCPsqw+Mgo29D4yHjw+MjI0DjTjYrOH9jFnzeB/bFQzBc0eGh7B4GOv/XHu9D4yvvYCLhKcLxQt6gIyMjIiNjoMhL6tJy6yrcnK1oY8wSwtJFl79WSij4bmAQFo9D42A7tA7ngmiyYyX7NMYj4XE3ly2P2Iavtw3u/u86DTJO/yG2mTbbaGHM814T/dXApxFHAUtcol4HE73KI8K0buFdxO2Up86PABqBKFshj+d8jfZj/x1rI5F5+u8WZp/wCQe0o+OjI2M");
        private static int[] order = new int[] { 11,2,9,12,8,9,10,11,11,10,11,11,12,13,14 };
        private static int key = 141;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
